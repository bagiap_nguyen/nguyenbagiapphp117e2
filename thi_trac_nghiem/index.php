
<?php session_start(); ?>

<?php 

	if (isset($_SESSION['user']['avatar']) && isset($_SESSION['user']['update'])) {
		echo "OK";
	} else {
		if (isset($_SESSION['user'])) {
			header('Location:uploads.php');
		} else {
			header('Location:login.php');
		}
	}
	
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Thi trac nghiem</title>
	<meta charset="utf-8">
	<style type="text/css">
		*{
			padding: 0px;
			margin: 0px;
		}
		body {
			font-size: 15px;
			font-family: tahoma;
			width: 1000px;
		}
		
		.container {
			width: 1000px;
		}
		p {
			font-weight: bold;
			color: blue;
		}
		.id_question span {
			font-weight: bold;
			font-size: 21px;
			text-align: center;
		}
		.id_quesion {
			font-weight: bold;
			float: left;
			width: :200px;
		}
		.items {
			border:none;
			clear: both;
		}
		.content {
			float: right;
			width: 800px;
			border-bottom: none;
			border-top:  2px solid #0072bc;
		}
		.question p {
			margin-top: 10px;
		}
		.nop_bai {
			text-align: center;
		}
		.nop_bai input {
			margin-top: 10px;
			display: inline-block;
			background-color: orange;
			padding: 15px 30px;
			border-radius: 10px;
			text-decoration: none;
			color: #fff;
			text-transform: uppercase;
		}
		
		.mega_choice {
			display: block;
			margin-top: 10px;
			margin-left: 30px;
		}
		.nop_bai {
			clear: both;
		}
		.mega_choice input {
			margin-top: 10px;
			margin-left: 150px;
		}
		.message img {
			width: 50px;
			height: 50px;
			border-radius: 50%;
		}
		.message p {
			height: 50px;
			line-height: 50px;
			display: flex;
			padding: 12px auto;
			text-align: right;
			margin-right: 5px;
			justify-content: center;
		}
		.message a {
			display: inline-block;
			text-decoration: none;
			color: #fff;
			padding: 6px 12px;
			background: black;
			text-align: center;
			margin-left: 250px;
		}

	</style>
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
</head>
<body>
	<?php 
		require('answers.php');
		require('options.php');
		require('questions.php');
		$styleRight = " style= 'color:blue;' " ;
		$styleError = " style= 'color:red;' " ;
		$styleNone  = " style= 'color:#00FF00;' ";
		$userCheck  = " checked='checked' ";
	 ?>

	 <?php 

		if (isset($_POST['submit'])) {
			// Delete ['submit'] => 'submit' in Array $_POST
			//unset($_POST['submit']);
			$errorCheck 	= []; 	// Wrong answer
			$rightCheck 	= [];	// Right answer
			$noneCheck  	= [];	// None answer
			// So sánh hai mảng $bai_lam và $answers để thêm phần tử vào các mảng trên.
			$_POST ;
			$point = 0;
			// So sánh hai mảng, return về kết quả.
			
			foreach ($answers as $listAnswer) {
				foreach ($_POST as $key => $value) {
					// Lấy được câu đúng, câu sai.
					if ($key == $listAnswer['question_id']) {
						if ($value == $listAnswer['option']) {
							$rightCheck[$key] = $value;
						} else {
							$errorCheck[$key] = $value;
						}
					} 
				}
				// Lấy câu chưa check
				$t = $listAnswer['question_id'];
				array_key_exists($t, $_POST) ? '' : $noneCheck[$t] = $listAnswer['option'];
			}
			
		}
 	?>
 	<?php if(isset($_SESSION['user']['avatar']) && isset($_SESSION['user']['update']) ) : ?>
 	<div class="message">
 		<p>
 			Hello, <?php echo $_SESSION['user']['update']['name']; ?>
 			<img src=" uploads/<?php echo $_SESSION['user']['avatar']['avatar']['name']; ?>">
 			
 		</p>
 		<a href="logout.php">Đăng xuất!</a>
 	</div>
 <?php endif; ?>
	 <form method="POST" action="" id="exam">
		<div class="container" >
			<?php 
				foreach ($questions as $listQuestions) :
			 ?>
			<div class="items">
				<div class="id_question">
					<span>
						<?php 
							echo $listQuestions['id'] . '.';
						 ?>
					</span>
				</div>
				<div class="content">
					<div class="question">
						<p>
							<?php 
								echo $listQuestions['question'];
							 ?>
						</p>
					</div>
					<?php 
						foreach ($options as $listOptions) :
					 ?>
					<div class="mega_choice">
							<?php if($listQuestions['id'] == $listOptions['question_id']): ?>	
						<div class="a">
							<input type="radio" name="<?php echo $listQuestions['id']; ?>" value="A"
								<?php 
									if (isset($_POST[$listQuestions['id']])) {
										if ($_POST[$listQuestions['id']] == 'A') {
											echo $userCheck;
										}
									}

								 ?>

							>A.
							<span
								<?php 
									if (isset($_POST['submit'])) {
										// Style đáp án
										if ($answers[$listQuestions['id']-1]['option'] == 'A') {
											echo $styleRight;
										}
										// Style câu sai :
										if (isset($_POST[$listQuestions['id']])) {
											if ($_POST[$listQuestions['id']] =='A' ) {
													if ($answers[$listQuestions['id']-1]['option'] == 'A') 
													{
														echo $styleRight;
													} else {
														echo $styleError;
													}
											} 
										} 
									}
								 ?>
							>
								<?php 
									echo $listOptions['options']['A'];
								 ?>
							</span>
						</div>
						<div class="b">
							<input type="radio" name="<?php echo $listQuestions['id']; ?>" value="B"

								<?php 
									if (isset($_POST[$listQuestions['id']])) {
										if ($_POST[$listQuestions['id']] == 'B') {
											echo $userCheck;
										}
									}

								 ?>
							>B.
							<span
								<?php 
									if (isset($_POST['submit'])) {
										// Style đáp án
										if ($answers[$listQuestions['id']-1]['option'] == 'B') {
											echo $styleRight;
										}
										// Style câu sai :
										if (isset($_POST[$listQuestions['id']])) {
											if ($_POST[$listQuestions['id']] =='B' ) {
													if ($answers[$listQuestions['id']-1]['option'] == 'B') 
													{
														echo $styleRight;
													} else {
														echo $styleError;
													}
											} 
										} 
									}
								 ?>
							>
								<?php 
									echo $listOptions['options']['B'];
								 ?>
							</span>
						</div>
						<div class="c">
							<input type="radio" name="<?php echo $listQuestions['id']; ?>" value="C"

								<?php 
									if (isset($_POST[$listQuestions['id']])) {
										if ($_POST[$listQuestions['id']] == 'C') {
											echo $userCheck;
										}
									}

								 ?>
							>C.
							<span
								<?php 
									if (isset($_POST['submit'])) {
										// Style đáp án
										if ($answers[$listQuestions['id']-1]['option'] == 'C') {
											echo $styleRight;
										}
										// Style câu sai :
										if (isset($_POST[$listQuestions['id']])) {
											if ($_POST[$listQuestions['id']] =='C' ) {
													if ($answers[$listQuestions['id']-1]['option'] == 'C') 
													{
														echo $styleRight;
													} else {
														echo $styleError;
													}
											} 
										} 
									}
								 ?>
							 >
								<?php 
									echo $listOptions['options']['C'];
								 ?>
							</span>
						</div>
						<div class="d">
							<input type="radio" name="<?php echo $listQuestions['id']; ?>" value="D"
								<?php 
									if (isset($_POST[$listQuestions['id']])) {
										if ($_POST[$listQuestions['id']] == 'D') {
											echo $userCheck;
										}
									}

								 ?>
							>D.
							<span
								<?php 
									if (isset($_POST['submit'])) {
										// Style đáp án
										if ($answers[$listQuestions['id']-1]['option'] == 'D') {
											echo $styleRight;
										}
										// Style câu sai :
										if (isset($_POST[$listQuestions['id']])) {
											if ($_POST[$listQuestions['id']] =='D' ) {
													if ($answers[$listQuestions['id']-1]['option'] == 'D') 
													{
														echo $styleRight;
													} else {
														echo $styleError;
													}
											} 
										} 
									}
								 ?>
							>
								<?php 
									echo $listOptions['options']['D'];
								 ?>
							</span>
						</div>

							<?php endif; ?>

					</div>
					<?php 
						
						endforeach;
					 ?>
				</div>
			</div>
			<?php 
				endforeach;
			 ?>
			 <div class="nop_bai">
			 	<input type="submit" name="submit" id="submit">
			 </div>
		</div>
	</form>
	
	 <div class="message2">
	 	<?php 
	 		if (isset($rightCheck)) :

	 	 ?>
	 	<p>
	 		Kết quả : <?php echo count($rightCheck); ?> / 8.
	 	</p>
	 <?php endif; ?>
	 </div>

</body>
</html>