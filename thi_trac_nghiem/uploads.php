<?php 
	session_start();
	$error = [];
	if (!isset($_SESSION['user'])) {
		header('Location:login.php');
	} else {
		if (isset($_SESSION['user']['avatar']) && isset($_SESSION['user']['update']) ) {
			header('Location:index.php');
		} else {
			if (isset($_POST['submitAvatar'])) {
			
			if (empty($_POST['name'])) {
				$error[] = 'Name chua dien !';
			}
			if (empty($_POST['email'])) {
				$error[] = 'Email chua dien ';
			}
			if (empty($_FILES)) {
				$error[]  = 'Phai upload avatar !';
			}

			$allowedExtention = ['png', 'gif', 'jpg'];
			$targetDir = "uploads/";
			$targetFile = $targetDir . $_FILES["avatar"]["name"];
		    $imageFileType = @end(explode('.', $_FILES["avatar"]["name"]));
		    
		    if (!in_array($imageFileType, $allowedExtention)) {
		        $error[] = 'Avatar sai dinh dang';
		    }
		    if ($_FILES['avatar']['size'] >= 5000000 ) {
		    	$error[] = 'Avatar không quá 5Mb.';
		    }

		    if (count($error) == 0) {
		        if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $targetFile)) {
		           	//$_SESSION['user']['avatar'] = $_FILES['avatar'];
		           	$_SESSION['user']['update'] = $_POST;
		           	$_SESSION['user']['avatar'] = $_FILES;
		           	header('Location:index.php');
		        } else {
		            echo "Sorry, there was an error uploading your file.";
		        }
		    } 
		}
		}
		
	}
	
 ?>


<!DOCTYPE html>
<html>
<head>
	<title>Upload avata</title>
	<style type="text/css">
		span {
			float: left;
			width: 100px;
		}
		.option {
			margin-top: 10px;
		}
	</style>
</head>
<body>
<?php if(count($error) > 0) : ?>
	<?php foreach ($error as $key => $value) : ?>
	<div class="message">
		<p>
			<?php echo $value; ?>
		</p>
	</div>
<?php endforeach; endif; ?>

	<form action="" method="POST" enctype="multipart/form-data">
		<div class="option">
			<span>
				Name:
			</span>
			<input type="text" name="name">
		</div>
		<div class="option">
			<span>
				Email:
			</span>
			<input type="text" name="email">
		</div>
		<div class="option">
			<span>
				Avatar:
			</span>
			<input type="file" name="avatar" id="file">
		</div>
		<div class="option">
			<input type="submit" name="submitAvatar">
		</div>
	</form>
</body>
</html>