<?php

	require 'database.php';
	require 'header.php';
	$sql 	= "SELECT * FROM sinhvien 
						INNER JOIN khoa 
						ON sinhvien.makhoa=khoa.makhoa";

	$query 	= $db->query($sql);
		
	$result = $query->fetch_all(MYSQLI_ASSOC);
?>
			<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/khoa.css">
<style type="text/css">
	.search form h3 {
		margin-bottom: 10px;
	}
	.search form {
		margin-top: 40px;
		display: block;
		width: 320px;
		align-items: center;
	}
	.search span {
		float: left;
		width: 100px;
	}
	.search input {
		margin-bottom: 20px;
	}
	.search {
		margin-bottom: 130px;
	}
</style>
			<!-- End style-->
<div class="container">
	<?php  
		if (isset($_SESSION['admin'])) :
	?>


	<?php  
		if (isset($_SESSION['message_delete'])) :
	?>
	<div class="message">
		<span style="color: blue;">
			<?php echo $_SESSION['message_delete']; ?>
		</span>
	</div>
	<?php  
			unset($_SESSION['message_delete']);
		endif;
	?>
	<div class="add">
		<a href="sinhvien.add.php">Thêm sinh viên</a>
	</div>
	<div class="search">
		<form method="GET" action="sinhvien.search.php">
			<div class="heading">
				<h3 align="center">
					Tìm kiếm sinh viên
				</h3>
			</div>
			<div class="masv">
				<span>
					Ma SV :
				</span>
				<input type="text" name="masv">
			</div>
			<div class="tensv">
				<span>
					Họ tên :
				</span>
				<input type="text" name="hoten">
			</div>
			<div class="email">
				<span>
					Email : 
				</span>
				<input type="text" name="email">
			</div>
			<div class="submit">
				<input type="submit" name="submitSearch" value="Tìm kiếm">
			</div>
			<div class="result">
				<?php  
					if(isset($_SESSION['search'])) :
				?>
				<span style="color: red; width: 600px;">
					<?php echo $_SESSION['search']; ?>
				</span>
				<?php  
						unset($_SESSION['search']);
					endif;
				?>
			</div>
		</form>
	</div>
	<?php  
		endif;
	?>
	<div class="sinhvien">
		<table border="1" cellpadding="10">
			<thead>
				
				<tr>
					<th colspan="9">
						Danh sách sinh viên
					</th>
				</tr>
				<tr>
					<th>
						STT
					</th>
					<th>
						Mã sinh viên
					</th>
					<th>
						Họ tên
					</th>
					<th>
						Giới tính
					</th>
					<th>
						Địa chỉ
					</th>
					<th>
						Khoa
					</th>

					<?php  
						if (isset($_SESSION['admin'])) :
					?>

					<th colspan="3">
						Tùy chọn
					</th>

					<?php  
						endif;
					?>
				</tr>
			</thead>
			<tbody>
				<?php  
					if (count($result) > 0 && !is_null($result)) :
						$dem = 1;
						foreach ($result as $sv) :
				?>
				<tr>
					<td>
						<?php echo $dem; ?>
					</td>

					<td>
						<?php echo $sv['masv']; ?>
					</td>
					<td>
						<?php echo $sv['hoten'] ?>
					</td>
					<td>
						<?php  
							if ($sv['gioitinh'] == "1") {
								echo "Nam";
							} else {
								echo "Nữ";
							}
						?>
					</td>
					<td>
						<?php echo $sv['diachi']; ?>
					</td>
					<td>
						<?php echo $sv['ten_khoa']; ?>
					</td>
					<?php  
						if (isset($_SESSION['admin'])) :
					?>
					<td>
						<a href="sinhvien.edit.php?id=<?php echo $sv['masv']; ?> ">
							Edit
						</a>
					</td>
					<td>
						<a href="sinhvien.delete.php?id=<?php echo $sv['masv']; ?> " id="active">
							Delete
						</a>
					</td>
					<td>
						<a href="sinhvien.diem.php?id=<?php echo $sv['masv'];?> ">
							Xem điểm
						</a>
					</td>
					<?php  
						endif;
					?>
				</tr>
				<?php  
							$dem++;
						endforeach;
					endif;
				?>
			</tbody>
		</table>
	</div>
</div>