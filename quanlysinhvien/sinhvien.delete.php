<?php
	session_start();  
	require 'database.php';
	$id 			= $_GET['id'];
	$flash_message	= '';

	$sql 		= " SELECT * FROM sinhvien WHERE masv = '{$id}' ";
	$query 		= $db->query($sql);
	$sinhvien	= $query->fetch_assoc();

// Trước khi xóa phải kiểm tra xem có sinh viên có ID kia không ?
	// Thường áp dụng khi xử lý dữ liệu GET !!
	if (is_null($sinhvien)) {
		$flash_message 				= 'Not found student has id =  $id ';
		$_SESSION['message_delete']	= $flash_message;
		header('Location:sinhvien.php');
		exit;
	}

	$sql 			= " DELETE FROM sinhvien
						WHERE masv = '{$id}' LIMIT 1 ";
	
	if ($db->query($sql) === TRUE) {
	    $flash_message 		= 'Xóa sinh viên thành công.';
	} else {
	    $flash_message 		= "Can not Delete student id = '{$id}' Try again.";
	}
	$_SESSION['message_delete'] 	= $flash_message;
	header('Location:sinhvien.php');
?>