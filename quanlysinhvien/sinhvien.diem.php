<?php  
	require 'database.php';
	require 'header.php';

	if (!isset($_GET['id'])) {
		header('Location:sinhvien.php');
	}

	$id 	= $_GET['id'];
	$flag	= 0;

	$sql 	= " SELECT sinhvien.hoten,ketqua.masv,mon_hoc.mamonhoc,
						mon_hoc.tenmonhoc,ketqua.diem
				FROM ketqua 
				INNER JOIN sinhvien ON sinhvien.masv = ketqua.masv
				INNER JOIN mon_hoc ON mon_hoc.mamonhoc = ketqua.mamonhoc
				WHERE sinhvien.masv = '{$id}'"; 


	$query 	= $db->query($sql);
	$sv 	= $query->fetch_all(MYSQLI_ASSOC);

	if (count($sv) == 0) {
		$flag = 1;
	}

?>
<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/khoa.css">
			<!-- End style-->
<div class="container">
	<div class="message">
		<?php if($flag == 1): ?>
		<span style="color: blue;">
			Sinh viên chưa có điểm hoặc điểm đang cập nhật.
		</span>
		<?php endif; ?>
		
	</div>
	<div class="result">
		<?php  
			if (!is_null($sv) && count($sv) > 0) :
		?>
		<p>
			Sinh viên: <?php echo $sv[0]['hoten']; ?>
		</p>
		<p>
			Mã SV : 	<?php echo $sv[0]['masv']; ?>
		</p>
		<table border="1" cellpadding="10">
			<thead>
				<tr>
					<th colspan="3">
						Kết quả thi.
					</th>
				</tr>
				
				<tr>
					<th>
						Mã môn học
					</th>
					<th>
						Tên môn học
					</th>
					<th>
						Điểm
					</th>
				</tr>

			</thead>
			<tbody>
				<?php  
					foreach ($sv as $item) :
				?>
				<tr>
					<td>
						<?php echo $item['mamonhoc']; ?>
					</td>
					<td>
						<?php echo $item['tenmonhoc']; ?>
					</td>
					<td>
						<?php echo $item['diem']; ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php  
			endif;
		?>
	</div>
</div>