-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2018 at 05:23 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quanlysinhvien`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `status`) VALUES
(1, 'admin', 'toilaadmin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ketqua`
--

CREATE TABLE `ketqua` (
  `diem` float NOT NULL,
  `masv` varchar(50) NOT NULL,
  `mamonhoc` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ketqua`
--

INSERT INTO `ketqua` (`diem`, `masv`, `mamonhoc`) VALUES
(7, 'sv0098', 'MH02'),
(8, 'sv0226', 'MH03'),
(9, 'sv0851', 'MH04'),
(7, 'sv1070', 'MH04'),
(0, 'sv0094', 'MH03'),
(6, 'sv0850', 'MH02'),
(4, 'sv0858', 'MH03'),
(2.5, 'sv0858', 'MH05'),
(6, 'sv0858', 'MH04'),
(8, 'sv0856', 'MH04'),
(9.5, 'sv0854', 'MH04'),
(9.2, 'sv0854', 'MH2058'),
(4, 'sv1032', 'MH110'),
(3, 'sv1047', 'MH370'),
(6, 'sv1634', 'MH03'),
(4, 'sv1682', 'MH110'),
(2, 'sv1045', 'MH370'),
(5, 'sv0845', 'MH2058'),
(6, 'sv0854', 'MH03');

-- --------------------------------------------------------

--
-- Table structure for table `khoa`
--

CREATE TABLE `khoa` (
  `makhoa` varchar(20) NOT NULL,
  `ten_khoa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khoa`
--

INSERT INTO `khoa` (`makhoa`, `ten_khoa`) VALUES
('k02', 'Khoa Công nghệ thông tin'),
('k03', 'Khoa Hàng không Vũ trụ'),
('k04', 'Khoa Kỹ thuật điều khiển'),
('k05', 'Khoa Vô tuyến điện tử'),
('k10', 'Toán cơ tin'),
('k11', 'Vật Lý'),
('k13', 'Hóa học'),
('k14', 'Sinh học'),
('k15', 'Công nghệ sinh học');

-- --------------------------------------------------------

--
-- Table structure for table `mon_hoc`
--

CREATE TABLE `mon_hoc` (
  `mamonhoc` varchar(20) NOT NULL,
  `tenmonhoc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mon_hoc`
--

INSERT INTO `mon_hoc` (`mamonhoc`, `tenmonhoc`) VALUES
('MH01', 'Lập trình đa phương tiện'),
('MH02', 'Các hệ thống phân tán'),
('MH03', 'Phân tích Malwares'),
('MH04', 'Quản lý dự án phần mềm'),
('MH05', 'Đạo đức nghề nghiệp'),
('MH110', 'Điều khiển tối ưu'),
('MH117', 'Tối ưu hóa'),
('MH2015', 'Thống kê ứng dụng'),
('MH2058', 'Lập trình hàm'),
('MH298', 'Phương pháp Monte-Carlo '),
('MH370', 'Cấu trúc dữ liệu và giải thuật'),
('MH373', 'Ngôn ngữ lập trình');

-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE `sinhvien` (
  `masv` varchar(50) NOT NULL,
  `hoten` varchar(100) NOT NULL,
  `ngaysinh` date DEFAULT NULL,
  `gioitinh` tinyint(4) NOT NULL,
  `diachi` varchar(100) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `makhoa` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL DEFAULT '123456abcde',
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sinhvien`
--

INSERT INTO `sinhvien` (`masv`, `hoten`, `ngaysinh`, `gioitinh`, `diachi`, `email`, `makhoa`, `password`, `status`) VALUES
('sv0094', 'hoàng bách', '2018-03-07', 1, 'quảng trị', 'aijfi@gmail.com', 'k04', '123456abcde', 0),
('sv0098', 'Trần Văn Came', '2018-03-02', 1, 'Phú xuyên', 'camtry@gmail.com', 'k04', '123456abcde', 0),
('sv0226', 'Mai hoàng yến', '2018-03-09', 0, 'vĩnh phúc', 'guhijpo@gmail.com', 'k03', '123456abcde', 0),
('sv0845', 'Tống xuân trường', '2018-03-20', 1, 'hà nội', 'hkjhagu@gmail.com', 'k04', '123456abcde', 0),
('sv0848', 'nguyễn anh duy', '2018-03-19', 1, 'hưng yên', 'yghonhu@gmail.com', 'k05', '123456abcde', 0),
('sv0850', 'phạm mỹ duyên', '2018-03-16', 0, 'hà nam', 'sv0850', 'k11', '123456abcde', 0),
('sv0851', 'vũ thị giang', '2018-03-22', 0, 'quảng ninh', 'ignv@gmail.com', 'k05', '123456abcde', 0),
('sv0852', 'phan đông dương', '2018-03-20', 1, 'lý sơn', 'jaif@gmail.com', 'k05', '123456abcde', 0),
('sv0853', 'lê thị hiền', '2018-03-20', 0, 'kiến xương', 'uuyygfv@gmail.com', 'k04', '123456abcde', 0),
('sv0854', 'hà đình đức', '2018-03-11', 1, 'tàu khựa', 'tfhgb@gmail.com', 'k02', '123456abcde', 0),
('sv0856', 'Trần anh dũng', '2018-03-06', 1, 'lạng sơn', 'aidfaihdf@gmail.com', 'k05', '123456abcde', 0),
('sv0858', 'nguyễn vắn đạt', '2018-03-14', 1, 'hà nội', 'aghj@gmail.com', 'k05', '123456abcde', 0),
('sv1029', 'chu văn dũng', '2018-03-02', 1, 'hà nam', 'iubj@gmail.com', 'k05', '123456abcde', 0),
('sv1031', 'vũ tông thủy', '2018-03-05', 1, 'thanh hóa', 'oiasfn@gmail.com', 'k02', '123456abcde', 0),
('sv1032', 'phạm vũ thảo', '2018-03-01', 0, 'thái bình', 'sv1032', 'k15', '123456abcde', 0),
('sv1040', 'nguyễn vắn đạt', '2018-03-14', 1, 'hà nội', 'aghj@gmail.com', 'k05', '123456abcde', 0),
('sv1043', 'lê thị hiền', '2018-03-20', 0, 'kiến xương', 'uuyygfv@gmail.com', 'k04', '123456abcde', 0),
('sv1045', 'hoàng công danh', '2018-03-15', 1, 'bắc ninh', 'yutydtsdgf@gmail.com', 'k03', '123456abcde', 0),
('sv1047', 'chu văn dũng', '2018-03-02', 1, 'hà nam', 'iubj@gmail.com', 'k05', '123456abcde', 0),
('sv1048', 'chu văn dũng', '2018-03-02', 1, 'hà nam', 'iubj@gmail.com', 'k05', '123456abcde', 0),
('sv1049', 'phạm mỹ duyên', '2018-03-16', 0, 'hà nam', 'gbjhhihh@gmail.com', 'k02', '123456abcde', 0),
('sv1051', 'Tống xuân trường', '2018-03-20', 1, 'hà nội', 'hkjhagu@gmail.com', 'k04', '123456abcde', 0),
('sv1053', 'đào duy lượng', '2018-03-13', 1, 'hải phòng', 'ihj@gmail.com', 'k04', '123456abcde', 0),
('sv1054', 'trịnh hải kiều', '2018-03-29', 0, 'xuân mai', 'aidhsf@gmail.com', 'k04', '123456abcde', 0),
('sv1070', 'Trần anh dũng', '2018-03-06', 1, 'lạng sơn', 'aidfaihdf@gmail.com', 'k05', '123456abcde', 0),
('sv110224', 'Nguyễn Văn Tèo', '2012-06-04', 1, 'Nam dinh', 'tom@gmail.com', 'k13', '123456abcde', 0),
('sv12002275', 'Nguyễn Văn A', '2012-04-06', 1, 'Hà nội', 'admin@gmail.com', 'k10', '123456abcde', 0),
('sv1625', 'đào duy lượng', '2018-03-13', 1, 'hải phòng', 'ihj@gmail.com', 'k04', '123456abcde', 0),
('sv1629', 'hoàng công Tánh', '2018-03-15', 0, 'bắc ninh', 'sv1629@gmail.com', 'k13', '123456abcde', 0),
('sv1634', 'hoàng công danh', '2018-03-15', 1, 'bắc ninh', 'yutydtsdgf@gmail.com', 'k03', '123456abcde', 0),
('sv1680', 'trịnh hải kiều', '2018-03-29', 0, 'xuân mai', 'aidhsf@gmail.com', 'k04', '123456abcde', 0),
('sv1682', 'phạm vũ thảo my', '2018-03-01', 0, 'thái bình', 'aidfha@gmail.com', 'k02', '123456abcde', 0),
('sv1722', 'vũ tông thủy', '2018-03-05', 1, 'thanh hóa', 'oiasfn@gmail.com', 'k02', '123456abcde', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ketqua`
--
ALTER TABLE `ketqua`
  ADD KEY `masv` (`masv`),
  ADD KEY `mamonhoc` (`mamonhoc`);

--
-- Indexes for table `khoa`
--
ALTER TABLE `khoa`
  ADD PRIMARY KEY (`makhoa`);

--
-- Indexes for table `mon_hoc`
--
ALTER TABLE `mon_hoc`
  ADD PRIMARY KEY (`mamonhoc`);

--
-- Indexes for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`masv`),
  ADD KEY `makhoa` (`makhoa`),
  ADD KEY `hoten` (`hoten`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ketqua`
--
ALTER TABLE `ketqua`
  ADD CONSTRAINT `ketqua_ibfk_1` FOREIGN KEY (`masv`) REFERENCES `sinhvien` (`masv`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ketqua_ibfk_2` FOREIGN KEY (`mamonhoc`) REFERENCES `mon_hoc` (`mamonhoc`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD CONSTRAINT `sinhvien_ibfk_1` FOREIGN KEY (`makhoa`) REFERENCES `khoa` (`makhoa`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
