<?php  
	require 'database.php';
	require 'header.php';

	$sql 		= " SELECT * FROM mon_hoc ";
	$query 		= $db->query($sql);
	$monhoc 	= $query->fetch_all(MYSQLI_ASSOC);	 
?>
			<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/khoa.css">
			<!-- End style-->
<div class="container">
	<div class="monhoc">
		<table border="1" cellpadding="10">
			<thead>
				<tr>
					<th>
						STT
					</th>
					<th>
						Mã môn học
					</th>
					<th>
						Tên môn học
					</th>
					<?php  
						if (isset($_SESSION['admin'])) :
					?>
					<th>
						Xem điểm
					</th>
					<?php  
						endif;
					?>
				</tr>
			</thead>
			<tbody>
				<?php  
					if (!is_null($monhoc) && count($monhoc)>0) :
						$dem = 1;
						foreach ($monhoc as $item):
				?>
				<tr>
					<td>
						<?php echo $dem; ?>
					</td>
					<td>
						<?php echo $item['mamonhoc']; ?>
					</td>
					<td>
						<?php echo $item['tenmonhoc']; ?>
					</td>
					<?php  
						if (isset($_SESSION['admin'])) :
					?>
					<td>
						<a href="monhoc.ketqua.php?id=<?php echo $item['mamonhoc']; ?>">
							Danh sách điểm
						</a>
					</td>
					<?php  
						endif;
					?>
				</tr>
				<?php
							$dem++;			  
						endforeach;
					endif;
				?>
			</tbody>
		</table>
	</div>
</div>