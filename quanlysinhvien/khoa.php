<?php  
	require 'database.php';
	require 'header.php';
	$sql 	= " SELECT * FROM khoa ";
 	$query 	= $db->query($sql);
 	$result	= $query->fetch_all(MYSQLI_ASSOC);
 	$dem = 1;
?>
			<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/khoa.css">
			<!-- End style-->
<div class="container">
	<?php  
		if (isset($_SESSION['admin'])) :
	?>

	<div class="add">
		<a href="khoa.add.php">Thêm khoa mới</a>
	</div>

	<?php 
		endif; 
	?>
	<div class="khoa">
		<table border="1" cellpadding="10">
			<thead>
				<tr>
					<th colspan="5" class="active">
						Danh sánh các khoa
					</th>
				</tr>
				<tr>
					<th>
						STT
					</th>
					<th>
						Mã khoa
					</th>
					<th>
						Tên khoa
					</th>
					<?php  
						if (isset($_SESSION['admin'])) :
					?>
					<th colspan="2">
						Tùy chỉnh
					</th>

					<?php  
						endif;
					?>

				</tr>
			</thead>
			<tbody>
				<?php  
					if (count($result) > 0):
						foreach ($result as $khoa) :
				?>
				<tr>
					<td>
						<?php echo $dem; ?>
					</td>
					<td>
						<?php echo $khoa['makhoa']; ?>
					</td>
					<td>
						<?php echo $khoa['ten_khoa']; ?>
					</td>
					<?php  
						if (isset($_SESSION['admin'])) :
					?>
					<td>
						<a href="khoa.update.php?id=<?php echo $khoa['makhoa']; ?>">
							Updates
						</a>
					</td>
					<td>
						<a href="khoa.delete.php?id=<?php echo $khoa['makhoa']; ?>" id="active" >
							Delete
						</a>
					</td>
					<?php  
						endif;
					?>
				</tr>
				<?php  
							$dem++;
						endforeach;
					endif;
				?>
			</tbody>
		</table>
	</div>
</div>