<?php  
	require 'database.php';
	require 'header.php';

	$id 	= $_GET['id'];
?>
<?php  
	// Lấy danh sách khoa
	$sql 	= " SELECT * FROM khoa ";
	$query 	= $db->query($sql);
	$khoa 	= $query->fetch_all(MYSQLI_ASSOC);	
?>
<?php  
	// Lấy bản ghi trong CSDL
	$sql	= " SELECT * FROM sinhvien WHERE masv='{$id}' LIMIT 1 ";
	$query 	= $db->query($sql);
	$sv 	= $query->fetch_assoc();
	// Kiểm tra xem lấy được hay ko ? Nếu không điều hướng về sinhvien.php
	if(is_null($sv)) {
		header('Location:sinhvien.php');
	}
	// Điền data lấy được vào form
	// Validate form 
	$errors = [];
	if (isset($_POST['submit'])) {
		# code...
		if (!isset($_POST['masv']) || $_POST['masv'] == '') {
		$errors[] 		= 'Vui lòng nhập Mã SV';
		}

		if (!isset($_POST['hoten']) || $_POST['hoten'] == '') {
			$errors[] 	= 'Vui lòng nhập Tên SV';
		}

		if (empty($_POST['ngaysinh'])) {
			$errors[] 	= 'Vui lòng nhập ngày sinh';
		}

		if (empty($_POST['diachi'])) {
			$errors[] 	= 'Vui lòng nhập địa chỉ';
		}

		if (empty($_POST['email'])) {
			$errors[] 	= 'Vui lòng nhập email';
		}

		if (count($errors) == 0) {
			# code...
			$masv 		= trim($_POST['masv']);
			$hoten 		= trim($_POST['hoten']);
			$ngaysinh 	= trim($_POST['ngaysinh']);
			$gioitinh 	= trim($_POST['gioitinh']);
			$diachi 	= trim($_POST['diachi']);
			$email 		= trim($_POST['email']); 
			$makhoa 	= trim($_POST['makhoa']);

			// Kiểm tra masv có bị trùng trong CSDL:
			// Lấy ra col masv = $_POST AND masv != id
			$sql 		= "SELECT * FROM sinhvien 
									WHERE masv = '{$masv}' AND masv <> '{$id}' 
					 				LIMIT 1";
			$query 		= $db->query($sql);
			$result 	= $query->fetch_assoc();

			if (!is_null($result)) {
				$errors[] = 'Mã SV đã tồn tại';
			} else {
				$sql = "UPDATE sinhvien 
							SET hoten 		='{$hoten}',
								ngaysinh 	='{$ngaysinh}',
								gioitinh 	='{$gioitinh}',
								diachi 		='{$diachi}',
								email 		='{$email}',
								makhoa 		='{$makhoa}'
							WHERE masv='{$masv}'";
				$query = $db->query($sql);
				if ($query) {
					header('Location: sinhvien.php');
				} else {
					$errors[] = 'Không cập nhật được sinh viên';
				}
			}

		}
	}

?>

			<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/edit.css">
			<!-- End style-->
<div class="container">
	<div class="message">
		<?php  
			if (count($errors) > 0) :
				foreach ($errors as $key => $value) :
		?>
		<span style="color: red;">
			<?php echo $value; ?>
		</span>
		<?php  
				endforeach;
			endif;
		?>
	</div>
	<div class="edit">
		<form method="POST">
			<table border="1" cellpadding="10">
				<thead>
					<tr>
						<th colspan="2">
							Cập nhật thông tin
						</th>
					</tr>
				</thead>
				<tr>
					<td>
						Mã sinh viên : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="masv" 
						value="<?php 
							if(isset($_POST['masv'])) echo $_POST['masv'];
								else echo $sv['masv'];
						?>">
					</td>
				</tr>
				<tr>
					<td>
						Họ tên: <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="hoten" 
						value="<?php 
							if(isset($_POST['hoten'])) echo $_POST['hoten'];
								else echo $sv['hoten'];
						?>">
					</td>
				</tr>
				<tr>
					<td>
						Ngày sinh : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="ngaysinh" 
						value="<?php 
							if(isset($_POST['ngaysinh'])) 
								echo $_POST['ngaysinh'];
								else echo $sv['ngaysinh'];
						?>">
					</td>
				</tr>
				<tr>
					<td>
						Giới tính : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="radio" name="gt" value="0"
						<?php 
							if ((isset($_POST['gioitinh']) && $_POST['gioitinh'] == 0) 
								|| $sv['gioitinh'] == 0) 
								echo 'checked="checked"';?>
						>Nữ <br>
						<input type="radio" name="gt" value="1"
						<?php 
							if ((isset($_POST['gioitinh']) && $_POST['gioitinh'] == 1) || $sv['gioitinh'] == 1) 
								echo 'checked="checked"';?>
						> Nam
					</td>
				</tr>
				<tr>
					<td>
						Địa chỉ : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="diachi" 
						value="<?php 
							if(isset($_POST['diachi'])) echo $_POST['diachi'];
								else echo $sv['diachi'];
						?>">
					</td>
				</tr>
				<tr>
					<td>
						Email : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="email" 
						value="<?php 
							if(isset($_POST['submit'])) echo $_POST['submit'];
								else echo $sv['masv'];
						?>">
					</td>
				</tr>
				<tr>
					<td>
						Khoa : <span style="color: red;">(*)</span>
					</td>
					<td>
						<select name="makhoa">
							<option>
								<---Chọn--->
							</option>
							<?php
								// $check dùng để in thông tin từ bản ghi trong CSDL lấy được.
								$check = 'selected="selected"';  
								// In checked.
								if(count($khoa) > 0 && !is_null($khoa)):
									foreach ($khoa as $items):
							?>
							<option 
							value="<?php echo $items['makhoa']; ?>"
							<?php  
								if (
									(isset($_POST['makhoa'])&& $_POST['makhoa'] == $items['makhoa']) 
									|| $sv['makhoa'] == $items['makhoa']) echo $check;
							?>

							>
								<?php echo $items['ten_khoa']; ?>
							</option>
							<?php  
									endforeach;
								endif;
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" name="submit" 
								value="Cập nhật" id="submit">
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<!--Chú thích về thuộc tính selected: 
Trong vòng lặp foreach :
 			 IN ra checked khi xảy ra 1 trong 2 trường hợp :
 			 	1.Tồn tại $_POST['makhoa'] AND $_POST['makhoa'] = 
 			 	2.Không tồn tại $_POST['makhoa'] $sv['makhoa'] = items['makhoa']

-->