<?php  
	require 'database.php';
	require 'header.php';

	$id 		= $_GET['id'];
	$flag 		= 0;

	$sql 		= " SELECT ketqua.masv,ketqua.mamonhoc,ketqua.diem,
							sinhvien.hoten , mon_hoc.tenmonhoc
					FROM ketqua
					INNER JOIN sinhvien ON sinhvien.masv = ketqua.masv
					INNER JOIN mon_hoc ON ketqua.mamonhoc = mon_hoc.mamonhoc
					WHERE ketqua.mamonhoc = '{$id}'
					ORDER BY ketqua.diem ASC ";

	$query 		= $db->query($sql);
	$result 	= $query->fetch_all(MYSQLI_ASSOC);

	if (count($result) == 0 || $result === NULL) {
		$flag = 1;
	}
?>
			<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/khoa.css">
			<!-- End style-->
<div class="container">
	<?php  
		if ($flag == 1) :
	?>
	<div class="message">
		<span style="color: blue;">
			Môn học chưa có điểm hoặc điểm đang cập nhật
		</span>
	</div>
	<?php  
		endif;
	?>


	<?php  
		if (count($result) > 0 && !is_null($result)) :
	?>
	<div class="ketqua">
		<h3 align="center">
			<span>
				Danh sách điểm môn học:
				<?php  
					echo $result[0]['tenmonhoc'];
				?>
			</span>
			<p>
				Mã Môn học :
				<?php  
					echo $result[0]['mamonhoc'];
				?>
			</p>
		</h3>
		<table border="1" cellpadding="10">
			<thead>
				<tr>
					<th colspan="3">
						Kết quả thi
					</th>
				</tr>
				<tr>
					<th>
						Mã SV
					</th>

					<th>
						Họ tên
					</th>
					
					<th>
						Điểm
					</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					foreach ($result as $item) :
				?>
				<tr>
					<td>
						<?php echo $item['masv']; ?>
					</td>
					<td>
						<?php echo $item['hoten']; ?>
					</td>
					
					<td>
						<?php echo $item['diem']; ?>
					</td>
				</tr>
				<?php  
					endforeach;
				?>
			</tbody>
		</table>
	</div>
	<?php  
		endif;
	?>
</div>