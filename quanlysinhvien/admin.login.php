<?php

	  
	session_start();

	if (isset($_SESSION['admin'])) {
	  	header('Location:header.php');
	 }

	require 'database.php';

	$error = [];
	if (isset($_POST['login'])) {
		#Validate form
		if (empty($_POST['username'])) {
			$error[] 	= 'Vui lòng điền tên đăng nhập.';
		}

		if (empty($_POST['password'])) {
			$error[] 	= 'Vui lòng nhập mật khẩu.';
		}

		if (count($error) == 0) {
			$username 		= trim($_POST['username']);
			$password 		= $_POST['password'];

			$sql 			= " SELECT * FROM admin 
				WHERE username  = '{$username}' LIMIT 1 " ;

			$query			= $db->query($sql);

			$acc 	 		= $query->fetch_assoc();

			if (!is_null($acc) && count($acc) > 0) {

				if ($acc['password'] == $password) {

					$_SESSION['admin'] 	= $acc;
					header('Location:xuly.login.php');
				} else {
					$error[] 			= 'Sai mật khẩu';
				}

				
			} else {
				$error[] 			= 'Sai tên đăng nhập hoặc mật khẩu.';
			}	
		}
	}

?>
<style type="text/css">
	.login {
		width: 1000px;
		margin: 0px auto;
		padding: 0px;
	}
	.login form {
		display: block;
		width: 600px;
	}
	.login .heading {
		background: blue;
		height: 40px;
	}
	.heading p {
		text-align: center;
		color: #fff;
		line-height: 40px;
		font-size: 26px;
		font-weight: bold;
	}
	.input {
		padding-top: 20px;
	}
	.input input {
		width: 250px;
		height: 30px;
	}
	.options {
		background: #ccc;
		min-height: 110px;
	}
	form .options .input  span {
		font-weight: bold;
		font-size: 21px;
		float: left;
		width: 200px;
		text-indent: 40px;
	}
	.submit {
		background: #ccc;
		display: block;
		text-align: left;
		text-indent: 50px;
		height: 45px;
	}
	.submit input {
		height: 40px;
		background: blue;
		color: #fff;
		text-align: center;
		line-height: 40px;
	}
</style>
<!-- End style -->


<div class="login">
	<?php  
		if (count($error) > 0) :
			foreach ($error as $key => $value) :
	?>
	<div class="message">
		<span style="color: red;">
			<?php  
				echo $value;
			?>
		</span>
	</div>
	<?php  
			endforeach;
		endif;
	?>
	<form action="" method="POST">
		<div class="heading">
			<p>
				Login Account
			</p>
		</div>
		<div class="options">
			<div class="input">
				<span>
					Tên đăng nhập:
				</span>
				<input type="text" name="username">
			</div>
			<div class="input">
				<span>
					Mật khẩu:
				</span>
				<input type="password" name="password">
			</div>
		</div>
		<div class="submit">
			<input type="submit" name="login" value="Đăng nhập">
		</div>
	</form>
</div>