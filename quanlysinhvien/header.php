<?php  
	session_start();
	if (!isset($_SESSION['admin']) && !isset($_SESSION['user'])) {
		header('Location:sinhvien.login.php');
	}
?>
<!--Style CSS -->
<link rel="stylesheet" type="text/css" href="css/header.css">
<!-- End Style CSS -->
<div class="container">
	<div class="header">
				<!-- Banner -->
		<div class="banner">
			<div class="logo">
				<img src="img/logo.jpg">
			</div>
			<div class="text">
				<h1>
					PHP Quản lý sinh viên
				</h1>
			</div>
		</div>
				<!-- Menu -->
		<div class="menu">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="khoa.php">Khoa</a></li>
				<li><a href="monhoc.php">Môn học</a></li>
				<li><a href="sinhvien.php">Sinh viên</a></li>
				<li><a href="ketqua.php">Kết quả</a></li>
				<li><a href="logout.php">Quit game</a></li>
			</ul>
		</div>
				<!-- End menu -->
	</div>
</div>