<?php  
	require 'database.php';
	require 'header.php';
?>
<?php  
	// Lấy danh sách khoa
	$sql 	= " SELECT * FROM khoa ";
	$query 	= $db->query($sql);
	$khoa 	= $query->fetch_all(MYSQLI_ASSOC);	
?>
<?php  
	// Validate Form
	$errors 	= [];
	$flag 		= 0;
	if (isset($_POST['submit'])) {
		# code...
		if (!isset($_POST['masv']) || $_POST['masv'] == '') {
		$errors[] 		= 'Vui lòng nhập Mã SV';
		}

		if (!isset($_POST['hoten']) || $_POST['hoten'] == '') {
			$errors[] 	= 'Vui lòng nhập Tên SV';
		}

		if (empty($_POST['ngaysinh'])) {
			$errors[] 	= 'Vui lòng nhập ngày sinh';
		}

		if (empty($_POST['diachi'])) {
			$errors[] 	= 'Vui lòng nhập địa chỉ';
		}

		if (empty($_POST['email'])) {
			$errors[] 	= 'Vui lòng nhập email';
		}

		if (count($errors) == 0) {
			# code...
			$masv 		= trim($_POST['masv']);
			$hoten 		= trim($_POST['hoten']);
			$ngaysinh 	= trim($_POST['ngaysinh']);
			$gioitinh 	= trim($_POST['gt']);
			$diachi 	= trim($_POST['diachi']);
			$email 		= trim($_POST['email']); 
			$makhoa 	= trim($_POST['makhoa']);

			// Kiểm tra masv đã có trong CSDL chưa ? nếu có -> errors
			$sql 		= " SELECT masv FROM sinhvien 
										WHERE masv = '{$masv}' ";
			$query 		= $db->query($sql);
			$result 	= $query->fetch_assoc();

			if (!is_null($result)) {
				$errors[] 	= ' Mã sinh viên đã tồn tại.';
			} else {
				$sql 	= " INSERT INTO sinhvien
							(
								masv,
								hoten,
								ngaysinh,
								gioitinh,
								diachi,
								email,
								makhoa
							)
							VALUES 
							(
								'{$masv}',
								'{$hoten}',
								'{$ngaysinh}',
								'{$gioitinh}',
								'{$diachi}',
								'{$email}',
								'{$makhoa}'
							)";

				$query 	= $db->query($sql);
				if ($query) {
					$flag = 1;
				} else {
					$errors = 'Xảy ra lỗi,không thêm được sinh viên.';
				}

			}
		}
	}
?>
			<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/edit.css">
			<!-- End style-->
<div class="container">
	<div class="message">
		<?php  
			if (count($errors) > 0) :
				foreach ($errors as $key => $value) :
		?>
		<p style="color: red;">
			<?php echo $value; ?>
		</p>
		<?php  
				endforeach;
			endif;
		?>
		<?php  
			if ($flag == 1) :
		?>
		<p style="color: blue;">
			Thêm thành công
		</p>
		<?php 
			endif; 
		?>
	</div>
	<div class="add">
		<form method="POST">
			<table border="1" cellpadding="10">
				<thead>
					<tr>
						<th colspan="2">
							Thêm sinh viên
						</th>
					</tr>
				</thead>
				<tr>
					<td>
						Mã sinh viên : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="masv">
					</td>
				</tr>
				<tr>
					<td>
						Họ tên: <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="hoten">
					</td>
				</tr>
				<tr>
					<td>
						Ngày sinh : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="ngaysinh">
					</td>
				</tr>
				<tr>
					<td>
						Giới tính : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="radio" name="gt" value="0">Nữ <br>
						<input type="radio" name="gt" value="1"> Nam
					</td>
				</tr>
				<tr>
					<td>
						Địa chỉ : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="diachi">
					</td>
				</tr>
				<tr>
					<td>
						Email : <span style="color: red;">(*)</span>
					</td>
					<td>
						<input type="text" name="email">
					</td>
				</tr>
				<tr>
					<td>
						Khoa : <span style="color: red;">(*)</span>
					</td>
					<td>
						<select name="makhoa">
							<option>
								<---Chọn--->
							</option>
							<?php
								if(count($khoa) > 0 && !is_null($khoa)):
									foreach ($khoa as $items):
							?>
							<option value="<?php echo $items['makhoa']; ?>">
								<?php echo $items['ten_khoa']; ?>
							</option>
							<?php  
									endforeach;
								endif;
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="submit" name="submit" 
								value="Thêm sinh viên" id="submit">
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
