<?php  
	session_start();
	if (isset($_SESSION['user'])) {
		header('Location:header.php');
	}

	if (isset($_SESSION['admin'])) {
		header('location:sinhvien.php');
	}

	if (!isset($_SESSION['user']) && !isset($_SESSION['admin'])) {
		header('Location:sinhvien.login.php');
	}
?>