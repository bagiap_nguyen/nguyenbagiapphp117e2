<?php

	require 'database.php';
	require 'header.php';

	if (!isset($_GET['submitSearch'])) {
		header('Location:sinhvien.php');
	}
	

	$sql 		= "SELECT * FROM sinhvien WHERE 1=1";
	$message 	= '';

	if (isset($_GET['masv']) && $_GET['masv'] != '') {
		$sql .= " AND masv  LIKE '%" . trim($_GET['masv']) . "%'";
	}

	if (isset($_GET['hoten']) && $_GET['hoten'] != '') {
		$sql .= " AND hoten  LIKE '%" . trim($_GET['hoten']) . "%'";
	}

	if (isset($_GET['email']) && $_GET['email'] != '') {
		$sql .= " AND email  LIKE '%" . trim($_GET['email']) . "%'";
	}

	$query 		= $db->query($sql);
	$result 	= $query->fetch_all(MYSQLI_ASSOC);

	if (count($result) == 0) {
		$message 			= 'Không tìm thấy sinh viên nào.';
		$_SESSION['search'] = $message;
		header('Location:sinhvien.php');
	}
?>
<!-- Style -->
<link rel="stylesheet" type="text/css" href="css/khoa.css">
<!-- Style -->
<div class="container">
	<?php  
		if (!is_null($result) && count($result) > 0) :
	?>
	<div class="sinhvien">
		<table border="1" cellpadding="10">
			<thead>
				<tr>
					<th colspan="8">
						<h3>
							Kết quả tìm kiếm
						</h3>
					</th>
				</tr>
				<tr>
					<th>
						Mã SV
					</th>
					<th>
						Họ tên
					</th>
					<th>
						Giới tính
					</th>
					<th>
						Địa chỉ
					</th>
					<th>
						Khoa
					</th>
					<th colspan="3">
						Tùy chọn
					</th>
				</tr>
			</thead>
			<tbody>
				<?php  
					foreach ($result as $sv) :
				?>
				<tr>
					<td>
						<?php echo $sv['masv']; ?>
					</td>
					<td>
						<?php echo $sv['hoten']; ?>
					</td>

					<td>
						<?php  
							if ($sv['gioitinh'] == "1") {
								echo "Nam";
							} else {
								echo "Nữ";
							}
						?>
					</td>
					<td>
						<?php echo $sv['diachi']; ?>
					</td>
					<td>
						<?php echo $sv['makhoa']; ?>
					</td>
					<td>
						<a href="sinhvien.edit.php?id=<?php echo $sv['masv']; ?> ">
							Edit
						</a>
					</td>
					<td>
						<a href="sinhvien.delete.php?id=<?php echo $sv['masv']; ?> " id="active">
							Delete
						</a>
					</td>
					<td>
						<a href="sinhvien.diem.php?id=<?php echo $sv['masv'];?> ">
							Xem điểm
						</a>
					</td>
					
				</tr>
				<?php  
					endforeach;
				?>
			</tbody>
		</table>
	<?php  
		endif;
	?>
	</div>
</div>