<?php  
	session_start();

	if (isset($_SESSION['user'])) {
		header('Location:header.php');
	}

	require 'database.php';

	$error = [];

	if (isset($_POST['login'])) {
		#Validate form
		if (empty($_POST['masv'])) {
			$error[] 	= 'Vui lòng điền tên đăng nhập.';
		}

		if (empty($_POST['password'])) {
			$error[] 	= 'Vui lòng nhập mật khẩu.';
		}

		
		## Nhaạp hết rồi thì ??
		if (count($error) == 0) {

			if (strlen($_POST['password']) <= 4 ) {
				$error[] 	= 'Mật khẩu không hợp lệ';
			}

			$username 		= trim($_POST['masv']);
			$password 		= $_POST['password'];

			$sql 			= " SELECT * FROM sinhvien 
				WHERE masv  = '{$username}' LIMIT 1 " ;

			$query			= $db->query($sql);

			$acc 	 		= $query->fetch_assoc();

			if (!is_null($acc) && count($acc) > 0) {

				if ($acc['password'] == $password) {

					// Kiểm tra role

					$_SESSION['user'] 			= $acc;

					$_SESSION['flash_message'] 	= 'Hello' . '&nbsp;'. $_SESSION['user']['hoten'];

					header('Location:xuly.login.php');
				} else {
					$error[] 			= 'Password không chuẩn.';
				}
				
			} else {

				$error[] 				= 'Tên đăng nhập không chính xác.';
			}	
		}
	}
?>
<!-- Style -->
<style type="text/css">
	.login {
		width: 1000px;
		margin: 0px auto;
		padding: 0px;
	}
	.login form {
		display: block;
		width: 600px;
	}
	.login .heading {
		background: blue;
		height: 40px;
	}
	.heading p {
		text-align: center;
		color: #fff;
		line-height: 40px;
		font-size: 26px;
		font-weight: bold;
	}
	.input {
		padding-top: 20px;
	}
	.input input {
		width: 250px;
		height: 30px;
	}
	.options {
		background: #ccc;
		min-height: 110px;
	}
	form .options .input  span {
		font-weight: bold;
		font-size: 21px;
		float: left;
		width: 200px;
		text-indent: 40px;
	}
	.submit {
		background: #ccc;
		display: block;
		text-align: left;
		text-indent: 50px;
		height: 45px;
	}
	.submit input {
		height: 40px;
		background: blue;
		color: #fff;
		text-align: center;
		line-height: 40px;
	}
</style>
<!-- End style -->


<div class="login">
	<?php  
		if (count($error) > 0) :
			foreach ($error as $key => $value) :
	?>
	<div class="message">
		<span style="color: red;">
			<?php  
				echo $value;
			?>
		</span>
	</div>
	<?php  
			endforeach;
		endif;
	?>
	<form action="" method="POST">
		<div class="heading">
			<p>
				Login Account
			</p>
		</div>
		<div class="options">
			<div class="input">
				<span>
					Tên đăng nhập:
				</span>
				<input type="text" name="masv">
			</div>
			<div class="input">
				<span>
					Mật khẩu:
				</span>
				<input type="password" name="password">
			</div>
			<div class="input">
				<p align="right">
					<a href="admin.login.php">Đăng nhập với tư cách admin</a>
				</p>
			</div>
		</div>
		<div class="submit">
			<input type="submit" name="login" value="Đăng nhập">
		</div>
	</form>
</div>