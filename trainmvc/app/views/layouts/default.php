<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Admin</title>
    <meta charset="utf-8">
</head>

<body>
    <header id="header">
        <?php  
            require $header;
        ?>
    </header>
    <div id="content">
        <?php  
            require $content;
        ?>
    </div>
    <footer id="footer">
        <?php  
            require $footer;
        ?>
    </footer>
</body>

</html>
