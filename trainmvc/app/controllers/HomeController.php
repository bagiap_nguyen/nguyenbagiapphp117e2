<?php  
	require_once 'AppController.php';
	/**
	 * Home
	 */
	class HomeController extends AppController
	{
		
		public function __construct()
		{
			
		}

		public function index() {

			return $this->view('home.index');
			
		}
	}
?>