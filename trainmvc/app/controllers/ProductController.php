<?php  
	require_once 'AppController.php';

	require_once MODEL_PATH . 'Product.php';
	/**
	 * 
	 */
	class ProductController extends AppController
	{
		protected $productModel;

		function __construct()
		{
			$this->productModel = new Product;
		}

		public function index()
		{
			$check = $this->productModel->test();

			$data = [
				'data'=> $check
			];

			return $this->view('product.index',$data);
		}
	}
?>