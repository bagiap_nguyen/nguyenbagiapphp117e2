<?php  
	/**
	 * App
	 */
	class AppController 
	{
		
		public function __construct()
		{
			# code...
		}

		public function view($view = '', $data = [])
		{

			foreach ($data as $key => $items) {
				$$key = $items;
			}

			$header = VIEW_PATH . 'layouts' . DIRECTORY_SEPARATOR .'header.php';


			$view = str_replace('.', DIRECTORY_SEPARATOR, $view);


			$content = VIEW_PATH . $view . '.php';


			if (!file_exists($content)) {
				$content = VIEW_PATH 	. 'layouts' . DIRECTORY_SEPARATOR
										.'error.php';
			}

			$footer = VIEW_PATH . 'layouts' . DIRECTORY_SEPARATOR . 'footer.php';



			require VIEW_PATH . 'layouts'	.  DIRECTORY_SEPARATOR 
											.'default.php';
		}
	}
?>