<?php  
	require_once 'Routes.php';
	/**
	 * App
	 */
	class App 
	{
		private $routes;

		public function __construct()
		{
			$this->routes = new Routes;
			$this->routes->map();
		}

	}


?>