<?php  
	/**
	 * Routes
	 * Example: index.php?controller=...&action=...&params=..
	 */
	class Routes
	{

		function __construct()
		{
			# code...
		}

		private function getRequestUrl()
		{
			// get Controller
			if (isset($_GET['controller'])) {

				$controller = $_GET['controller'];
			} else {
				$controller = 'home';
			}

			// get action
			if (isset($_GET['action'])) {

				$action = $_GET['action'];
			} else {
				$action = 'index';
			}

			// array request
			$request = [
				'controller' => $controller,
				'action' 	 => $action
			];

			return $request;
		}

		private function process()
		{
			$request 	= $this->getRequestUrl();

			$controller = ucfirst($request['controller']) . 'Controller';

			$action 	= $request['action'];

			$result 	= [
				'controller'	=> $controller,
				'action' 		=> $action
			];

			return $result;
		}

		public function map()
		{
			$request = $this->process();

			foreach ($request as $key => $value) {
				$$key = $value;
			}

			$url = dirname(__DIR__) . DIRECTORY_SEPARATOR .  "controllers";

			$url = $url . DIRECTORY_SEPARATOR . $controller . '.php';

			if (file_exists($url)) {

				require $url;

				$tem = new $controller;

				$tem->$action();
			}
			
		}

	}

?>