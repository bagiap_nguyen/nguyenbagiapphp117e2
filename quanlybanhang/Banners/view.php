<?php  
	require '../Layouts/index.php';

	$id 			= $_GET['id'];

	$banner 		= getOneRecord('*','banners',"id = '{$id}'");

	if (is_null($banner)) {
		header('Location:index.php');
		exit();
	}
?>

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Banner
                            <small>View</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Img</th>
                                <th>Link</th>
                                <th>Position</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX" align="center">
                                <td>
                                	<?php  
                                		echo $banner['id'];
                                	?>
                                </td>
                                <td>
                                	<?php  
                                		echo $banner['name'];
                                	?> 
                                </td>
                                <td>
                                	<?php  
                                		echo $banner['img'];
                                	?>
                                </td>
                                <td>
                                	<?php  
                                		echo $banner['link'];
                                	?>
                                </td>
                                <td class="center">
                                	<?php  
                                		echo $banner['position'];
                                	?>
                                </td>
                                <td class="center">
                                	<?php  
                                		 if ($banner['status'] == 1) {
                                		 	echo "Hiện";
                                		 } else {
                                		 	echo "Ẩn";
                                		 }
                                	?>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                    <div class="form-group">
                        <label>Descriptions</label>
                        <p class="form-control" rows="3" name=""><?php echo $banner['description']; ?></p>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->