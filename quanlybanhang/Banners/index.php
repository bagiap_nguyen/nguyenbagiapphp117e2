<?php  
	require '../Layouts/index.php';

	$banners = getAllData('id,img,name,link','banners','','');

?>

<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Banner
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <?php  
                    	if (!is_null($banners) && count($banners) > 0) :
                    ?>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>
                                <th>IMG</th>
                                <th>Link</th>
                                <th>View</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php  
                        		foreach ($banners as $item) :
                        	?>
                            <tr class="odd gradeX" align="center">
                                <td>
                                	<?php  
                                		echo $item['id'];
                                	?>
                                </td>

                                <td>
                                	<?php  
                                		echo $item['name'];
                                	?>
                                </td>

                                <td>
                                	<?php  
                                		echo $item['img'];
                                	?>
                                </td>

                                <td>
                                	<?php  
                                		echo $item['link'];
                                	?>
                                </td>

                                <td>
                                	<a href="view.php?id=<?php echo $item['id']; ?>">View</a>
                                </td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
                                	<a href="delete.php?id=<?php echo $item['id']; ?>">Delete</a>
                                </td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> 
                                	<a href="edit.php?id=<?php echo $item['id']; ?>">Edit</a>
                                </td>
                            </tr>
                            <?php  
                            	endforeach;
                            ?>
                        </tbody>
                    </table>
                    <?php  
                    	else:
                    ?>

                    <p>
                    	Chưa có dữ liệu.
                    </p>
                    <?php  
                    	endif;
                    ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->