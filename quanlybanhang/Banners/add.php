<?php  
	require '../Layouts/index.php';


	$errors 	= [];
	if (isset($_POST['submit'])) {
		
		if (!isset($_POST['name']) || empty($_POST['name'])) {
			$errors[] = 'Vui lòng nhập tên.';
		}

		if (!isset($_POST['status'])) {
			$errors[] = 'Vui lòng chọn trạng thái.';
		}

		if (count($errors) == 0) {

			$name 			= $_POST['name'];

			$link 			= $_POST['link'];

			$description 	= $_POST['description'];

			$img 			= $_POST['img'];

			$position 		= $_POST['position'];

			$status 		= $_POST['status'];

			// INSERT

			$colums 		= ['name', 'img', 'link', 'description', 
			'position', 'status'];

			$data 			= ["'{$name}'","'{$img}'","'{$link}'",
            "'{$description}'","'{$position}'","'{$status}'"];

			$insert 		= insertData('banners',$colums,$data);

			if ($insert == true) {
				$success = 'Thêm thành công.';
			} else {
				$errors[] = 'Xảy ra lỗi, thêm thất bại.';
			}
		}
	}
?>

<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Banner
                        <small>Add</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    <form action="" method="POST">

                    	<div class="form-group">
                                <?php  
                                    if (isset($success)) :
                                ?>
                                <label style="color: green;">
                                    <?php echo $success; ?>
                                    
                                </label>
                                <?php  
                                    endif;
                                ?>
                                <?php  
                                    if (count($errors) > 0) :
                                        foreach ($errors as $key => $value) :
                                ?>
                                <p style="color: red;">
                                   <?php echo $value; ?> 
                                </p>
                                <?php  
                                        endforeach;
                                    endif;
                                ?>
                        </div>
                        
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" name="name"  />
                        </div>

                        <div class="form-group">
                            <label>Link</label>
                            <input class="form-control" name="link" />
                        </div>

                        <div class="form-group">
                            <label>Images</label>
                            <input type="file" name="img">
                        </div>

                        
                        <div class="form-group">
                            <label>Positions</label>
                            <input class="form-control" name="position" />
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <label class="radio-inline">
                                <input name="status" value="1" type="radio">Visible
                            </label>
                            <label class="radio-inline">
                                <input name="status" value="0" type="radio" checked="" >Invisible
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default" name="submit">Add</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
<!-- /#page-wrapper -->