<?php  
	require '../Layouts/index.php';

	$id 		= $_GET['id'];

	$banners 	= getOneRecord('*','banners',"id = '{$id}'");

	if ($id == null || count($banners) == 0) {
		header('Location:index.php');
		exit;
	}


	$errors 		= [];
	if (isset($_POST['submit'])) {
		
		if (!isset($_POST['id'])) {
            $errors[]   = 'ID không được sửa.';
        }
         
        if (!isset($_POST['name']) || empty($_POST['name'])) {
            $errors[]   = 'Vui lòng nhập tên banners.';
        }

        if (!isset($_POST['status'])) {
            $errors[]   = 'Vui lòng nhập trạng thái banners.';
        }

        $check_id       = getOneRecord('id','banners',"id = '{$_POST['id']}' AND id <> '{$id}' ");

        if (!is_null($check_id)) {
            $errors[]   = 'Banners có id đã tồn tại  .';
        }

        if (count($errors) == 0) {
        	
        	$name 			= $_POST['name'];

			$link 			= $_POST['link'];

			$description 	= $_POST['description'];

			$img 			= $_POST['img'];

			$position 		= $_POST['position'];

			$status 		= $_POST['status'];

			$colums 		= ['name', 'img', 'link', 'description', 
			'position', 'status'];

			$data 			= ["'{$name}'","'{$img}'","'{$link}'",
            "'{$description}'","'{$position}'","'{$status}'"];

            $updated 		= [];

            for ($i=0; $i < count($colums) ; $i++) { 

                $updated[$colums[$i]] = $data[$i];
            }

            $updatedTo 		= updateData('banners',$updated," id ='{$id}'");

            if ($updatedTo) {
            	$success = 'Success!';
            } else {
            	$errors[] = 'Failed';
            }
        }
	}
?>

<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Banner
                        <small>Edit</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    <form action="" method="POST">

                    	<div class="form-group">
                                <?php  
                                    if (isset($success)) :
                                ?>
                                <label style="color: green;">
                                    <?php echo $success; ?>
                                    
                                </label>
                                <?php  
                                    endif;
                                ?>
                                <?php  
                                    if (count($errors) > 0) :
                                        foreach ($errors as $key => $value) :
                                ?>
                                <p style="color: red;">
                                   <?php echo $value; ?> 
                                </p>
                                <?php  
                                        endforeach;
                                    endif;
                                ?>
                        </div>

                        <div class="form-group">

                            <label>ID</label>
                            <input class="form-control" name="id" value="<?php 
                                if(isset($_POST['id'])) echo $_POST['id'];
                                    else echo $banners['id'];
                            ?>" readonly/>
                        </div>
                        
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" name="name" value="<?php 
                                if(isset($_POST['name'])) echo $_POST['name'];
                                    else echo $banners['name'];
                            ?>" />
                        </div>

                        <div class="form-group">
                            <label>Link</label>
                            <input class="form-control" name="link" value="<?php 
                                if(isset($_POST['link'])) echo $_POST['link'];
                                    else echo $banners['link'];
                            ?>" />
                        </div>

                        <div class="form-group">
                            <label>Images</label>
                            <p name= 'img' >
                            	<?php  
                            		echo $banners['img'];
                            	?>
                            </p>
                        </div>

                        <div class="form-group">
                            <label>Updated Images</label>
                            <input type="file" name="img">
                        </div>

                        
                        <div class="form-group">
                            <label>Positions</label>
                            <input class="form-control" name="position" value="<?php 
                                if(isset($_POST['position'])) echo $_POST['position'];
                                    else echo $banners['position'];
                            ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control" rows="3" name="description">
                            <?php 
                                if(isset($_POST['description'])) echo $_POST['description'];
                                    else echo $banners['description'];
                            ?>"
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label> Status</label>
                            <label class="radio-inline">
                                <input name="status" value="1" type="radio" <?php if (isset($_POST['status']) && $_POST['status'] == 1 || $banners['status'] == 1 )  {
                                    echo "checked = 'checked'";
                                } ?>>Show
                            </label>
                            <label class="radio-inline">
                                <input name="status" value="0" type="radio" <?php if (isset($_POST['status']) && $_POST['status'] == 0 || $banners['status'] == 0 )  {
                                    echo "checked = 'checked'";
                                } ?>>Hidden
                            </label>
                        </div>

                        <button type="submit" class="btn btn-default" name="submit">Edit</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
<!-- /#page-wrapper -->