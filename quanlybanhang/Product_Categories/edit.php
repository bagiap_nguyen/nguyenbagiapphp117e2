<?php  
	require '../Layouts/index.php';

	$id = $_GET['id'];

	if (!isset($id)) {
		header("Location:http://localhost:8888/project/nguyenbagiapphp117e2/quanlybanhang/Product_Categories");
		exit();
	}

	$table 			= 'product_categories';

	$categories 	= getOneRecord('*',$table,"id = '{$id}'");

	if (is_null($categories)) {
		header("Location:http://localhost:8888/project/nguyenbagiapphp117e2/quanlybanhang/Product_Categories");
		exit();
	}

	$parent_id_categories   = getAllData('id,name',$table,'','');

	$errors =[];
	if (isset($_POST['submit'])) {
		//validate
		if (!isset($_POST['name'])) {
			$errors[] = 'Vui lòng nhập tên danh mục.';
		}

		if (!isset($_POST['status'])) {
			$errors[] = 'Vui lòng chọn trạng thái danh mục';
		}

		if (count($errors) == 0) {

			$id_cate 		= $_POST['id'];
			
			$name 			= trim($_POST['name']);

			$parent_id 		= $_POST['parent_id'];
			$slug 			= slugify($name);
			$description 	= trim($_POST['description']);

			$img 			= $_POST['img'];

			$meta_title 	= trim($_POST['meta_title']);

			$meta_keyword 	= trim($_POST['meta_keyword']);

			$meta_description = trim($_POST['meta_description']);

			$status 		  = $_POST['status'];

			$check_id       = getOneRecord('id',$table,"id = '{$id_cate}' AND id <> '{$id}' ");

			if (!is_null($check_id)) {
				$errors[] 	= 'Danh mục có id đã tồn tại.';
			} else {
				$check_slug     = getOneRecord('slug',$table,"slug ='{$slug}' AND id <> '{$id}' ");

				if ($check_slug) {
					$errors[] = 'Danh mục có slug bị trùng. Hãy đổi lại tên.';
				} else {
					$colums 	= ['name', 'slug', 'description', 'img', 'parent_id', 'meta_title', 'meta_keyword', 'meta_description', 'status'];
					$data 		= ["'{$name}'","'{$slug}'","'{$description}'","'{$img}'",
						"'{$parent_id}'","'{$meta_title}'","'{$meta_keyword}'",
						"'{$meta_description}'","'{$status}'"];

					$updated = [];

                    for ($i=0; $i < count($colums) ; $i++) { 
                        $updated[$colums[$i]] = $data[$i];
                    }

                    $where = "id ='{$id}'";

                    $updated_record = updateData($table,$updated,$where);

                    if ($updated_record) {
                        $success = 'Cập nhật thành công!';
                    } else {
                        $errors[] = 'Xảy ra lỗi';
                    }

				}
			}

		}
	}

?>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Category
                    <small>Add</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <form action="" method="POST">

                    <div class="form-group">
                        <?php  
                            if (count($errors) > 0) :
                                foreach ($errors as $key => $value) :
                        ?>
                        <label style="color: red; font-size: 21px;">
                            <?php echo $value; ?>
                        </label>
                        <?php 
                                endforeach; 
                            endif;
                        ?>

                        <?php  
                            if (isset($success)) :
                        ?>
                        <p style="color: blue;font-size: 22px;">
                            <?php echo $success; ?>
                        </p>
                        <?php  
                            endif;
                        ?>
                    </div>
                    
                    <div class="form-group">

                            <label>ID</label>
                            <input class="form-control" name="id" value="<?php 
                                    if(isset($_POST['id'])) echo $_POST['id'];
                                        else echo $categories['id'];
                                ?>" readonly/>
                        </div>
                    
                    <div class="form-group">
                        <label>Danh mục cha</label>
                        <select class="form-control" name="parent_id">
                            <option <?php if(!isset($_POST['parent_id'])&& $categories['parent_id'] == null) echo "checked ='checked'"; ?>>
                            	
                            </option>
                            <?php 
                                if (!is_null($parent_id_categories)) : 
                                    foreach ($parent_id_categories as $item) :
                            ?>
                            <option value="<?php echo $item['id']; ?>" 
                            	<?php 
                                    $check = 'selected="selected"';
                                    if (
                                        (isset($_POST['parent_id'])&& $_POST['parent_id'] == $item['id']) 
                                        || $categories['parent_id'] == $item['id']) echo $check;
                                ?>
                            	>
                                <?php echo $item['name']; ?>
                            </option>
                            <?php  

                                    endforeach;
                                endif;
                            ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Category Name</label>
                        <input class="form-control" name="name" placeholder="Please Enter Category Name" 
                        value="<?php 
                                    if(isset($_POST['name'])) echo $_POST['name'];
                                        else echo $categories['name'];
                                ?>" />
                    </div>
                    
                    <div class="form-group">
                        <label>Category Description</label>
                        <textarea class="form-control" rows="3" name="description"><?php 
                                    if(isset($_POST['description'])) echo $_POST['description'];
                                        else echo $categories['description'];
                                ?></textarea>
                    </div>

                    <div class="form-group">
                        
                        <?php  
                        	if ($categories['img'] != null) :
                        ?>
                        <label><?php echo $categories['img'];?></label>
                        <br>
                        <label>Cập nhật</label>
                        <?php  
                        	endif;
                        ?>

                        <input type="file" name="img"><br>
                    </div>

                    <div class="form-group">
                        <label>Category Status</label>
                        <label class="radio-inline">
                            <input name="status" value="0" checked="" type="radio">Invisible
                        </label>
                        <label class="radio-inline">
                            <input name="status" value="1" type="radio">Visible
                        </label>
                    </div>

                    <div class="form-group">
                        <label>Meta title</label>
                        <input class="form-control" name="meta_title" value="<?php 
                                    if(isset($_POST['meta_title'])) echo $_POST['meta_title'];
                                        else echo $categories['meta_title'];
                                ?>"/>
                    </div>

                    <div class="form-group">
                        <label>Meta Keywords</label>
                        <input class="form-control" name="meta_keyword" value="<?php 
                                    if(isset($_POST['meta_keyword'])) echo $_POST['meta_keyword'];
                                        else echo $categories['meta_keyword'];
                                ?>" />
                    </div>

                    <div class="form-group">
                        <label>Meta Description</label>
                        <input class="form-control" name="meta_description" value="<?php 
                                    if(isset($_POST['meta_description'])) echo $_POST['meta_description'];
                                        else echo $categories['meta_description'];
                                ?>"/>
                    </div>

                    <button type="submit" class="btn btn-default" name="submit">Category Add</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>