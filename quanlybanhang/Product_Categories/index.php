<?php  
	require '../Layouts/index.php';


	$table 			= 'product_categories';
	$x 				= 1;

	$categories 	= getAllData('id,name,parent_id,status',$table,'','');

	//var_dump($categories);
	function showCate($categories = [],$parent_id = 0){
		$result = '';
		foreach ($categories as $value) {
			if ( isset($value['id']) && $value['id'] == $parent_id) {
				$result = $value['name'];
			}
		}

		return $result;
	}
?>

<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Category
                        <small>List</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <?php  
                	if (is_null($categories)) :
                ?>
                <h1>
                	Không có dữ liệu.
                </h1>
                <?php  
                	else:
                ?>
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr align="center">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Category Parent</th>
                            <th>Status</th>
                            <th>View</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php  
                    		foreach ($categories as $item) :
                    			if ($x % 2 == 1) :
                    	?>

                        <tr class="odd gradeX" align="center">
                            <td>
                            	<?php echo $item['id']; ?>
                            </td>
                            <td>
                            	<?php echo $item['name']; ?>
                            </td>
                            <td>
                            	<?php  
                            		if ($item['parent_id'] == 0) {
                            			echo "None";
                            		} else {
                            			foreach ($categories as $key => $value) {
                            				if ($value['id'] == $item['parent_id']) {
                            					echo $value['name'];
                            				}
                            			}
                            		}

                            	?>
                            </td>
                            <td>
                            	<?php  
                            		if ($item['status'] == 1) {
                            			echo "Hiện";
                            		} else {
                            			echo "Ẩn";
                            		}
                            	?>
                            </td>

                            <td class="center">
	                        	<i class="fa fa-eye fa-fw"></i>
	                         	<a href="view.php?id=<?php echo $item['id']; ?>"> 
	                         		View
	                         	</a>
	                     	</td>
                            <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="delete.php?id=<?php echo $item['id']; ?>"> Delete</a></td>
                            <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="edit.php?id=<?php echo $item['id']; ?>">Edit</a></td>
                        </tr>
                        

                        <?php  		
                        			endif;
                        		endforeach;
                        ?>
                    </tbody>
                </table>

                <?php  
                	endif;
                ?>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
 <!-- /#page-wrapper -->