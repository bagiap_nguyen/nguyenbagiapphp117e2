<?php  
	require '../Layouts/index.php';

    $table                  = "product_categories";

    $parent_id_categories   = getAllData('id,name',$table,'','');

    $errors = [];
    if (isset($_POST['submit'])) {
        // Validate 
         //var_dump($_POST);
        if (!isset($_POST['name'])) {
            $errors[]       = 'Vui lòng nhập tên danh mục.';
        }

        if (count($errors) == 0) {

            // Trong form:
            $name           = trim($_POST['name']);

            $parent_id      = trim($_POST['parent_id']);

            $description    = trim($_POST['description']);

            $status         = $_POST['status'];

            $meta_title     = trim($_POST['meta_title']);

            $meta_keyword   = trim($_POST['meta_keyword']);

            $meta_description = trim($_POST['meta_description']);

            $img              = $_POST['img'];

            // Tự động sinh: slug
            
            $slug              = slugify($name);

            $check_slug        = getOneRecord('slug',$table,"slug = '{$slug}'");

            if ($check_slug) {
                $errors[]      = 'Tên danh mục đã tồn tại';
            } else {
                $colums = ['name','slug', 'description', 'img', 
                'parent_id', 'meta_title', 'meta_keyword',
                 'meta_description',
                 'status'];

                $data   = ["'{$name}'","'{$slug}'","'{$description}'","'{$img}'","'{$parent_id}'","'{$meta_title}'","'{$meta_keyword}'","'{$meta_description}'","'{$status}'"];
                $insert     = insertData($table,$colums,$data);

                if ($insert) {
                    $success    = 'Thêm thành công!';
                } else {
                    $errors[]   = 'Xảy ra lỗi,không thể thêm.';
                }
            }


        }
    }
?>
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Category
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST">

                            <div class="form-group">
                                <?php  
                                    if (count($errors) > 0) :
                                        foreach ($errors as $key => $value) :
                                ?>
                                <label style="color: red; font-size: 21px;">
                                    <?php echo $value; ?>
                                </label>
                                <?php 
                                        endforeach; 
                                    endif;
                                ?>

                                <?php  
                                    if (isset($success)) :
                                ?>
                                <p style="color: blue;font-size: 22px;">
                                    <?php echo $success; ?>
                                </p>
                                <?php  
                                    endif;
                                ?>
                            </div>
                            
                            <div class="form-group">
                                <label>Danh mục cha</label>
                                <select class="form-control" name="parent_id">
                                    <option></option>
                                    <?php 
                                        if (!is_null($parent_id_categories)) : 
                                            foreach ($parent_id_categories as $item) :
                                    ?>
                                    <option value="<?php echo $item['id']; ?>">
                                        <?php echo $item['name']; ?>
                                    </option>
                                    <?php  

                                            endforeach;
                                        endif;
                                    ?>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label>Category Name</label>
                                <input class="form-control" name="name" placeholder="Please Enter Category Name" />
                            </div>
                            
                            <div class="form-group">
                                <label>Category Description</label>
                                <textarea class="form-control" rows="3" name="description"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Images</label>
                                <input type="file" name="img"><br>
                                <input type="checkbox" name="img_featured" value="1"> Chọn làm ảnh đại diện.
                            </div>

                            <div class="form-group">
                                <label>Category Status</label>
                                <label class="radio-inline">
                                    <input name="status" value="0" checked="" type="radio">Invisible
                                </label>
                                <label class="radio-inline">
                                    <input name="status" value="1" type="radio">Visible
                                </label>
                            </div>

                            <div class="form-group">
                                <label>Meta title</label>
                                <input class="form-control" name="meta_title"/>
                            </div>

                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <input class="form-control" name="meta_keyword" />
                            </div>

                            <div class="form-group">
                                <label>Meta Description</label>
                                <input class="form-control" name="meta_description" />
                            </div>

                            <button type="submit" class="btn btn-default" name="submit">Category Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->