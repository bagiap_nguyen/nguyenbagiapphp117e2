<?php  
    function deleteProduct($id){

        if ($id == null) {
            return false;
        }

        $product = getOneRecord('*','products'," id = '{$id}'");

        if (is_null($product)) {
            return false;
        }

        $check_relate       = getAllData('*','product_relates',
                                "AND product_id = '{$id}'",'');

        if (!is_null($check_relate)) {
            $del_pro_relate     = deleteData('product_relates',
                                    "product_id = '{$id}'");
        }


        $check_img          = getAllData('*','images',
                                " AND product_id = '{$id}'",'');
        

        if (!is_null($check_img)) {
            $del_pro_img        = deleteData('product_images',
                                    "product_id = '{$id}'");
        }

        $check_review       = getAllData('*','product_reviews',
                                    " AND product_id = '{$id}'",'');

        if (!is_null($check_review)) {
            $del_pro_review     = deleteData('product_reviews',
                                        "product_id = '{$id}'");
        }

        $check_order        = getAllData('*','order_items',
                                        " AND product_id = '{$id}'",'');

        if (!is_null($check_order)) {
            $del_pro_order      = deleteData('order_items',"product_id = '{$id}'");
        }

        $del_product    = deleteData('products',"id = '{$id}' ");

        if ($del_product) {
            return true;
        } else {
            return false;
        }

    }
?>