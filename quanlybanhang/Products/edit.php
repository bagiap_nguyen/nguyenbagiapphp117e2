
<?php  
	require '../Layouts/index.php';

	$id = $_GET['id'];

	if ($id == null) {
        header('Location:index.php');
        exit();
    }

    $brands     = getAllData('*','brands','','');

    $categories = getAllData('*','product_categories','','');

    $products   = getOneRecord('*','products',"id = '{$id}'");

    if (is_null($products)) {
        header('Location:index.php');
        exit();
    }


    $errors     = [];
    $table  = 'products';
    if (isset($_POST['submit'])) {
        // Validate form
        //var_dump($_POST); 

        if (!isset($_POST['id'])) {
            $errors[]   = 'ID không được sửa.';
        }
         
        if (!isset($_POST['name'])) {
            $errors[]   = 'Vui lòng nhập tên sản phẩm.';
        }

        if (!isset($_POST['sku'])) {
            $errors[]   = 'Vui lòng nhập mã sản phẩm.';
        }

        if (!isset($_POST['brand']) || $_POST['brand'] == 0) {
            $errors[]   = 'Vui lòng chọn thương hiệu.';
        }

        if (!isset($_POST['category'])|| $_POST['category'] == 0) {
            $errors[]   = 'Vui lòng chọn danh mục.';
        }

        if (count($errors) == 0) {

            if (isset($_POST['is_new'])) {
                $is_new =$_POST['is_new'];
            } else {
                $is_new = 0;
            }

            if (isset($_POST['is_sale'])) {
                $is_sale = $_POST['is_sale'];
            } else {
                $is_sale = 0;
            }

            if (isset($_POST['is_featured'])) {
                $is_featured = $_POST['is_featured'];
            } else {
                $is_featured = 0;
            }

            if (isset($_POST['is_promo'])) {
                $is_promo = $_POST['is_promo'];
            } else {
                $is_promo = 0;
            }

            if (isset($_POST['img_featured'])) {
                $img_featured = $_POST['img_featured'];
            } else {
                $img_featured = 0;
            }

            $id_sp          = $_POST['id'];

            $name           = $_POST['name'];
            $sku            = $_POST['sku'];
            $brand          = $_POST['brand'];
            $category       = $_POST['category'];
            $price          = $_POST['price'];
            $colors         = $_POST['colors'];
            $size           = $_POST['size'];
            $qty            = $_POST['qty'];
            $description    = $_POST['description'];
            $content        = $_POST['content'];
            $status         = $_POST['status'];


            // table img_product
            $img            = trim($_POST['img']);
            $img_featured   = trim($_POST['name']);
            // end table img_product
            // 
            // created_slug
            $slug           = slugify($name);
            // updated_at:
            $updated_at     = date("Y-m-d");
            
            $check_id       = getOneRecord('id',$table,"id = '{$id_sp}' AND id <> '{$id}' ");

            if (!is_null($check_id)) {
                $errors[]   = 'Sản phẩm có id đã tồn tại  .';
            } else {

                $check_slug     = getOneRecord('slug',$table,"slug ='{$slug}' AND id <> '{$id}' ");

                $check_sku      = getOneRecord('sku',$table,"sku ='{$sku}' AND id <> '{$id}' ");

                if ($check_sku || $check_slug) {
                    $errors[]   = 'Slug hoặc sku bị trùng.';
                } else {
                    $colums = ['sku','name','slug','price','colors',
                    'size',
                    'qty','brand_id','product_category_id','description',
                    'content','is_new','is_promo','is_featured','is_sale',
                    'updated_at','status'];
        
                    $data = ["'{$sku}'","'{$name}'","'{$slug}'","'{$price}'",
                    "'{$colors}'","'{$size}'","'{$qty}'","'{$brand}'",
                    "'{$category}'","'{$description}'","'{$content}'",
                    "'{$is_new}'","'{$is_promo}'","'{$is_featured}'",
                    "'{$is_sale}'","'{$updated_at}'",
                    "'{$status}'"];
                    
                    $updated = [];

                    for ($i=0; $i < count($colums) ; $i++) { 
                        $updated[$colums[$i]] = $data[$i];
                    }

                    $where = "id ='{$id}'";

                    $updated_record = updateData($table,$updated,$where);

                    if ($updated_record) {
                        $success = 'Cập nhật thành công!';
                    } else {
                        $errors[] = 'Xảy ra lỗi';
                    }
                }

                
            }
                   
        }
    }
?>
<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Product
                        <small>Edit</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    <form action="" method="POST">

                        <div class="form-group">
                            <?php  
                                if (count($errors) > 0) :
                                    foreach ($errors as $key => $value) :
                            ?>
                            <label style="color: red; font-size: 21px;">
                                <?php echo $value; ?>
                                
                            </label>

                            <?php  
                                    endforeach;
                                endif;
                            ?>
                            <?php  
                                if (isset($success)) :
                            ?>
                            <p style="color: blue;font-size: 21px;">
                                <?php echo $success; ?>
                            </p>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">

                            <label>ID</label>
                            <input class="form-control" name="id" value="<?php 
                                    if(isset($_POST['id'])) echo $_POST['id'];
                                        else echo $products['id'];
                                ?>" readonly/>
                        </div>

                        <div class="form-group">
                            <label>Tên sản phẩm</label>
                            <input class="form-control" name="name" value="<?php 
                                    if(isset($_POST['name'])) echo $_POST['name'];
                                        else echo $products['name'];
                                ?>" />
                        </div>
                        <div class="form-group">
                            <label>Mã sản phẩm</label>
                            <input class="form-control" name="sku" value="<?php 
                                    if(isset($_POST['sku'])) echo $_POST['sku'];
                                        else echo $products['sku'];
                                ?>" />
                        </div>

                        <div class="form-group">
                            <label>Thương hiệu</label>

                            <select class="form-control" name="brand">
                                <option value="0">
                                    Please Choose Brand
                                </option>
                                <?php  
                                    if (count($brands) > 0 && !is_null($brands)) :
                                        foreach ($brands as $item) :
                                ?>

                                <option value="<?php if(isset($_POST['brands']) && $_POST['brands'] == $item['id']) echo $item['id']; else echo $item['id']; ?>" 
                                    <?php 
                                        $check = 'selected="selected"';
                                        if (
                                            (isset($_POST['brand'])&& $_POST['brand'] == $item['id']) 
                                            || $products['brand_id'] == $item['id']) echo $check;
                                    ?>
                                    >
                                    <?php  
                                        echo $item['name'];
                                    ?>
                                </option>

                                <?php  
                                        endforeach;
                                    endif;
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Danh mục</label>
                            <select class="form-control" name="category">
                                <option value="0">
                                    Please Choose Category
                                </option>

                                <?php  
                                    if (count($categories) > 0 && !is_null($categories)) :
                                        foreach ($categories as $item) :
                                            if ($item['parent_id'] != 0) :
                                ?>

                                <option value="<?php echo $item['id']; ?>"
                                    <?php 
                                        $check = 'selected="selected"';
                                        if (
                                            (isset($_POST['category'])&& $_POST['category'] == $item['id']) 
                                            || $products['product_category_id'] == $item['id']) echo $check;
                                    ?>
                                >
                                    <?php  
                                        echo $item['name'];
                                    ?>
                                </option>

                                <?php  
                                            endif;
                                        endforeach;
                                    endif;
                                ?>

                            </select>
                        </div>

                        <div class="form-group">
                            <label>Price</label>
                            <input class="form-control" name="price" value="<?php 
                                    if(isset($_POST['price'])) echo $_POST['price'];
                                        else echo $products['price'];
                                ?>"  />
                        </div>

                        <div class="form-group">
                            <label>Colors</label>
                            <input class="form-control" name="colors" value="<?php 
                                    if(isset($_POST['colors'])) echo $_POST['colors'];
                                        else echo $products['colors'];
                                ?>"  />
                        </div>

                        <div class="form-group">
                            <label>Images</label>
                            <input type="file" name="img"><br>
                            <input type="checkbox" name="img_featured" value="1"> Chọn làm ảnh đại diện.
                        </div>

                        <div class="form-group">
                            <label>Size</label>
                            <input class="form-control" name="size" value="<?php 
                                    if(isset($_POST['size'])) echo $_POST['size'];
                                        else echo $products['size'];
                                ?>"  />
                        </div>

                        <div class="form-group">
                            <label>Số lượng</label>
                            <input class="form-control" name="qty" value="<?php 
                                    if(isset($_POST['qty'])) echo $_POST['qty'];
                                        else echo $products['qty'];
                                ?>" />
                        </div>

                        <div class="form-group">
                            <label>Product Description</label>
                            <textarea class="form-control" rows="3" name="description">
                                <?php 
                                    if(isset($_POST['description'])) echo trim($_POST['description']);
                                        else echo trim($products['description']);
                                ?>
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>Content</label>
                            <textarea class="form-control" rows="3" name="content">
                                <?php 
                                    if(isset($_POST['content'])) echo $_POST['content'];
                                        else echo $products['content'];
                                ?>
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label>Product options</label>
                            <label class="radio-inline">
                                <input name="is_new" value="1" type="checkbox" <?php if (isset($_POST['is_new']) && $_POST['is_new'] == 1 || $products['is_new'] == 1 )  {
                                    echo "checked = 'checked'";
                                } ?>>New
                            </label>
                            <label class="radio-inline">
                                <input name="is_sale" value="1" type="checkbox" <?php if (isset($_POST['is_sale']) && $_POST['is_sale'] == 1 || $products['is_sale'] == 1 )  {
                                    echo "checked = 'checked'";
                                } ?>>Sale
                            </label>
                            <label class="radio-inline">
                                <input name="is_promo" value="1" type="checkbox" <?php if (isset($_POST['is_promo']) && $_POST['is_promo'] == 1 || $products['is_promo'] == 1 )  {
                                    echo "checked = 'checked'";
                                } ?>>Promo
                            </label>
                            <label class="radio-inline">
                                <input name="is_featured" value="1" type="checkbox" <?php if (isset($_POST['is_featured']) && $_POST['is_featured'] == 1 || $products['is_featured'] == 1 )  {
                                    echo "checked = 'checked'";
                                } ?>>Featured
                            </label>
                        </div>
                        
                        
                        <div class="form-group">
                            <label>Product Status</label>
                            <label class="radio-inline">
                                <input name="status" value="1" type="radio" <?php if (isset($_POST['status']) && $_POST['status'] == 1 || $products['status'] == 1 )  {
                                    echo "checked = 'checked'";
                                } ?>>Show
                            </label>
                            <label class="radio-inline">
                                <input name="status" value="0" type="radio" <?php if (isset($_POST['status']) && $_POST['status'] == 0 || $products['status'] == 0 )  {
                                    echo "checked = 'checked'";
                                } ?>>Hidden
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default" name="submit">Product Save</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
<!-- /#page-wrapper -->
