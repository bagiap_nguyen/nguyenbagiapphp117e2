<?php  
	require '../Layouts/index.php';

	require 'function_delete.php';

	$id 	= $_GET['id'];

	$delete = deleteProduct($id);

	if ($delete == true) {
		$_SESSION['flash_message'] = 'Xóa thành công!';
		header('Location:index.php');
		exit();
	}

	if ($delete == flase) {
		$_SESSION['flash_message'] = 'Không thể xóa, kiểm tra lại!';
		header('Location:index.php');
		exit();
	}

?>