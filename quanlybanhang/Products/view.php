<?php  
	require '../Layouts/index.php';

	$id 		      = $_GET['id'];

	$table 		= 'products';
	$where 		= " id = '{$id}' ";

	$products 	= getOneRecord('*',$table,$where); 

	if (is_null($products)) {
		header('Location:http://localhost:8888/project/nguyenbagiapphp117e2/quanlybanhang/Products/index.php'); // Warning ??
		exit();
	}

	$brand 		= getOneRecord('name','brands'," id = '{$products['brand_id']}' ");
	$category 	      = getOneRecord('name','product_categories'," id = '{$products['product_category_id']}' ")
?>
<!-- Page Content -->
	<div id="page-wrapper">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-lg-12">
	                <h1 class="page-header">Product
	                    <small>Infomation</small>
	                </h1>
	            </div>
	            <!-- /.col-lg-12 -->
	            <table border="1" cellpadding="10">
	            	<tr align="center">
	            		<th colspan="2" align="center">
	            			<?php echo $products['name']; ?>
	            		</th>
	            	</tr>
	            	<tr>
	            		<td>
	            			ID
	            		</td>
	            		<td>
	            			<?php  
	            				echo $products['id'];
	            			?>
	            		</td>
	            	</tr>
                  	<tr>
                  		<td>
                  			Tên sản phẩm
                  		</td>
                  		<td>
                  			<?php  
                  				echo $products['name'];
                  			?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Thương hiệu 
                  		</td>
                  		<td>
                  			<?php echo $brand['name']; ?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Danh mục 
                  		</td>
                  		<td>
                  			<?php  echo $category['name']; ?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Created:
                  		</td>
                  		<td>
                  			<?php echo $products['created_at']; ?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Updated: 
                  		</td>
                  		<td>
                  			<?php echo $products['updated_at']; ?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Lượt xem:
                  		</td>
                  		<td>
                  			<?php echo $products['views']; ?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Tồn kho:
                  		</td>
                  		<td>
                  			<?php echo $products['qty']; ?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Mô tả
                  		</td>
                  		<td>
                  			<?php 
                  				if ($products['description'] != '') {
                  					echo $products['description']; 
                  				} else {
                  					echo "Chưa có dữ liệu";
                  				}
                  			
                  			?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Content
                  		</td>
                  		<td>
                  			<?php 
                  				if ($products['content'] != '') {
                  					echo $products['content']; 
                  				} else {
                  					echo "Chưa có dữ liệu";
                  				}
                  			
                  			?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Trạng thái
                  		</td>
                  		<td>
                  			<?php  
                  				if ($products['status'] == 0) {
                  					echo "Ẩn";
                  				} else {
                  					echo "Hiện";
                  				}
                  			?>
                  		</td>
                  	</tr>
                  	<tr>
                  		<td>
                  			Tùy chọn
                  		</td>
                  		<td>
                  			<a href="edit.php?id=<?php echo $products['id']; ?>" class="btn btn-primary" >  
	                        	Edit
	                        </a>

	                        <a href="delete.php?id=<?php echo $products['id']; ?>" class="btn btn-default" >  
	                        	Delete
	                        </a>
                  		</td>
                  	</tr>
                </table>
	        </div>
	        <!-- /.row -->
	    </div>
	    <!-- /.container-fluid -->
	</div>
<!-- /#page-wrapper -->
