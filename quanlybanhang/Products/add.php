<?php  
	require '../Layouts/index.php';

    $brands     = getAllData('*','brands','','');

    $categories = getAllData('*','product_categories','','');



    $errors     = [];
    $table  = 'products';
    if (isset($_POST['submit'])) {
        // Validate form
         
        if (!isset($_POST['name'])) {
            $errors[]   = 'Vui lòng nhập tên sản phẩm.';
        }

        if (!isset($_POST['sku'])) {
            $errors[]   = 'Vui lòng nhập mã sản phẩm.';
        }

        if (!isset($_POST['brand']) || $_POST['brand'] == 0) {
            $errors[]   = 'Vui lòng chọn thương hiệu.';
        }

        if (!isset($_POST['category'])|| $_POST['category'] == 0) {
            $errors[]   = 'Vui lòng chọn danh mục.';
        }

        if (count($errors) == 0) {

            if (isset($_POST['is_new'])) {
                $is_new =$_POST['is_new'];
            } else {
                $is_new = 0;
            }

            if (isset($_POST['is_sale'])) {
                $is_sale = $_POST['is_sale'];
            } else {
                $is_sale = 0;
            }

            if (isset($_POST['is_featured'])) {
                $is_featured = $_POST['is_featured'];
            } else {
                $is_featured = 0;
            }

            if (isset($_POST['is_promo'])) {
                $is_promo = $_POST['is_promo'];
            } else {
                $is_promo = 0;
            }

            if (isset($_POST['img_featured'])) {
                $img_featured = $_POST['img_featured'];
            } else {
                $img_featured = 0;
            }

            $name           = $_POST['name'];
            $sku            = $_POST['sku'];
            $brand          = $_POST['brand'];
            $category       = $_POST['category'];
            $price          = $_POST['price'];
            $colors         = $_POST['colors'];
            $size           = $_POST['size'];
            $qty            = $_POST['qty'];
            $description    = $_POST['description'];
            $content        = $_POST['content'];
            $status         = $_POST['status'];


            // table img_product
            $img            = trim($_POST['img']);
            $img_featured   = trim($_POST['name']);
            // end table img_product
            // 
            // created_slug
            $slug           = slugify($name);
            // created_at:
            $created_at     = date("Y-m-d");
            $updated_at     = date("Y-m-d");


            $check_slug     = getOneRecord('slug',$table,"slug ='{$slug}'");
            $check_sku      = getOneRecord('sku',$table,"sku ='{$sku}'");

            if ($check_sku || $check_slug) {
                $errors[]   = 'Sản phẩm đã tồn tại.';
            } else {
                $colums = ['sku,name,slug,price,colors,size,qty,brand_id,product_category_id,description,content,is_new,is_promo,is_featured,is_sale,created_at,updated_at,status'];

                $data = ["'{$sku}'","'{$name}'","'{$slug}'","'{$price}'",
                "'{$colors}'","'{$size}'","'{$qty}'","'{$brand}'",
                "'{$category}'","'{$description}'","'{$content}'",
                "'{$is_new}'","'{$is_promo}'","'{$is_featured}'",
                "'{$is_sale}'","'{$created_at}'","'{$updated_at}'",
                "'{$status}'"];

                $insert = insertData($table,$colums,$data);

                if ($insert) {
                    $flag       = 'Thêm thành công!';
                } else {
                    $errors[]   = 'Xảy ra lỗi,không thể thêm vào CSDL';
                }
            }
            

            
        }



    }
?>
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST">
                            <div class="form-group">
                                <?php  
                                    if (isset($flag)) :
                                ?>
                                <label style="color: green;">
                                    <?php echo $flag; ?>
                                    
                                </label>
                                <?php  
                                    endif;
                                ?>
                                <?php  
                                    if (count($errors) > 0) :
                                        foreach ($errors as $key => $value) :
                                ?>
                                <p style="color: red;">
                                   <?php echo $value; ?> 
                                </p>
                                <?php  
                                        endforeach;
                                    endif;
                                ?>
                            </div>
                            
                            <div class="form-group">
                                <label>Tên sản phẩm</label>
                                <input class="form-control" name="name" placeholder="Please Enter Name Product" />
                            </div>
                            <div class="form-group">
                                <label>Mã sản phẩm</label>

                                <input class="form-control" name="sku" placeholder="Please Enter Password" />
                            </div>

                            <div class="form-group">
                                <label>Thương hiệu</label>

                                <select class="form-control" name="brand">
                                    <option value="0">
                                        Please Choose Brand
                                    </option>
                                    <?php  
                                        if (count($brands) > 0 && !is_null($brands)) :
                                            foreach ($brands as $item) :
                                    ?>

                                    <option value="<?php if(isset($_POST['brands']) && $_POST['brands'] == $item['id']) echo $item['id']; else echo $item['id']; ?>">
                                        <?php  
                                            echo $item['name'];
                                        ?>
                                    </option>

                                    <?php  
                                            endforeach;
                                        endif;
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Danh mục</label>
                                <select class="form-control" name="category">
                                    <option value="0">
                                        Please Choose Category
                                    </option>

                                    <?php  
                                        if (count($categories) > 0 && !is_null($categories)) :
                                            foreach ($categories as $item) :
                                                if ($item['parent_id'] != 0) :
                                    ?>

                                    <option value="<?php if(isset($_POST['categories']) && $_POST['categories'] == $item['id']) echo $item['id']; else echo $item['id']; ?>">
                                        <?php  
                                            echo $item['name'];
                                        ?>
                                    </option>

                                    <?php  
                                                endif;
                                            endforeach;
                                        endif;
                                    ?>

                                </select>
                            </div>

                            <div class="form-group">
                                <label>Price</label>
                                <input class="form-control" name="price"  />
                            </div>

                            <div class="form-group">
                                <label>Colors</label>
                                <input class="form-control" name="colors"  />
                            </div>

                            <div class="form-group">
                                <label>Images</label>
                                <input type="file" name="img"><br>
                                <input type="checkbox" name="img_featured" value="1"> Chọn làm ảnh đại diện.
                            </div>

                            <div class="form-group">
                                <label>Size</label>
                                <input class="form-control" name="size"  />
                            </div>

                            <div class="form-group">
                                <label>Số lượng</label>
                                <input class="form-control" name="qty"  />
                            </div>

                            <div class="form-group">
                                <label>Product Description</label>
                                <textarea class="form-control" rows="3" name="description"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Content</label>
                                <textarea class="form-control" rows="3" name="content"></textarea>
                            </div>

                            <div class="form-group">
                                <label>Product options</label>
                                <label class="radio-inline">
                                    <input name="is_new" value="1" checked="" type="checkbox">New
                                </label>
                                <label class="radio-inline">
                                    <input name="is_sale" value="1" type="checkbox">Sale
                                </label>
                                <label class="radio-inline">
                                    <input name="is_promo" value="1" type="checkbox">Promo
                                </label>
                                <label class="radio-inline">
                                    <input name="is_featured" value="1" type="checkbox">Featured
                                </label>
                            </div>
                            
                            
                            <div class="form-group">
                                <label>Product Status</label>
                                <label class="radio-inline">
                                    <input name="status" value="1" checked="" type="radio">Show
                                </label>
                                <label class="radio-inline">
                                    <input name="status" value="0" type="radio">Hidden
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default" name="submit">Product Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
