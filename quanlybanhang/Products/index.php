<?php  
	require '../Layouts/index.php';

	$products 	= getAllData('*','products','','');

?>
<!-- Page Content -->
	<div id="page-wrapper">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-lg-12">
	                <h1 class="page-header">Product
	                    <small>List</small>
	                </h1>
	            </div>
	            <!-- /.col-lg-12 -->
	            <?php  
	            	if (isset($_SESSION['flash_message'])) :
	            ?>
	            <div class="_message">
	            	<p style="font-size: 30px;color: blue;" >
	            		<?php  
	            			echo $_SESSION['flash_message'];
	            		?>
	            	</p>
	            </div>
	            <?php  
	            		unset($_SESSION['flash_message']);
	            	endif;
	            ?>
	            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                <thead>
	                    <tr align="center">
	                        <th>ID</th>
	                        <th>Tên sản phẩm</th>
	                        <th>Mã sản phẩm</th>
	                        <th>Giá</th>
	                        <th>View</th>
	                        <th>Edit</th>
                            <th>Delete</th>
	                        
	                    </tr>
	                </thead>
	                <tbody>
	                	<?php  
	                		if (!is_null($products) && count($products) >0) :
	                			$x = 1;
	                			foreach ($products as $item) :
	                				if ($x % 2 == 1) :
	                	?>

	                    <tr class="odd gradeX" align="center">
	                        <td>
	                        	<?php  
	                        		echo $item['id'];
	                        	?>
	                        </td>
	                        <td>
	                        	<?php  
	                        		echo $item['name'];
	                        	?>
	                        
	                        </td>
	                        <td>
	                        	<?php  
	                        		echo $item['sku'];
	                        	?>
	                        </td>
	                        <td>
	                        	<?php  
	                        		echo $item['price'];
	                        	?>
	                        </td>
	                        <td class="center">
	                        	<i class="fa fa-eye fa-fw"></i>
	                         	<a href="view.php?id=<?php echo $item['id']; ?>"> 
	                         		View
	                         	</a>
	                     	</td>
	                        <td class="center">
	                        	<i class="fa fa-trash-o  fa-fw"></i>
	                        	<a href="edit.php?id=<?php echo $item['id']; ?>">  
	                        		Edit
	                        	</a>
	                        </td>
	                        <td class="center">
	                        	<i class="fa fa-pencil fa-fw"></i>
	                        	<a href="delete.php?id=<?php echo $item['id']; ?>" class="delete-item"> 
	                        		Delete
	                        	</a>
	                        </td>
	                    </tr>

	                    <?php  
	                    	else:
	                    ?>
	                    <tr class="even gradeC" align="center">
	                        <td>
	                        	<?php  
	                        		echo $item['id'];
	                        	?>
	                        </td>
	                        <td>
	                        	<?php  
	                        		echo $item['name'];
	                        	?>
	                        </td>
	                        <td>
	                        	<?php  
	                        		echo $item['sku'];
	                        	?>
	                        </td>
	                        <td>
	                        	<?php  
	                        		echo $item['price'];
	                        	?>
	                        </td>
	                        <td class="center">
	                        	<i class="fa fa-eye fa-fw"></i>
	                         	<a href="view.php?id=<?php echo $item['id']; ?>"> 
	                         		View
	                         	</a>
	                     	</td>
	                        <td class="center">
	                        	<i class="fa fa-trash-o  fa-fw"></i>
	                        	<a href="edit.php?id=<?php echo $item['id']; ?>">  
	                        		Edit
	                        	</a>
	                        </td>
	                        <td class="center">
	                        	<i class="fa fa-pencil fa-fw"></i>
	                        	<a href="delete.php?id=<?php echo $item['id']; ?>" class="delete-item"> 
	                        		Delete
	                        	</a>
	                        </td>
	                    </tr>

	                    <?php  
	                    				$x++;
	                    			endif;
	                    		endforeach;
	                    	endif;
	                    ?>
	                </tbody>
	            </table>
	        </div>
	        <!-- /.row -->
	    </div>
	    <!-- /.container-fluid -->
	</div>
<!-- /#page-wrapper -->

