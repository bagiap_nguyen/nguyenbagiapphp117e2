-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 24, 2018 lúc 12:22 AM
-- Phiên bản máy phục vụ: 10.1.29-MariaDB
-- Phiên bản PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `onetech`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` text,
  `position` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `logo`, `image`, `description`, `meta_title`, `meta_keyword`, `meta_description`, `status`) VALUES
(1, 'Acer', 'brand-acer', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 'Asus', 'brand-asus', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(3, 'Apple', 'brand-apple', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, 'Dell', 'brand-dell', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(5, 'HP', 'brand-hp', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, 'Lenovo', 'brand-lenovo', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(7, 'MSI', 'brand-msi', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, 'LG', 'brand-lg', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(9, 'Antecmemory', 'brand-antecmemory', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(10, 'Avexir', 'brand-avexir', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(11, 'Axpro', 'brand-axpro', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(12, 'Corsair', 'brand-corsair', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(13, 'Kingmax', 'brand-kingmax', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(14, 'Kingston', 'brand-kingston', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(15, 'SiliconPower', 'brand-siliconpower', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(16, 'HITACHI', 'brand-hitachi', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(17, 'SEAGATE', 'brand-seagate', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(18, 'TOSHIBA', 'brand-toshiba', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(19, 'Western Digital', 'brand-western-digital', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(20, 'PISEN', 'brand-pisen', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(21, 'PUMA', 'brand-puma', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(22, 'RAZER', 'brand-razer', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(23, 'SAKOS', 'brand-sakos', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(24, 'COOL COLD', 'brand-cool-cold', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(25, 'COOLER MASTER', 'brand-cooler-master', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(26, 'DEEP COOL', 'brand-deep-cool', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(27, 'THERMALTAKE', 'brand-thermaltake', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(28, 'OTHER', 'brand-other', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(29, 'NEWMEN', 'brand-newmen', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(30, 'OZONE', 'brand-ozone', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(31, 'E-Blue', 'brand-e-blue', NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `usage_limit_per_user` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `provicence_id` int(11) NOT NULL,
  `districts_id` int(11) NOT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `password` varchar(20) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `fullname`, `email`, `phone`, `address`, `provicence_id`, `districts_id`, `gender`, `password`, `customer_group_id`, `created_at`, `updated_at`, `status`) VALUES
(1, 'customer test', 'customer@gmail.com', '11241434234', 'ha noi', 39, 102, 1, 'abcde123456', NULL, '2018-04-11', '2018-04-14', 0),
(2, 'cusstomer tesst bbbb', 'testbbbb@gmail.com', '13325434543', 'ha noi', 35, 224, 0, '123456abcde', NULL, '2018-04-25', '2018-04-10', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers_group`
--

CREATE TABLE `customers_group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `districts`
--

CREATE TABLE `districts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `province_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `districts`
--

INSERT INTO `districts` (`id`, `name`, `province_id`) VALUES
(2, 'Quận Ba Đình', 1),
(3, 'Quận Hoàn Kiếm', 1),
(4, 'Quận Tây Hồ', 1),
(5, 'Quận Long Biên', 1),
(6, 'Quận Cầu Giấy', 1),
(7, 'Quận Đống Đa', 1),
(8, 'Quận Hai Bà Trưng', 1),
(9, 'Quận Hoàng Mai', 1),
(10, 'Quận Thanh Xuân', 1),
(11, 'Huyện Sóc Sơn', 1),
(12, 'Huyện Đông Anh', 1),
(13, 'Huyện Gia Lâm', 1),
(14, 'Quận Nam Từ Liêm', 1),
(15, 'Huyện Thanh Trì', 1),
(16, 'Quận Bắc Từ Liêm', 1),
(17, 'Huyện Mê Linh', 1),
(18, 'Quận Hà Đông', 1),
(19, 'Thị xã Sơn Tây', 1),
(20, 'Huyện Ba Vì', 1),
(21, 'Huyện Phúc Thọ', 1),
(22, 'Huyện Đan Phượng', 1),
(23, 'Huyện Hoài Đức', 1),
(24, 'Huyện Quốc Oai', 1),
(25, 'Huyện Thạch Thất', 1),
(26, 'Huyện Chương Mỹ', 1),
(27, 'Huyện Thanh Oai', 1),
(28, 'Huyện Thường Tín', 1),
(29, 'Huyện Phú Xuyên', 1),
(30, 'Huyện Ứng Hòa', 1),
(31, 'Huyện Mỹ Đức', 1),
(32, 'Thành phố Hà Giang', 2),
(33, 'Huyện Đồng Văn', 2),
(34, 'Huyện Mèo Vạc', 2),
(35, 'Huyện Yên Minh', 2),
(36, 'Huyện Quản Bạ', 2),
(37, 'Huyện Vị Xuyên', 2),
(38, 'Huyện Bắc Mê', 2),
(39, 'Huyện Hoàng Su Phì', 2),
(40, 'Huyện Xín Mần', 2),
(41, 'Huyện Bắc Quang', 2),
(42, 'Huyện Quang Bình', 2),
(43, 'Thành phố Cao Bằng', 3),
(44, 'Huyện Bảo Lâm', 3),
(45, 'Huyện Bảo Lạc', 3),
(46, 'Huyện Thông Nông', 3),
(47, 'Huyện Hà Quảng', 3),
(48, 'Huyện Trà Lĩnh', 3),
(49, 'Huyện Trùng Khánh', 3),
(50, 'Huyện Hạ Lang', 3),
(51, 'Huyện Quảng Uyên', 3),
(52, 'Huyện Phục Hoà', 3),
(53, 'Huyện Hoà An', 3),
(54, 'Huyện Nguyên Bình', 3),
(55, 'Huyện Thạch An', 3),
(56, 'Thành Phố Bắc Kạn', 4),
(57, 'Huyện Pác Nặm', 4),
(58, 'Huyện Ba Bể', 4),
(59, 'Huyện Ngân Sơn', 4),
(60, 'Huyện Bạch Thông', 4),
(61, 'Huyện Chợ Đồn', 4),
(62, 'Huyện Chợ Mới', 4),
(63, 'Huyện Na Rì', 4),
(64, 'Thành phố Tuyên Quang', 5),
(65, 'Huyện Lâm Bình', 5),
(66, 'Huyện Nà Hang', 5),
(67, 'Huyện Chiêm Hóa', 5),
(68, 'Huyện Hàm Yên', 5),
(69, 'Huyện Yên Sơn', 5),
(70, 'Huyện Sơn Dương', 5),
(71, 'Thành phố Lào Cai', 6),
(72, 'Huyện Bát Xát', 6),
(73, 'Huyện Mường Khương', 6),
(74, 'Huyện Si Ma Cai', 6),
(75, 'Huyện Bắc Hà', 6),
(76, 'Huyện Bảo Thắng', 6),
(77, 'Huyện Bảo Yên', 6),
(78, 'Huyện Sa Pa', 6),
(79, 'Huyện Văn Bàn', 6),
(80, 'Thành phố Điện Biên Phủ', 7),
(81, 'Thị Xã Mường Lay', 7),
(82, 'Huyện Mường Nhé', 7),
(83, 'Huyện Mường Chà', 7),
(84, 'Huyện Tủa Chùa', 7),
(85, 'Huyện Tuần Giáo', 7),
(86, 'Huyện Điện Biên', 7),
(87, 'Huyện Điện Biên Đông', 7),
(88, 'Huyện Mường Ảng', 7),
(89, 'Huyện Nậm Pồ', 7),
(90, 'Thành phố Lai Châu', 8),
(91, 'Huyện Tam Đường', 8),
(92, 'Huyện Mường Tè', 8),
(93, 'Huyện Sìn Hồ', 8),
(94, 'Huyện Phong Thổ', 8),
(95, 'Huyện Than Uyên', 8),
(96, 'Huyện Tân Uyên', 8),
(97, 'Huyện Nậm Nhùn', 8),
(98, 'Thành phố Sơn La', 9),
(99, 'Huyện Quỳnh Nhai', 9),
(100, 'Huyện Thuận Châu', 9),
(101, 'Huyện Mường La', 9),
(102, 'Huyện Bắc Yên', 9),
(103, 'Huyện Phù Yên', 9),
(104, 'Huyện Mộc Châu', 9),
(105, 'Huyện Yên Châu', 9),
(106, 'Huyện Mai Sơn', 9),
(107, 'Huyện Sông Mã', 9),
(108, 'Huyện Sốp Cộp', 9),
(109, 'Huyện Vân Hồ', 9),
(110, 'Thành phố Yên Bái', 10),
(111, 'Thị xã Nghĩa Lộ', 10),
(112, 'Huyện Lục Yên', 10),
(113, 'Huyện Văn Yên', 10),
(114, 'Huyện Mù Căng Chải', 10),
(115, 'Huyện Trấn Yên', 10),
(116, 'Huyện Trạm Tấu', 10),
(117, 'Huyện Văn Chấn', 10),
(118, 'Huyện Yên Bình', 10),
(119, 'Thành phố Hòa Bình', 11),
(120, 'Huyện Đà Bắc', 11),
(121, 'Huyện Kỳ Sơn', 11),
(122, 'Huyện Lương Sơn', 11),
(123, 'Huyện Kim Bôi', 11),
(124, 'Huyện Cao Phong', 11),
(125, 'Huyện Tân Lạc', 11),
(126, 'Huyện Mai Châu', 11),
(127, 'Huyện Lạc Sơn', 11),
(128, 'Huyện Yên Thủy', 11),
(129, 'Huyện Lạc Thủy', 11),
(130, 'Thành phố Thái Nguyên', 12),
(131, 'Thành phố Sông Công', 12),
(132, 'Huyện Định Hóa', 12),
(133, 'Huyện Phú Lương', 12),
(134, 'Huyện Đồng Hỷ', 12),
(135, 'Huyện Võ Nhai', 12),
(136, 'Huyện Đại Từ', 12),
(137, 'Thị xã Phổ Yên', 12),
(138, 'Huyện Phú Bình', 12),
(139, 'Thành phố Lạng Sơn', 13),
(140, 'Huyện Tràng Định', 13),
(141, 'Huyện Bình Gia', 13),
(142, 'Huyện Văn Lãng', 13),
(143, 'Huyện Cao Lộc', 13),
(144, 'Huyện Văn Quan', 13),
(145, 'Huyện Bắc Sơn', 13),
(146, 'Huyện Hữu Lũng', 13),
(147, 'Huyện Chi Lăng', 13),
(148, 'Huyện Lộc Bình', 13),
(149, 'Huyện Đình Lập', 13),
(150, 'Thành phố Hạ Long', 14),
(151, 'Thành phố Móng Cái', 14),
(152, 'Thành phố Cẩm Phả', 14),
(153, 'Thành phố Uông Bí', 14),
(154, 'Huyện Bình Liêu', 14),
(155, 'Huyện Tiên Yên', 14),
(156, 'Huyện Đầm Hà', 14),
(157, 'Huyện Hải Hà', 14),
(158, 'Huyện Ba Chẽ', 14),
(159, 'Huyện Vân Đồn', 14),
(160, 'Huyện Hoành Bồ', 14),
(161, 'Thị xã Đông Triều', 14),
(162, 'Thị xã Quảng Yên', 14),
(163, 'Huyện Cô Tô', 14),
(164, 'Thành phố Bắc Giang', 15),
(165, 'Huyện Yên Thế', 15),
(166, 'Huyện Tân Yên', 15),
(167, 'Huyện Lạng Giang', 15),
(168, 'Huyện Lục Nam', 15),
(169, 'Huyện Lục Ngạn', 15),
(170, 'Huyện Sơn Động', 15),
(171, 'Huyện Yên Dũng', 15),
(172, 'Huyện Việt Yên', 15),
(173, 'Huyện Hiệp Hòa', 15),
(174, 'Thành phố Việt Trì', 16),
(175, 'Thị xã Phú Thọ', 16),
(176, 'Huyện Đoan Hùng', 16),
(177, 'Huyện Hạ Hoà', 16),
(178, 'Huyện Thanh Ba', 16),
(179, 'Huyện Phù Ninh', 16),
(180, 'Huyện Yên Lập', 16),
(181, 'Huyện Cẩm Khê', 16),
(182, 'Huyện Tam Nông', 16),
(183, 'Huyện Lâm Thao', 16),
(184, 'Huyện Thanh Sơn', 16),
(185, 'Huyện Thanh Thuỷ', 16),
(186, 'Huyện Tân Sơn', 16),
(187, 'Thành phố Vĩnh Yên', 17),
(188, 'Thị xã Phúc Yên', 17),
(189, 'Huyện Lập Thạch', 17),
(190, 'Huyện Tam Dương', 17),
(191, 'Huyện Tam Đảo', 17),
(192, 'Huyện Bình Xuyên', 17),
(193, 'Huyện Yên Lạc', 17),
(194, 'Huyện Vĩnh Tường', 17),
(195, 'Huyện Sông Lô', 17),
(196, 'Thành phố Bắc Ninh', 18),
(197, 'Huyện Yên Phong', 18),
(198, 'Huyện Quế Võ', 18),
(199, 'Huyện Tiên Du', 18),
(200, 'Thị xã Từ Sơn', 18),
(201, 'Huyện Thuận Thành', 18),
(202, 'Huyện Gia Bình', 18),
(203, 'Huyện Lương Tài', 18),
(204, 'Thành phố Hải Dương', 19),
(205, 'Thị xã Chí Linh', 19),
(206, 'Huyện Nam Sách', 19),
(207, 'Huyện Kinh Môn', 19),
(208, 'Huyện Kim Thành', 19),
(209, 'Huyện Thanh Hà', 19),
(210, 'Huyện Cẩm Giàng', 19),
(211, 'Huyện Bình Giang', 19),
(212, 'Huyện Gia Lộc', 19),
(213, 'Huyện Tứ Kỳ', 19),
(214, 'Huyện Ninh Giang', 19),
(215, 'Huyện Thanh Miện', 19),
(216, 'Quận Hồng Bàng', 20),
(217, 'Quận Ngô Quyền', 20),
(218, 'Quận Lê Chân', 20),
(219, 'Quận Hải An', 20),
(220, 'Quận Kiến An', 20),
(221, 'Quận Đồ Sơn', 20),
(222, 'Quận Dương Kinh', 20),
(223, 'Huyện Thuỷ Nguyên', 20),
(224, 'Huyện An Dương', 20),
(225, 'Huyện An Lão', 20),
(226, 'Huyện Kiến Thuỵ', 20),
(227, 'Huyện Tiên Lãng', 20),
(228, 'Huyện Vĩnh Bảo', 20),
(229, 'Huyện Cát Hải', 20),
(230, 'Huyện Bạch Long Vĩ', 20),
(231, 'Thành phố Hưng Yên', 21),
(232, 'Huyện Văn Lâm', 21),
(233, 'Huyện Văn Giang', 21),
(234, 'Huyện Yên Mỹ', 21),
(235, 'Huyện Mỹ Hào', 21),
(236, 'Huyện Ân Thi', 21),
(237, 'Huyện Khoái Châu', 21),
(238, 'Huyện Kim Động', 21),
(239, 'Huyện Tiên Lữ', 21),
(240, 'Huyện Phù Cừ', 21),
(241, 'Thành phố Thái Bình', 22),
(242, 'Huyện Quỳnh Phụ', 22),
(243, 'Huyện Hưng Hà', 22),
(244, 'Huyện Đông Hưng', 22),
(245, 'Huyện Thái Thụy', 22),
(246, 'Huyện Tiền Hải', 22),
(247, 'Huyện Kiến Xương', 22),
(248, 'Huyện Vũ Thư', 22),
(249, 'Thành phố Phủ Lý', 23),
(250, 'Huyện Duy Tiên', 23),
(251, 'Huyện Kim Bảng', 23),
(252, 'Huyện Thanh Liêm', 23),
(253, 'Huyện Bình Lục', 23),
(254, 'Huyện Lý Nhân', 23),
(255, 'Thành phố Nam Định', 24),
(256, 'Huyện Mỹ Lộc', 24),
(257, 'Huyện Vụ Bản', 24),
(258, 'Huyện Ý Yên', 24),
(259, 'Huyện Nghĩa Hưng', 24),
(260, 'Huyện Nam Trực', 24),
(261, 'Huyện Trực Ninh', 24),
(262, 'Huyện Xuân Trường', 24),
(263, 'Huyện Giao Thủy', 24),
(264, 'Huyện Hải Hậu', 24),
(265, 'Thành phố Ninh Bình', 25),
(266, 'Thành phố Tam Điệp', 25),
(267, 'Huyện Nho Quan', 25),
(268, 'Huyện Gia Viễn', 25),
(269, 'Huyện Hoa Lư', 25),
(270, 'Huyện Yên Khánh', 25),
(271, 'Huyện Kim Sơn', 25),
(272, 'Huyện Yên Mô', 25),
(273, 'Thành phố Thanh Hóa', 26),
(274, 'Thị xã Bỉm Sơn', 26),
(275, 'Thành phố Sầm Sơn', 26),
(276, 'Huyện Mường Lát', 26),
(277, 'Huyện Quan Hóa', 26),
(278, 'Huyện Bá Thước', 26),
(279, 'Huyện Quan Sơn', 26),
(280, 'Huyện Lang Chánh', 26),
(281, 'Huyện Ngọc Lặc', 26),
(282, 'Huyện Cẩm Thủy', 26),
(283, 'Huyện Thạch Thành', 26),
(284, 'Huyện Hà Trung', 26),
(285, 'Huyện Vĩnh Lộc', 26),
(286, 'Huyện Yên Định', 26),
(287, 'Huyện Thọ Xuân', 26),
(288, 'Huyện Thường Xuân', 26),
(289, 'Huyện Triệu Sơn', 26),
(290, 'Huyện Thiệu Hóa', 26),
(291, 'Huyện Hoằng Hóa', 26),
(292, 'Huyện Hậu Lộc', 26),
(293, 'Huyện Nga Sơn', 26),
(294, 'Huyện Như Xuân', 26),
(295, 'Huyện Như Thanh', 26),
(296, 'Huyện Nông Cống', 26),
(297, 'Huyện Đông Sơn', 26),
(298, 'Huyện Quảng Xương', 26),
(299, 'Huyện Tĩnh Gia', 26),
(300, 'Thành phố Vinh', 27),
(301, 'Thị xã Cửa Lò', 27),
(302, 'Thị xã Thái Hoà', 27),
(303, 'Huyện Quế Phong', 27),
(304, 'Huyện Quỳ Châu', 27),
(305, 'Huyện Kỳ Sơn', 27),
(306, 'Huyện Tương Dương', 27),
(307, 'Huyện Nghĩa Đàn', 27),
(308, 'Huyện Quỳ Hợp', 27),
(309, 'Huyện Quỳnh Lưu', 27),
(310, 'Huyện Con Cuông', 27),
(311, 'Huyện Tân Kỳ', 27),
(312, 'Huyện Anh Sơn', 27),
(313, 'Huyện Diễn Châu', 27),
(314, 'Huyện Yên Thành', 27),
(315, 'Huyện Đô Lương', 27),
(316, 'Huyện Thanh Chương', 27),
(317, 'Huyện Nghi Lộc', 27),
(318, 'Huyện Nam Đàn', 27),
(319, 'Huyện Hưng Nguyên', 27),
(320, 'Thị xã Hoàng Mai', 27),
(321, 'Thành phố Hà Tĩnh', 28),
(322, 'Thị xã Hồng Lĩnh', 28),
(323, 'Huyện Hương Sơn', 28),
(324, 'Huyện Đức Thọ', 28),
(325, 'Huyện Vũ Quang', 28),
(326, 'Huyện Nghi Xuân', 28),
(327, 'Huyện Can Lộc', 28),
(328, 'Huyện Hương Khê', 28),
(329, 'Huyện Thạch Hà', 28),
(330, 'Huyện Cẩm Xuyên', 28),
(331, 'Huyện Kỳ Anh', 28),
(332, 'Huyện Lộc Hà', 28),
(333, 'Thị xã Kỳ Anh', 28),
(334, 'Thành Phố Đồng Hới', 29),
(335, 'Huyện Minh Hóa', 29),
(336, 'Huyện Tuyên Hóa', 29),
(337, 'Huyện Quảng Trạch', 29),
(338, 'Huyện Bố Trạch', 29),
(339, 'Huyện Quảng Ninh', 29),
(340, 'Huyện Lệ Thủy', 29),
(341, 'Thị xã Ba Đồn', 29),
(342, 'Thành phố Đông Hà', 30),
(343, 'Thị xã Quảng Trị', 30),
(344, 'Huyện Vĩnh Linh', 30),
(345, 'Huyện Hướng Hóa', 30),
(346, 'Huyện Gio Linh', 30),
(347, 'Huyện Đa Krông', 30),
(348, 'Huyện Cam Lộ', 30),
(349, 'Huyện Triệu Phong', 30),
(350, 'Huyện Hải Lăng', 30),
(351, 'Huyện Cồn Cỏ', 30),
(352, 'Thành phố Huế', 31),
(353, 'Huyện Phong Điền', 31),
(354, 'Huyện Quảng Điền', 31),
(355, 'Huyện Phú Vang', 31),
(356, 'Thị xã Hương Thủy', 31),
(357, 'Thị xã Hương Trà', 31),
(358, 'Huyện A Lưới', 31),
(359, 'Huyện Phú Lộc', 31),
(360, 'Huyện Nam Đông', 31),
(361, 'Quận Liên Chiểu', 32),
(362, 'Quận Thanh Khê', 32),
(363, 'Quận Hải Châu', 32),
(364, 'Quận Sơn Trà', 32),
(365, 'Quận Ngũ Hành Sơn', 32),
(366, 'Quận Cẩm Lệ', 32),
(367, 'Huyện Hòa Vang', 32),
(368, 'Huyện Hoàng Sa', 32),
(369, 'Thành phố Tam Kỳ', 33),
(370, 'Thành phố Hội An', 33),
(371, 'Huyện Tây Giang', 33),
(372, 'Huyện Đông Giang', 33),
(373, 'Huyện Đại Lộc', 33),
(374, 'Thị xã Điện Bàn', 33),
(375, 'Huyện Duy Xuyên', 33),
(376, 'Huyện Quế Sơn', 33),
(377, 'Huyện Nam Giang', 33),
(378, 'Huyện Phước Sơn', 33),
(379, 'Huyện Hiệp Đức', 33),
(380, 'Huyện Thăng Bình', 33),
(381, 'Huyện Tiên Phước', 33),
(382, 'Huyện Bắc Trà My', 33),
(383, 'Huyện Nam Trà My', 33),
(384, 'Huyện Núi Thành', 33),
(385, 'Huyện Phú Ninh', 33),
(386, 'Huyện Nông Sơn', 33),
(387, 'Thành phố Quảng Ngãi', 34),
(388, 'Huyện Bình Sơn', 34),
(389, 'Huyện Trà Bồng', 34),
(390, 'Huyện Tây Trà', 34),
(391, 'Huyện Sơn Tịnh', 34),
(392, 'Huyện Tư Nghĩa', 34),
(393, 'Huyện Sơn Hà', 34),
(394, 'Huyện Sơn Tây', 34),
(395, 'Huyện Minh Long', 34),
(396, 'Huyện Nghĩa Hành', 34),
(397, 'Huyện Mộ Đức', 34),
(398, 'Huyện Đức Phổ', 34),
(399, 'Huyện Ba Tơ', 34),
(400, 'Huyện Lý Sơn', 34),
(401, 'Thành phố Qui Nhơn', 35),
(402, 'Huyện An Lão', 35),
(403, 'Huyện Hoài Nhơn', 35),
(404, 'Huyện Hoài Ân', 35),
(405, 'Huyện Phù Mỹ', 35),
(406, 'Huyện Vĩnh Thạnh', 35),
(407, 'Huyện Tây Sơn', 35),
(408, 'Huyện Phù Cát', 35),
(409, 'Thị xã An Nhơn', 35),
(410, 'Huyện Tuy Phước', 35),
(411, 'Huyện Vân Canh', 35),
(412, 'Thành phố Tuy Hoà', 36),
(413, 'Thị xã Sông Cầu', 36),
(414, 'Huyện Đồng Xuân', 36),
(415, 'Huyện Tuy An', 36),
(416, 'Huyện Sơn Hòa', 36),
(417, 'Huyện Sông Hinh', 36),
(418, 'Huyện Tây Hoà', 36),
(419, 'Huyện Phú Hoà', 36),
(420, 'Huyện Đông Hòa', 36),
(421, 'Thành phố Nha Trang', 37),
(422, 'Thành phố Cam Ranh', 37),
(423, 'Huyện Cam Lâm', 37),
(424, 'Huyện Vạn Ninh', 37),
(425, 'Thị xã Ninh Hòa', 37),
(426, 'Huyện Khánh Vĩnh', 37),
(427, 'Huyện Diên Khánh', 37),
(428, 'Huyện Khánh Sơn', 37),
(429, 'Huyện Trường Sa', 37),
(430, 'Thành phố Phan Rang-Tháp Chàm', 38),
(431, 'Huyện Bác Ái', 38),
(432, 'Huyện Ninh Sơn', 38),
(433, 'Huyện Ninh Hải', 38),
(434, 'Huyện Ninh Phước', 38),
(435, 'Huyện Thuận Bắc', 38),
(436, 'Huyện Thuận Nam', 38),
(437, 'Thành phố Phan Thiết', 39),
(438, 'Thị xã La Gi', 39),
(439, 'Huyện Tuy Phong', 39),
(440, 'Huyện Bắc Bình', 39),
(441, 'Huyện Hàm Thuận Bắc', 39),
(442, 'Huyện Hàm Thuận Nam', 39),
(443, 'Huyện Tánh Linh', 39),
(444, 'Huyện Đức Linh', 39),
(445, 'Huyện Hàm Tân', 39),
(446, 'Huyện Phú Quí', 39),
(447, 'Thành phố Kon Tum', 40),
(448, 'Huyện Đắk Glei', 40),
(449, 'Huyện Ngọc Hồi', 40),
(450, 'Huyện Đắk Tô', 40),
(451, 'Huyện Kon Plông', 40),
(452, 'Huyện Kon Rẫy', 40),
(453, 'Huyện Đắk Hà', 40),
(454, 'Huyện Sa Thầy', 40),
(455, 'Huyện Tu Mơ Rông', 40),
(456, 'Huyện Ia H\' Drai', 40),
(457, 'Thành phố Pleiku', 41),
(458, 'Thị xã An Khê', 41),
(459, 'Thị xã Ayun Pa', 41),
(460, 'Huyện KBang', 41),
(461, 'Huyện Đăk Đoa', 41),
(462, 'Huyện Chư Păh', 41),
(463, 'Huyện Ia Grai', 41),
(464, 'Huyện Mang Yang', 41),
(465, 'Huyện Kông Chro', 41),
(466, 'Huyện Đức Cơ', 41),
(467, 'Huyện Chư Prông', 41),
(468, 'Huyện Chư Sê', 41),
(469, 'Huyện Đăk Pơ', 41),
(470, 'Huyện Ia Pa', 41),
(471, 'Huyện Krông Pa', 41),
(472, 'Huyện Phú Thiện', 41),
(473, 'Huyện Chư Pưh', 41),
(474, 'Thành phố Buôn Ma Thuột', 42),
(475, 'Thị Xã Buôn Hồ', 42),
(476, 'Huyện Ea H\'leo', 42),
(477, 'Huyện Ea Súp', 42),
(478, 'Huyện Buôn Đôn', 42),
(479, 'Huyện Cư M\'gar', 42),
(480, 'Huyện Krông Búk', 42),
(481, 'Huyện Krông Năng', 42),
(482, 'Huyện Ea Kar', 42),
(483, 'Huyện M\'Đrắk', 42),
(484, 'Huyện Krông Bông', 42),
(485, 'Huyện Krông Pắc', 42),
(486, 'Huyện Krông A Na', 42),
(487, 'Huyện Lắk', 42),
(488, 'Huyện Cư Kuin', 42),
(489, 'Thị xã Gia Nghĩa', 43),
(490, 'Huyện Đăk Glong', 43),
(491, 'Huyện Cư Jút', 43),
(492, 'Huyện Đắk Mil', 43),
(493, 'Huyện Krông Nô', 43),
(494, 'Huyện Đắk Song', 43),
(495, 'Huyện Đắk R\'Lấp', 43),
(496, 'Huyện Tuy Đức', 43),
(497, 'Thành phố Đà Lạt', 44),
(498, 'Thành phố Bảo Lộc', 44),
(499, 'Huyện Đam Rông', 44),
(500, 'Huyện Lạc Dương', 44),
(501, 'Huyện Lâm Hà', 44),
(502, 'Huyện Đơn Dương', 44),
(503, 'Huyện Đức Trọng', 44),
(504, 'Huyện Di Linh', 44),
(505, 'Huyện Bảo Lâm', 44),
(506, 'Huyện Đạ Huoai', 44),
(507, 'Huyện Đạ Tẻh', 44),
(508, 'Huyện Cát Tiên', 44),
(509, 'Thị xã Phước Long', 45),
(510, 'Thị xã Đồng Xoài', 45),
(511, 'Thị xã Bình Long', 45),
(512, 'Huyện Bù Gia Mập', 45),
(513, 'Huyện Lộc Ninh', 45),
(514, 'Huyện Bù Đốp', 45),
(515, 'Huyện Hớn Quản', 45),
(516, 'Huyện Đồng Phú', 45),
(517, 'Huyện Bù Đăng', 45),
(518, 'Huyện Chơn Thành', 45),
(519, 'Huyện Phú Riềng', 45),
(520, 'Thành phố Tây Ninh', 46),
(521, 'Huyện Tân Biên', 46),
(522, 'Huyện Tân Châu', 46),
(523, 'Huyện Dương Minh Châu', 46),
(524, 'Huyện Châu Thành', 46),
(525, 'Huyện Hòa Thành', 46),
(526, 'Huyện Gò Dầu', 46),
(527, 'Huyện Bến Cầu', 46),
(528, 'Huyện Trảng Bàng', 46),
(529, 'Thành phố Thủ Dầu Một', 47),
(530, 'Huyện Bàu Bàng', 47),
(531, 'Huyện Dầu Tiếng', 47),
(532, 'Thị xã Bến Cát', 47),
(533, 'Huyện Phú Giáo', 47),
(534, 'Thị xã Tân Uyên', 47),
(535, 'Thị xã Dĩ An', 47),
(536, 'Thị xã Thuận An', 47),
(537, 'Huyện Bắc Tân Uyên', 47),
(538, 'Thành phố Biên Hòa', 48),
(539, 'Thị xã Long Khánh', 48),
(540, 'Huyện Tân Phú', 48),
(541, 'Huyện Vĩnh Cửu', 48),
(542, 'Huyện Định Quán', 48),
(543, 'Huyện Trảng Bom', 48),
(544, 'Huyện Thống Nhất', 48),
(545, 'Huyện Cẩm Mỹ', 48),
(546, 'Huyện Long Thành', 48),
(547, 'Huyện Xuân Lộc', 48),
(548, 'Huyện Nhơn Trạch', 48),
(549, 'Thành phố Vũng Tàu', 49),
(550, 'Thành phố Bà Rịa', 49),
(551, 'Huyện Châu Đức', 49),
(552, 'Huyện Xuyên Mộc', 49),
(553, 'Huyện Long Điền', 49),
(554, 'Huyện Đất Đỏ', 49),
(555, 'Huyện Tân Thành', 49),
(556, 'Huyện Côn Đảo', 49),
(557, 'Quận 1', 50),
(558, 'Quận 12', 50),
(559, 'Quận Thủ Đức', 50),
(560, 'Quận 9', 50),
(561, 'Quận Gò Vấp', 50),
(562, 'Quận Bình Thạnh', 50),
(563, 'Quận Tân Bình', 50),
(564, 'Quận Tân Phú', 50),
(565, 'Quận Phú Nhuận', 50),
(566, 'Quận 2', 50),
(567, 'Quận 3', 50),
(568, 'Quận 10', 50),
(569, 'Quận 11', 50),
(570, 'Quận 4', 50),
(571, 'Quận 5', 50),
(572, 'Quận 6', 50),
(573, 'Quận 8', 50),
(574, 'Quận Bình Tân', 50),
(575, 'Quận 7', 50),
(576, 'Huyện Củ Chi', 50),
(577, 'Huyện Hóc Môn', 50),
(578, 'Huyện Bình Chánh', 50),
(579, 'Huyện Nhà Bè', 50),
(580, 'Huyện Cần Giờ', 50),
(581, 'Thành phố Tân An', 51),
(582, 'Thị xã Kiến Tường', 51),
(583, 'Huyện Tân Hưng', 51),
(584, 'Huyện Vĩnh Hưng', 51),
(585, 'Huyện Mộc Hóa', 51),
(586, 'Huyện Tân Thạnh', 51),
(587, 'Huyện Thạnh Hóa', 51),
(588, 'Huyện Đức Huệ', 51),
(589, 'Huyện Đức Hòa', 51),
(590, 'Huyện Bến Lức', 51),
(591, 'Huyện Thủ Thừa', 51),
(592, 'Huyện Tân Trụ', 51),
(593, 'Huyện Cần Đước', 51),
(594, 'Huyện Cần Giuộc', 51),
(595, 'Huyện Châu Thành', 51),
(596, 'Thành phố Mỹ Tho', 52),
(597, 'Thị xã Gò Công', 52),
(598, 'Thị xã Cai Lậy', 52),
(599, 'Huyện Tân Phước', 52),
(600, 'Huyện Cái Bè', 52),
(601, 'Huyện Cai Lậy', 52),
(602, 'Huyện Châu Thành', 52),
(603, 'Huyện Chợ Gạo', 52),
(604, 'Huyện Gò Công Tây', 52),
(605, 'Huyện Gò Công Đông', 52),
(606, 'Huyện Tân Phú Đông', 52),
(607, 'Thành phố Bến Tre', 53),
(608, 'Huyện Châu Thành', 53),
(609, 'Huyện Chợ Lách', 53),
(610, 'Huyện Mỏ Cày Nam', 53),
(611, 'Huyện Giồng Trôm', 53),
(612, 'Huyện Bình Đại', 53),
(613, 'Huyện Ba Tri', 53),
(614, 'Huyện Thạnh Phú', 53),
(615, 'Huyện Mỏ Cày Bắc', 53),
(616, 'Thành phố Trà Vinh', 54),
(617, 'Huyện Càng Long', 54),
(618, 'Huyện Cầu Kè', 54),
(619, 'Huyện Tiểu Cần', 54),
(620, 'Huyện Châu Thành', 54),
(621, 'Huyện Cầu Ngang', 54),
(622, 'Huyện Trà Cú', 54),
(623, 'Huyện Duyên Hải', 54),
(624, 'Thị xã Duyên Hải', 54),
(625, 'Thành phố Vĩnh Long', 55),
(626, 'Huyện Long Hồ', 55),
(627, 'Huyện Mang Thít', 55),
(628, 'Huyện Vũng Liêm', 55),
(629, 'Huyện Tam Bình', 55),
(630, 'Thị xã Bình Minh', 55),
(631, 'Huyện Trà Ôn', 55),
(632, 'Huyện Bình Tân', 55),
(633, 'Thành phố Cao Lãnh', 56),
(634, 'Thành phố Sa Đéc', 56),
(635, 'Thị xã Hồng Ngự', 56),
(636, 'Huyện Tân Hồng', 56),
(637, 'Huyện Hồng Ngự', 56),
(638, 'Huyện Tam Nông', 56),
(639, 'Huyện Tháp Mười', 56),
(640, 'Huyện Cao Lãnh', 56),
(641, 'Huyện Thanh Bình', 56),
(642, 'Huyện Lấp Vò', 56),
(643, 'Huyện Lai Vung', 56),
(644, 'Huyện Châu Thành', 56),
(645, 'Thành phố Long Xuyên', 57),
(646, 'Thành phố Châu Đốc', 57),
(647, 'Huyện An Phú', 57),
(648, 'Thị xã Tân Châu', 57),
(649, 'Huyện Phú Tân', 57),
(650, 'Huyện Châu Phú', 57),
(651, 'Huyện Tịnh Biên', 57),
(652, 'Huyện Tri Tôn', 57),
(653, 'Huyện Châu Thành', 57),
(654, 'Huyện Chợ Mới', 57),
(655, 'Huyện Thoại Sơn', 57),
(656, 'Thành phố Rạch Giá', 58),
(657, 'Thị xã Hà Tiên', 58),
(658, 'Huyện Kiên Lương', 58),
(659, 'Huyện Hòn Đất', 58),
(660, 'Huyện Tân Hiệp', 58),
(661, 'Huyện Châu Thành', 58),
(662, 'Huyện Giồng Riềng', 58),
(663, 'Huyện Gò Quao', 58),
(664, 'Huyện An Biên', 58),
(665, 'Huyện An Minh', 58),
(666, 'Huyện Vĩnh Thuận', 58),
(667, 'Huyện Phú Quốc', 58),
(668, 'Huyện Kiên Hải', 58),
(669, 'Huyện U Minh Thượng', 58),
(670, 'Huyện Giang Thành', 58),
(671, 'Quận Ninh Kiều', 59),
(672, 'Quận Ô Môn', 59),
(673, 'Quận Bình Thuỷ', 59),
(674, 'Quận Cái Răng', 59),
(675, 'Quận Thốt Nốt', 59),
(676, 'Huyện Vĩnh Thạnh', 59),
(677, 'Huyện Cờ Đỏ', 59),
(678, 'Huyện Phong Điền', 59),
(679, 'Huyện Thới Lai', 59),
(680, 'Thành phố Vị Thanh', 60),
(681, 'Thị xã Ngã Bảy', 60),
(682, 'Huyện Châu Thành A', 60),
(683, 'Huyện Châu Thành', 60),
(684, 'Huyện Phụng Hiệp', 60),
(685, 'Huyện Vị Thuỷ', 60),
(686, 'Huyện Long Mỹ', 60),
(687, 'Thị xã Long Mỹ', 60),
(688, 'Thành phố Sóc Trăng', 61),
(689, 'Huyện Châu Thành', 61),
(690, 'Huyện Kế Sách', 61),
(691, 'Huyện Mỹ Tú', 61),
(692, 'Huyện Cù Lao Dung', 61),
(693, 'Huyện Long Phú', 61),
(694, 'Huyện Mỹ Xuyên', 61),
(695, 'Thị xã Ngã Năm', 61),
(696, 'Huyện Thạnh Trị', 61),
(697, 'Thị xã Vĩnh Châu', 61),
(698, 'Huyện Trần Đề', 61),
(699, 'Thành phố Bạc Liêu', 62),
(700, 'Huyện Hồng Dân', 62),
(701, 'Huyện Phước Long', 62),
(702, 'Huyện Vĩnh Lợi', 62),
(703, 'Thị xã Giá Rai', 62),
(704, 'Huyện Đông Hải', 62),
(705, 'Huyện Hoà Bình', 62),
(706, 'Thành phố Cà Mau', 63),
(707, 'Huyện U Minh', 63),
(708, 'Huyện Thới Bình', 63),
(709, 'Huyện Trần Văn Thời', 63),
(710, 'Huyện Cái Nước', 63),
(711, 'Huyện Đầm Dơi', 63),
(712, 'Huyện Năm Căn', 63),
(713, 'Huyện Phú Tân', 63),
(714, 'Huyện Ngọc Hiển', 63);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `provicence_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `note` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `fullname`, `email`, `phone`, `address`, `provicence_id`, `district_id`, `amount`, `note`, `created_at`, `updated_at`, `status`) VALUES
(1, NULL, 'nguyenvan aaaa', 'test@gmail.com', '01234731395', 'ha noi', 39, 158, '5', '', '2018-04-03', '2018-04-26', 1),
(2, 2, 'cbcvbcvbcv', 'cvbcvbd', 'fgdfgs', 'fsfasfsdfsd', 35, 225, '12', 'cvgdfzfdsfsd', '2018-04-04', '2018-04-09', 0),
(3, 2, 'sdffsf', 'sdscczc', 'vssdfsfsdfsdfsd', 'zxxcvxcvxvxc', 3, 102, 'xcvxvxcvsfs', 'xcvxvsfsfsdfsd', '2018-04-03', '2018-04-03', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `price`, `qty`) VALUES
(1, 1, 186, '1555', 3),
(3, 3, 220, '3442', 323);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `content` text NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `content` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `post_category_id` int(11) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `is_featured` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_categories`
--

CREATE TABLE `post_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `description` text,
  `parent_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'Laptop , phụ kiện giá rẻ',
  `slug` varchar(255) NOT NULL,
  `price` varchar(255) DEFAULT NULL,
  `colors` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `qty` float NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `description` text,
  `content` text,
  `views` int(11) NOT NULL DEFAULT '1',
  `is_new` tinyint(4) DEFAULT '0',
  `is_promo` tinyint(4) DEFAULT '0',
  `is_featured` tinyint(4) DEFAULT '0',
  `is_sale` int(11) DEFAULT '0',
  `created_at` date NOT NULL DEFAULT '2018-02-06',
  `updated_at` date NOT NULL DEFAULT '2018-04-18',
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `slug`, `price`, `colors`, `size`, `qty`, `brand_id`, `product_category_id`, `description`, `content`, `views`, `is_new`, `is_promo`, `is_featured`, `is_sale`, `created_at`, `updated_at`, `status`) VALUES
(164, 'LTAS880', 'Laptop , phụ kiện giá rẻ', 'laptop-asus-rog-strix-gl553vd-fy305-vga-1050', '23599', 'Đen', '15.6', 0, 2, 11, 'Bộ vi xử lý Intel® Core™ i7 7700HQ Kabylake\nChipset Intel® HM175\nBộ nhớ trong 8GB DDR4\nVGA NVIDIA GeForce GTX 1050 4GB GDDR5 VRAM\nỔ cứng 1TB 7200RPM SSH\nỔ quang Super-Multi DVD\nCard Reader\nBảo mật, công nghệ Đèn nền bàn phím với 4 dải màu\nMàn hình 15.6\" (16:9) LED backlit FHD (1920x1080) IPS, 300nits', 'ROG Strix GL753 được cài đặt sẵn Windows 10 và mang đến bạn những trải nghiệm chơi game thật sự đắm chìm nhờ bộ vi xử lý Intel® Core™ i7 thế hệ thứ 7 và đồ họa rời NVIDIA® GeForce® GTX 1050Ti. ROG Strix GL753 được sản xuất cho chơi game và rất thích hợp cho việc sáng tạo. Đã đến lúc trải nghiệm game và ứng dụng như bạn chưa từng được trải nghiệm trước đây!', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(165, 'LTAS885', 'Laptop , phụ kiện giá rẻ', 'laptop-asus-rog-strix-gl753ve-gc059-vga-1050ti', '26489', 'Xanh', '15.6', 0, 2, 11, 'Bộ vi xử lý Intel® Core™ i7 7700HQ Kabylake\nChipset Intel® HM175\nBộ nhớ trong 8GB DDR4\nVGA NVIDIA GeForce GTX 1050Ti 4GB GDDR5 VRAM\nỔ cứng 1TB 7200RPM SSH\nỔ quang Super-Multi DVD\nCard Reader\nBảo mật, công nghệ Đèn nền bàn phím với 4 dải màu\nMàn hình 17.3\" (16:9) LED backlit FHD (1920x1080) IPS , 300 nits', 'ROG Strix GL753 sở hữu một bàn phím chiclet được thiết kế đặc biệt với ánh sáng RGB 4 khu vực và cơ cấu nút phím dạng kéo có đèn nền màu đỏ và hành trình phím là 2,5mm cho một trải nghiệm đánh máy chắc chắn. Bàn phím còn có các phím WASD được in màu để bạn luôn biết vị trí để tay của mình. Thêm vào đó, dấu cách lớn hơn và các phím mũi tên tách biệt đảm bảo rằng mọi thứ bạn cần đều luôn nằm trong tầm với.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(167, 'LTMA001', 'Laptop , phụ kiện giá rẻ', 'apple-macbook-pro-15-touchbar-mptv2saa-2017', '60999', 'Bạc', '15.4', 0, 3, 4, 'Bộ vi xử lý Intel Kabylake Core i7-7820HQ 4 core/8 thread 2.9Ghz-3.9Ghz\nBộ nhớ trong 16GB LP DDR3 bus 2133Mhz\nVGA AMD Radeon Pro 560 - 4GB GDDR5\nỔ cứng 512GB SSD PCIe 3.0\nBảo mật, Công nghệ TouchID\nMàn hình Retina 15.4\" độ phân giải 2880x1800', 'Touch Bar và Touch ID - một cuộc cách mạng về cách sử dụng máy MAC của bạn\n\nTouch Bar sẽ thay thế các phím chức năng từ lâu đã chiếm phần trên cùng của bàn phím. Nó tự động thay đổi dựa trên những gì bạn đang làm để hiển thị các công cụ có liên quan như điều khiển độ sáng, âm lượng hệ thống... Touch ID cho phép bạn sử dụng tính năng sinh trắc học để xác nhận mật khẩu, mua hàng trực tuyến nhanh hơn với Apple Pay', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(168, 'LTMA006', 'Laptop , phụ kiện giá rẻ', 'apple-macbook-pro-13-mpxt2saa', '33999', 'Xám', '13.6', 0, 3, 4, 'Bộ vi xử lý Intel Kabylake Core i5-7360U 2 core/4 thread 2.3Ghz-3.6Ghz\nBộ nhớ trong 8GB LP DDR3 bus 2133Mhz\nVGA Intel Iris Plus Graphics 640\nỔ cứng 256GB SSD PCIe 3.0\nMàn hình Retina 13.3\" độ phân giải 2560x1600\nWebcam 720p HD', 'Bàn phím tốt hơn và bàn di rộng hơn\n\nBàn phím cơ chế cánh bướm thế hệ thứ 2 mang lại sự ổn định gấp 4 lần so với những bàn phím thông thường cùng với độ phản hồi tốt hơn. Bàn di Force Touch rộng rãi giúp bạn có nhiều không gian để sử dụng các cử chỉ hơn.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(169, 'LTMA009', 'Laptop , phụ kiện giá rẻ', 'apple-macbook-pro-13-mpxr2saa', '29299', 'Bạc', '13.6', 0, 3, 4, 'Bộ vi xử lý Intel Kabylake Core i5-7360U 2 core/4 thread 2.3Ghz-3.6Ghz\nBộ nhớ trong 8GB LP DDR3 bus 2133Mhz\nVGA Intel Iris Plus Graphics 640\nỔ cứng 128GB SSD PCIe 3.0\nMàn hình Retina 13.3\" độ phân giải 2560x1600', 'Thời lượng pin ấn tương, đủ dùng cả ngày\n\nMacbook Pro mang lại hiệu suất cao trong một thiết kế mỏng nhưng vẫn cung cấp đủ thời lượng pin cho cả ngày - lên đến 10 tiếng. Vì vậy bạn có thể hoàn toàn thoải mái sử dụng ở bất cứ đâu.\n\n', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(170, 'LTMA013', 'Laptop , phụ kiện giá rẻ', 'apple-macbook-12-mnyl2saa', '36399', 'Vàng', '12', 0, 3, 4, 'Bộ vi xử lý Intel Kabylake Core i5-7Y54 2 core/4 thread 1.3Ghz-3.2Ghz\nBộ nhớ trong 8GB LP DDR3 bus 1866Mhz\nVGA Intel HD Graphics 615\nỔ cứng 512GB SSD PCIe 3.0\nBảo mật, Công nghệ TouchID\nMàn hình Retina 12\" độ phân giải 2304x1440', 'Siêu nhẹ\n\nMục tiêu của chúng tôi khi làm ra Macbook là làm được những điều bất khả thi: mang lại một trải nghiệm hoàn chỉnh cho một chiếc máy tính xách tay MAC mỏng nhất, nhẹ nhất. Nó không chỉ nhỏ gọn mà còn rất mạnh mẽ, Macbook mới mang lại tốc độ nhanh hơn 20% với các bộ xử lý Core m3, i5, i7 thế hệ thứ 7 và bộ nhớ SSD nhanh hơn đến 50%', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(171, 'LTMA014', 'Laptop , phụ kiện giá rẻ', 'apple-macbook-12-mnyf2saa', '30999', 'Xám', '12', 0, 3, 4, 'Bộ vi xử lý Intel Kabylake Core M3-7Y32 2 core/4 thread 1.2Ghz-3.0Ghz\nBộ nhớ trong 8GB LP DDR3 bus 1866Mhz\nVGA Intel HD Graphics 615\nỔ cứng 256GB SSD PCIe 3.0\nBảo mật, Công nghệ TouchID\nMàn hình Retina 12\" độ phân giải 2304x1440', 'Mạnh mẽ trên từng Milimet\n\nGiờ đây, được trang bị bộ vi xử lý Intel Core thế hệ thứ 7, Macbook đã trở nên mạnh mẽ hơn bao giờ hết. Từ các tác vụ hàng ngày như khởi chạy ứng dụng, và mở các tệp tin đến các ứng dụng tính toán phức tạp đã nhanh hơn rất nhiều nhờ tốc độ SSD nhanh hơn và mức xung Turbo Boost lên đến 3.6Ghz', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(172, 'LTLE456', 'Laptop , phụ kiện giá rẻ', 'laptop-lenovo-ideapad-720s-part-number-81bv0061vn', '19999', 'Vàng', '13', 0, 6, 7, 'Bộ vi xử lý Intel Core i5-8250U Processor (4 x 1.6Ghz ),Max Turbo Frequency: 3.40 GHz\nChipset Intel\nBộ nhớ trong 8GB DDR4 Onboard/ 2400MHz\nVGA On\nỔ cứng 256GB SSD PCIe (M2 2280)\nỔ quang\nCard Reader\nBảo mật, công nghệ Fingerprint, Administrator Password, User Password, Intel Platform Trust Technology\nMàn hình 13.3\" FHD (1920 x 1080) IPS - AntiGlare', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(173, 'LTDE893', 'Laptop , phụ kiện giá rẻ', 'laptop-dell-vostro-5471-42vn530w02', '21999', 'Bạc', '14', 0, 4, 5, 'Bộ vi xử lý Intel Core™ i5 8250U (1.6, 6MB Cache) CoffeeLake\nChipset Intel\nBộ nhớ trong 8GB DDR4 2400MHz\nVGA Intel 620\nỔ cứng 1TB + 128GB SSD\nỔ quang No\nCard Reader Yes\nBảo mật, công nghệ\nMàn hình 14.0” FHD (1920 * 1080), LED backlight', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(174, 'LTDE902', 'Laptop , phụ kiện giá rẻ', 'laptop-dell-vostro-5468-vti5019', '14689', 'Vàng', '14', 0, 4, 5, 'Bộ vi xử lý Intel Core™ i5 7200U (2.5G, 3MB Cache) Kabylake\nChipset Intel\nBộ nhớ trong 4GB DDR4 2400MHz\nVGA Intel HD 620\nỔ cứng 500GB SATA 5400rpm\nỔ quang\nCard Reader Yes\nBảo mật, công nghệ Finger Print\nMàn hình 14.0” HD LED', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(175, 'LTDE844', 'Laptop , phụ kiện giá rẻ', 'laptop-dell-inspiron-n7567a-p65f001gaming-i7-vga-1050ti', '26799', 'Đỏ', '15.6', 0, 4, 12, 'Bộ vi xử lý\n\nIntel Core™ i7 7700HQ (2.6G upto 3.8Ghz, 6MB Cache) Kabylake\n\nChipset\n\nIntel HM170\n\nBộ nhớ trong\n\n8GB DDR4 2400Mhz\n\nSố khe cắm\n\n2', 'Máy dành riêng cho \"Game thủ\"?\nCấu hình mạnh nhưng không có nghĩa máy chỉ được sử dụng với mục đích chơi game. Dell Inspiron 7567 i7 7700HQ còn có thể cài đặt các phần mềm: Microsoft Office, các phần mềm đồ hoạ, phần mềm kế toán, thương mại điện tử… thuận tiện cho công việc.\n\n', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(176, 'LTDE856', 'Laptop , phụ kiện giá rẻ', 'laptop-dell-inspiron-7566-70091106-gaming-i5', '16699', 'Đen', '15.6', 0, 4, 12, 'Bộ vi xử lý: Intel Core™ i5 6300HQ (2.3Ghz upto 3.2Ghz, 6MB Cache) Skylake\nChipset: Intel HM170\nBộ nhớ trong: 4GBDDR4 2400Mhz\nVGA: Nvidia Geforce GTX960M 4G DDR5\nỔ cứng: 1TB SATA 5400rpm\nMàn hình: 15.6” Full HD 1920 * 1080 Led Backlit', 'Dell Inspiron 7566 được trang bị chíp intel Core i5 Skylake, xung nhịp 2,3Ghz. Một sự cải tiến khác là Ram DDR4 với tốc độ nhanh hơn, ít tiêu tốn điện năng hơn giúp duy trì thời lượng sử dụng pin lâu hơn và mát hơn.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(177, 'LTDE827', 'Laptop , phụ kiện giá rẻ', 'laptop-dell-inspiron-3567-70093474-kabylake', '12999', 'Đen', '15.6', 0, 4, 5, 'Bộ vi xử lý Intel Core i5 7200U (2*2.5Ghz/ 3MB Cache) Kabylake\nChipset Intel\nBộ nhớ trong 4GB DDR4 2400Mhz\nVGA AMD Radeon 2G DDR3\nỔ cứng 500B SATA 5400rpm\nỔ quang DVDRW\nCard Reader Yes\nBảo mật, công nghệ\nMàn hình 15.6” HD LED', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(178, 'LTHP534', 'Laptop , phụ kiện giá rẻ', 'laptop-hp-14bs100tu-3cy83pa', '12599', 'Bạc', '14', 0, 5, 6, 'Bộ vi xử lý Intel® Core i5 8250U(4x1.6Ghz /6MB L3 cache) Coffee Lake\nChipset Intel\nBộ nhớ trong 4 GB DDR4-2133 SDRAM (1 x 4 GB)\nVGA Intel HD 620\nỔ cứng 1Tb 5400 rpm SATA\nỔ quang No\nCard Reader 1 đầu đọc thẻ phương tiện SD đa định dạng\nBảo mật, công nghệ Kensington MicroSaver® lock slot\nMàn hình 14.0\" diagonal HD SVA BrightView WLED-backlit (1366 x 768)', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(179, 'LTHP547', 'Laptop , phụ kiện giá rẻ', 'laptop-hp-pavilion-15-cc104tu-3ch57pa', '13499', 'Xám', '14', 0, 5, 6, 'Bộ vi xử lý Intel® Core i5 8250U(4*1.6Ghz /6MB cache)\nChipset Intel\nBộ nhớ trong 4 GB DDR4 2400Mhz\nVGA Intel HD 620\nỔ cứng 1TB 5400 rpm SATA\nỔ quang DVDRW\nCard Reader multi-format SD media card reader\nBảo mật, công nghệ Administrator Password, Power-On Password, TPM Device\nMàn hình 15.6\" HD BrightView WLED-backlit (1366 x 768)', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(180, 'LTHP412', 'Laptop , phụ kiện giá rẻ', 'laptop-hp-pavilion-gaming-15-bc018tx-x3c06pa', '22899', 'Bạc', '15.6', 0, 5, 13, 'Bộ vi xử lý Intel® Core i7 6700HQ(2.6Ghz /6MB cache)\nChipset Intel HM170\nBộ nhớ trong 8GB DDR4 1600Mhz (1x8Gb)\nSố khe cắm 2\nDung lượng tối đa\nVGA NVIDIA® GeForce® GTX 960M 4Gb DDR5\nỔ cứng 1TB SATA 7200rpm\nCard Reader Yes\nBảo mật No\nMàn hình 15.6\" FHD IPS UWVA anti-glare WLED-backlit', 'HP Pavilion Gaming 15 đang được rất nhiều người sử dụng lựa chọn bởi thiết kế đẹp mắt và cấu hình cao đáp ứng hoàn hảo nhu cầu làm việc cũng như giải trí.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(181, 'LTLE444', 'Laptop , phụ kiện giá rẻ', 'laptop-lenovo-ideapad-320-14ast-80xu001xvn', '8099', 'Xám bạc', '14', 0, 6, 7, 'Bộ vi xử lý AMD A9-9420 APU WITH AMD RADEON™ R5\nChipset AMD\nBộ nhớ trong 4G(1X4GBDDR4 2133)\nỔ cứng 1TB 7MM 5400RPM\nỔ quang No\nCard Reader 4-in-1\nBảo mật, công nghệ\nMàn hình 14.0 FHD TN AG SLIM', 'IdeaPad 320S có bề mặt sơn trơn với thiết kế cứng cáp, sử dụng vi xử lý intel Core i thế hệ thứ 7, bổ sung thêm ram và bộ nhớ cho hiệu năng sử dụng mạnh mẽ, Màn hình sắc nét với thiết kế kim loại cao cấp được thiết kế để đáp ứng các nhu cầu giải trí theo phong cách của bạn', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(182, 'LTLE427', 'Laptop , phụ kiện giá rẻ', 'laptop-lenovo-gaming-y720-15ikb-80vr009cvn-vga-1060-6g', '38999', 'Đen', '15.6', 0, 6, 14, 'Bộ vi xử lý Intel® Core™ i7-7700HQ (2.8Ghz/6MB)\nChipset Intel HM\nBộ nhớ trong 16GB DDR4 (2*8G)\nVGA Nvidia Geforce GTX1060 6GB DDR5\nỔ cứng 1TB + 128GB SSD\nỔ quang Ext Blueray\nCard Reader 4 in 1\nBảo mật, công nghệ Multi Color Backlit Keyboard, Dual Metal Fans\nMàn hình 15.6” Full HD 1920 * 1080 Anti-Glare IPS', 'Y720 vi xử lý Intel® Core™ thế hệ thứ bảy là chiếc laptop lý tưởng cho bạn, có khả năng đáp ứng hầu hết các yêu cầu bạn cần từ chơi game cho đến các tác vụ đồ họa di động như dựng phim/ dựng hình 3D hay thiết kế đồ họa.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(183, 'LTLG001', 'Laptop , phụ kiện giá rẻ', 'laptop-lg-gram-13zd970-gax51a5', '23899', 'Trắng', '13.3', 0, 8, 8, 'Bộ vi xử lý Intel® Core™ i7-7700HQ (2.8Ghz/6MB)\nChipset Intel HM\nBộ nhớ trong 16GB DDR4 (2*8G)\nVGA Nvidia Geforce GTX1060 6GB DDR5\nỔ cứng 1TB + 128GB SSD\nỔ quang Ext Blueray\nCard Reader 4 in 1\nBảo mật, công nghệ Multi Color Backlit Keyboard, Dual Metal Fans\nMàn hình 15.6” Full HD 1920 * 1080 Anti-Glare IPS', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(184, 'LTMS218', 'Laptop , phụ kiện giá rẻ', 'laptop-msi-gaming-gv62-7rd-1882xvn', '22699', 'Xám', '15.6', 0, 7, 16, 'Bộ vi xử lý Kabylake i7-7700HQ\nChipset Intel HM175\nBộ nhớ trong 8GB DDR4 bus 2400\nVGA GeForce® GTX 1050, 4GB GDDR5\nỔ cứng 1TB (SATA) 7200rpm\nỔ quang None\nCard Reader 1x SD (XC/HC)\nBảo mật, công nghệ Backlight Keyboard (Single-Color, Red)\nMàn hình 15.6\" FHD (1920*1080), wideview 94%NTSC color Anti-glare', 'Với giải pháp làm mát Cooler Boost 4 sáng tạo và các tính năng chơi game độc ​​quyền của MSI dành cho game thủ, bộ vi xử lý Intel Kaby Lake Core i7 7700HQ trang bị trên MSI GV62 7RD-1882XVN sẽ đảm bảo cho bạn một hiệu suất vô cùng mạnh mẽ.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(185, 'LTMS225', 'Laptop , phụ kiện giá rẻ', 'laptop-msi-gv62-7re-2443xvn-kabylake-i7-7700hq', '24699', 'Trắng', '15.6', 0, 7, 16, 'Bộ vi xử lý Kabylake i7-7700HQ\nChipset Intel HM175\nBộ nhớ trong 8GB DDR4 bus 2400\nVGA GeForce® GTX 1050Ti 4GB GDDR5\nỔ cứng 1TB (SATA) 7200rpm\nỔ quang None\nCard Reader 1x SD (XC/HC)\nBảo mật, công nghệ Backlight Keyboard (Single-Color, Red)\nMàn hình 15.6\" FHD (1920*1080), wideview 94%NTSC color Anti-glare', 'MSI GV62 7RD-1882XVN được trang bị card đồ họa NVIDIA Geforce GTX 1050 được xây dựng trên kiến trúc Pascal mới nhất, cho chiếc laptop của bạn một hiệu suất mạnh mẽ có thể xử lý tốt hầu hết các tựa game và các ứng dụng đòi hỏi khả năng đồ họa cao.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(186, 'LTMS087', 'Laptop , phụ kiện giá rẻ', 'laptop-msi-workstation-ws60-6qi', '39399', 'Đen', '15.6', 0, 7, 17, 'Bộ vi xử lý Skylake i7 6700HQ\nChipset Intel HM170\nBộ nhớ trong 16GB (8GB *2) DDR4 2133Mhz\nSố khe cắm 2\nDung lượng tối đa 32GB\nVGA NVIDIA® Quadro® M1000M, 2GB GDDR5\nỔ cứng 1TB (SATA) 7200rpm\nỔ quang\nCard Reader Yes\nBảo mật, Công nghê Single Color Backlit Keyboard\nMàn hình 15.6\" FHD, Anti-Glare (1920*080) eDP Wide View Angle', 'Laptop MSI WS60 6QI là cỗ máy laptop workstation chuyên dụng dành riêng cho những người làm đồ họa từ MSI. Máy được trang bị cấu hình mạnh với vi xử lý mới nhất đến từ Intel, dòng vi xử lý Intel Core i7-6700HQ cao cấp nhất dành cho laptop, 8GB RAM DDR4, Card đồ họa chuyên dụng Quadro M1000M dung lượng 2GB DDR5.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(187, 'LTMS083', 'Laptop , phụ kiện giá rẻ', 'laptop-msi-workstation-ws63-7rk-ban-option', '75999', 'Đen', '15.6', 0, 7, 15, 'Bộ vi xử lý Skylake i7 6700HQ\nChipset Intel HM170\nBộ nhớ trong 16GB (8GB *2) DDR4 2133Mhz\nSố khe cắm 2\nDung lượng tối đa 32GB\nVGA NVIDIA® Quadro® M1000M, 2GB GDDR5\nỔ cứng 1TB (SATA) 7200rpm\nỔ quang\nCard Reader Yes\nBảo mật, Công nghê Single Color Backlit Keyboard\nMàn hình 15.6\" FHD, Anti-Glare (1920*080) eDP Wide View Angle', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(188, 'RASP012', 'Laptop , phụ kiện giá rẻ', 'ram-silicon-power-8gb-18gb-ddr4-bus-2133-sodimm-notebook', '2099', NULL, '\n8GB', 0, 15, 19, 'Density: 8GB (1x8GB)\nSpeed: 2133MHz\nTested Latency: 15-15-15-36', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(189, 'RACO294', 'Laptop , phụ kiện giá rẻ', 'ram-corsair-8gb-4x8gb-ddr4-bus-2133mhz-cmso8gx4m1a2133c15-for-laptop-12v', '1999', NULL, '8GB', 0, 12, 19, 'Density: 8GB (1x8GB)\nSpeed: 2133MHz\nTested Latency: 15-15-15-36', 'Hệ thống DDR4 SODIMM nhanh, vận hành ổn định, và đáng tin cậy cho các hệ thống Core nền tảng của Intel\nĐược thiết kế cho máy tính xách tay và máy tính xách tay\n\n', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(190, 'RAKM206', 'Laptop , phụ kiện giá rẻ', 'ram-kingmax-8gb-ddr3-bus-1600-for-laptop-135v', '1509', NULL, '4GB', 0, 13, 19, 'Density: 8GB (1x8GB)\nSpeed: 2133MHz\nTested Latency: 15-15-15-36', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(191, 'RAKT219', 'Laptop , phụ kiện giá rẻ', 'ddram-4-kingston-8gb2400-for-laptop-12v', '1502', NULL, '4GB', 0, 14, 19, '• DDR4 có sự lựa chọn Bus 2133Mhz trở lên\n• Hiệu suất tăng 50%\n• ADDR / CMD chẵn lẻ trên DRAM\n• Điện áp thấp hơn 1,2 V so với DDR3 1,5 V', 'RAM của AVEXIR đều được trang bị bộ phận bảo vệ để tránh oxy hóa không khí và nước trước khi người dùng mở gói.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(192, 'RAAV124', 'Laptop , phụ kiện giá rẻ', 'ram-avexir-core-series-16gb-1x16gb-ddr4-bus-2400-mhz-1cob', '4099', NULL, '16GB', 0, 10, 19, '• DDR4 có sự lựa chọn Bus 2133Mhz trở lên\n• Hiệu suất tăng 50%\n• ADDR / CMD chẵn lẻ trên DRAM\n• Điện áp thấp hơn 1,2 V so với DDR3 1,5 V', 'RAM của AVEXIR đều được trang bị bộ phận bảo vệ để tránh oxy hóa không khí và nước trước khi người dùng mở gói.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(193, 'RAGE002', 'Laptop , phụ kiện giá rẻ', 'ddram-4-geil-8gb2400-c17sc', '2199', NULL, '4GB', 0, 28, 19, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(194, 'HDHI063', 'Laptop , phụ kiện giá rẻ', 'hdd-hitachi-hgst-1tb-7200-sata-for-laptop', '1499', NULL, '1TB', 0, 16, 20, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(195, 'HDSE106', 'Laptop , phụ kiện giá rẻ', 'hdd-seagate-500gb7200-sata-32mb-for-laptop', '1199', NULL, '500GB', 0, 17, 20, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(196, 'HDTO012', 'Laptop , phụ kiện giá rẻ', 'hdd-toshiba-500gb5400-sata-for-laptop', '1059', NULL, '500GB', 0, 18, 20, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(197, 'HDWD148', 'Laptop , phụ kiện giá rẻ', 'hdd-western-digital-black-500gb7200-sata-for-laptop', '1399', NULL, '500GB', 0, 19, 20, 'Dung lượng\n\n500GB\n\nChuẩn cắm\n\nSATA3\n\nBộ nhớ đệm\n\n16MB cache\n\nTốc độ vòng quay\n\n7200 Rpm', 'Công nghệ NoTouch Rampgiúp giảm rung động do hệ thống gây ra từ các\n\nthành phần khác giúp tối ưu hóa hiệu suất và độ tin cậy.', 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(198, 'PINN103', 'Laptop , phụ kiện giá rẻ', 'pin-nb-dell-n5010', '609', NULL, NULL, 0, 4, 21, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(199, 'PINN080', 'Laptop , phụ kiện giá rẻ', 'pin-nb-lenovo-b450', '450', NULL, NULL, 0, 6, 21, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(200, 'SACD129', 'Laptop , phụ kiện giá rẻ', 'adapter-asus-19v-474a-90w', '199', NULL, NULL, 0, 2, 22, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(201, 'SACD078', 'Laptop , phụ kiện giá rẻ', 'adapter-compaq-hp-185v-35a-chan-kim', '250', NULL, NULL, 0, 5, 22, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(203, 'BALO438', 'Laptop , phụ kiện giá rẻ', 'balo-razer-rogue-156-backpack', '2153', NULL, NULL, 0, 22, 23, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(205, 'DELM226', 'Laptop , phụ kiện giá rẻ', 'de-lam-mat-cooler-master-sf15', '2059', NULL, NULL, 0, 25, 24, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(206, 'DELM083', 'Laptop , phụ kiện giá rẻ', 'deepcool-n200', '229', NULL, NULL, 0, 26, 24, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(207, 'NUOC002', 'Laptop , phụ kiện giá rẻ', 'nuoc-lau-man-hinh-lcd', '39', NULL, NULL, 0, 31, 27, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(208, 'PADM320', 'Laptop , phụ kiện giá rẻ', 'mouse-pad-corsair-vengeance-mm200-medium-gaming-ch-9000099-ww', '329', NULL, NULL, 0, 12, 26, 'Kích cỡ: 360mm x 300mm\nĐộ dày: 2mm\nChất liệu: Cao su tổng hợp, bên trên phủ vải cao cấp.\nBề mặt vi mô đen tuyền giúp mắt đọc của chuột tối ưu hoá hiệu năng\nMặt sau bàn di là đế cao su cao cấp không đọc hại, dày 1,5mm chống trơn trượt', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(209, 'KBFU009', 'Laptop , phụ kiện giá rẻ', 'keyboard-fuhlen-game-pro-g450-usb', '389', NULL, NULL, 0, 31, 27, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(210, 'BAOD122', 'Laptop , phụ kiện giá rẻ', 'bao-da-ipad-mini-pisen-clever-cover-nau', '529', NULL, NULL, 0, 3, 29, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(212, 'CABH140', 'Laptop , phụ kiện giá rẻ', 'cable-hdmi-10m-ugreen-hdmi-sang-dvi/', '429', NULL, NULL, 0, 20, 29, '', NULL, 1, 0, 0, 0, 0, '2018-02-06', '2018-04-18', 0),
(213, 'sdfsdfsd', 'đây là sản phẩm test', 'day-la-san-pham-test', '155541', 'xanh', '123123', 121212, 17, 19, 'sdfsdfsdfsdfvvsc', 'sfsdfsdfsdfsdfsdfsf', 1, 0, 1, 0, 1, '2018-04-21', '2018-04-21', 0),
(214, 'laptop24h', 'Quả bóng đá laptop', 'Qua-bong-da-laptop', '1142514dfdf', '131414', 'dfgdfgdfg', 0, 17, 17, 'dfgfdgfdgdfgdf', '', 1, 0, 0, 0, 0, '2018-04-21', '2018-04-21', 0),
(220, 'sdfsfsdfswerwr', 'sản phẩm test 232323', 'san-pham-test-232323', 'sdfdsfsdf', 'ưerwerwer', 'fdsfdsfsdfew', 0, 18, 20, 'sdfsdfsdf', 'sfwerwer', 1, 0, 0, 1, 1, '2018-04-22', '2018-04-22', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text,
  `img` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) CHARACTER SET utf32 DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `description`, `img`, `parent_id`, `meta_title`, `meta_keyword`, `meta_description`, `status`) VALUES
(1, 'Laptop', 'laptop', '', '', 0, NULL, NULL, NULL, 0),
(2, 'Laptop Acer', 'Laptop-Acer', 'Laptop Acer đã sửa lại không bị mất cha', '', 1, '', '', '', 0),
(3, 'xfgsdgfsdfsdfsdf', 'xfgsdgfsdfsdfsdf', 'xcvxcvxvsfsd', '000_DV914287.jpg', 1, 'gyjgh', 'fghfh', 'tyertyryrtyrty', 1),
(4, 'Laptop Apple', 'laptop-apple', 'Laptop Apple', '', 1, NULL, NULL, NULL, 0),
(5, 'Laptop Dell', 'laptop-dell', 'Laptop Dell', '', 1, NULL, NULL, NULL, 0),
(6, 'Laptop HP', 'laptop-hp', 'Laptop HP', '', 1, NULL, NULL, NULL, 0),
(7, 'Laptop Lenovo', 'laptop-lenovo', 'Laptop Lenovo', '', 1, NULL, NULL, NULL, 0),
(8, 'Laptop LG', 'laptop-lg', 'Laptop LG', '', 1, NULL, NULL, NULL, 0),
(9, 'Acer Gaming', 'acer-gaming', 'Acer gaming', '', 2, NULL, NULL, NULL, 0),
(10, 'Acer other', 'acer-other', 'Acer workstation', '', 2, NULL, NULL, NULL, 0),
(11, 'Asus gaming', 'asus-gaming', 'Asus gaming', '', 3, NULL, NULL, NULL, 0),
(12, 'Dell gaming', 'dell-gaming', 'Dell gaming', '', 5, NULL, NULL, NULL, 0),
(13, 'HP gaming', 'hp-gaming', 'Hp gaming', '', 6, NULL, NULL, NULL, 0),
(14, 'Lenovo gaming', 'lenovo-gaming', 'Lenovo gaming', '', 7, NULL, NULL, NULL, 0),
(15, 'Laptop MSI', 'laptop-msi', 'MSI laptop', '', 1, NULL, NULL, NULL, 0),
(16, 'MSI gaming', 'msi-gaming', 'MSI gaming', '', 15, NULL, NULL, NULL, 0),
(17, 'MSI workstation', 'msi-workstation', 'MSI workstation', '', 15, NULL, NULL, NULL, 0),
(18, 'Phụ kiện laptop', 'phu-kien-laptop', 'Linh phụ kiện Laptop', '', 0, NULL, NULL, NULL, 0),
(19, 'RAM laptop', 'ram-laptop', 'RAM laptop', '', 18, NULL, NULL, NULL, 0),
(20, 'Ổ cứng laptop', 'o-cung-laptop', 'Ổ cứng laptop', '', 18, NULL, NULL, NULL, 0),
(21, 'Pin laptop', 'pin-laptop', 'Pin laptop', '', 18, NULL, NULL, NULL, 0),
(22, 'Sạc laptop', 'sac-laptop', 'Sạc laptop', '', 18, NULL, NULL, NULL, 0),
(23, 'Túi,Balo laptop', 'tui-balo-laptop', 'Túi và balo laptop', '', 18, NULL, NULL, NULL, 0),
(24, 'Đế làm mát laptop', 'de-lam-mat-laptop', 'Đế làm mát laptop', '', 18, NULL, NULL, NULL, 0),
(25, 'Bàn laptop', 'ban-laptop', 'Bàn để laptop', '', 18, NULL, NULL, NULL, 0),
(26, 'Bàn di chuột laptop', 'ban-di-chuot-laptop', 'Bàn di chuột laptop', '', 18, NULL, NULL, NULL, 0),
(27, 'Phụ kiện laptop khác', 'phu-kien-khac-laptop', 'Phụ kiện laptop khác', '', 18, NULL, NULL, NULL, 0),
(28, 'Phụ kiện tablet', 'phu-kien-table', 'Phụ kiện tablet', '', 0, NULL, NULL, NULL, 0),
(29, 'Bao da,Case', 'bao-da-case-table', 'Bao da và case cho table', '', 28, NULL, NULL, NULL, 0),
(30, 'Sạc ,cable,kết nối', 'sac-cable-ket-noi', 'Các thiết bị hỗ trợTablet', '', 28, NULL, NULL, NULL, 0),
(31, 'sdfsdfsdfwerwerwer', 'werwerwerwer', 'sdfsdfsdfsfsfdsf', 'fsdfsdfsdfwerwerwerewfwefwfef', NULL, '', '', '', 0),
(32, 'ádasdasda', 'adasdasda', '', '', 2, '', '', '', 0),
(33, 'vxvxcv', 'vxvxcv', '', '', 2, '', '', '', 0),
(34, 'dsfsdwerwerwer', 'dsfsdwerwerwer', '', '', 0, '', '', '', 1),
(35, 'Cái này để thử', 'Cai-nay-de-thu', 'bcbcvbcvb', 'da.png', 1, '', '', '', 0),
(36, 'sadsadadwq', 'sadsadadwq', '', '', 2, '', '', '', 0),
(37, 'cvxcvxcvxsdfsdf', 'cvxcvxcvxsdfsdf', 'fsdfwerwerwerrrrrrrr', 'framework.jpg', 36, '', '', '', 0),
(38, 'cái này không có cha', 'cai-nay-khong-co-cha', 'fdgfdgfgert', '', 0, 'ểtrtert', 'gdfgdfg', 'dfgdfgdfg', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `is_featured` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `img`, `is_featured`) VALUES
(10, 203, 'vxvxcvxcvxcvxcv', 1),
(11, 203, 'hjkhjkhjkhjkhjkhj', 1),
(14, 210, 'cxvvn,kuyuyu', 1),
(15, 196, 'cvcbcvbcvbcvbcvb', 1),
(16, 209, 'vcvc', 0),
(17, 170, '000_Was3668032.jpg', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_relates`
--

CREATE TABLE `product_relates` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_relate_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_relates`
--

INSERT INTO `product_relates` (`id`, `product_id`, `product_relate_id`, `status`) VALUES
(4, 196, 200, 1),
(5, 220, 198, 0),
(6, 220, 214, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `rate` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_reviews`
--

INSERT INTO `product_reviews` (`id`, `product_id`, `user_id`, `content`, `rate`, `created_at`, `updated_at`, `status`) VALUES
(3, 210, 1, 'cxvxcvxssdfsdfsfsdf', 2, '2018-04-04', '2018-04-16', 0),
(4, 220, 2, 'dsfsdfsdfw', 2, '2018-04-02', '2018-04-08', 0),
(5, 197, 1, 'sdfsfsdfwerwerwer', 2, '2018-04-11', '2018-04-14', 0),
(6, 220, 1, 'dvvsfsdfsdf', 2, '2018-04-04', '2018-04-09', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `provinces`
--

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `code`, `zipcode`) VALUES
(1, 'Thành phố Hà Nội', '1', '100000'),
(2, 'Tỉnh Hà Giang', '2', '310000'),
(3, 'Tỉnh Cao Bằng', '4', '270000'),
(4, 'Tỉnh Bắc Kạn', '6', '960000'),
(5, 'Tỉnh Tuyên Quang', '8', '300000'),
(6, 'Tỉnh Lào Cai', '10', '330000'),
(7, 'Tỉnh Điện Biên', '11', '380000'),
(8, 'Tỉnh Lai Châu', '12', '390000'),
(9, 'Tỉnh Sơn La', '14', '360000'),
(10, 'Tỉnh Yên Bái', '15', '320000'),
(11, 'Tỉnh Hoà Bình', '17', '350000'),
(12, 'Tỉnh Thái Nguyên', '19', '250000'),
(13, 'Tỉnh Lạng Sơn', '20', '240000'),
(14, 'Tỉnh Quảng Ninh', '22', '200000'),
(15, 'Tỉnh Bắc Giang', '24', '220000'),
(16, 'Tỉnh Phú Thọ', '25', '290000'),
(17, 'Tỉnh Vĩnh Phúc', '26', '280000'),
(18, 'Tỉnh Bắc Ninh', '27', '790000'),
(19, 'Tỉnh Hải Dương', '30', '170000'),
(20, 'Thành phố Hải Phòng', '31', '180000'),
(21, 'Tỉnh Hưng Yên', '33', '160000'),
(22, 'Tỉnh Thái Bình', '34', '410000'),
(23, 'Tỉnh Hà Nam', '35', '400000'),
(24, 'Tỉnh Nam Định', '36', '420000'),
(25, 'Tỉnh Ninh Bình', '37', '430000'),
(26, 'Tỉnh Thanh Hóa', '38', '440000'),
(27, 'Tỉnh Nghệ An', '40', '460000'),
(28, 'Tỉnh Hà Tĩnh', '42', '480000'),
(29, 'Tỉnh Quảng Bình', '44', '510000'),
(30, 'Tỉnh Quảng Trị', '45', '520000'),
(31, 'Tỉnh Thừa Thiên Huế', '46', '530000'),
(32, 'Thành phố Đà Nẵng', '48', '550000'),
(33, 'Tỉnh Quảng Nam', '49', '560000'),
(34, 'Tỉnh Quảng Ngãi', '51', '570000'),
(35, 'Tỉnh Bình Định', '52', '820000'),
(36, 'Tỉnh Phú Yên', '54', '620000'),
(37, 'Tỉnh Khánh Hòa', '56', '650000'),
(38, 'Tỉnh Ninh Thuận', '58', '660000'),
(39, 'Tỉnh Bình Thuận', '60', '800000'),
(40, 'Tỉnh Kon Tum', '62', '580000'),
(41, 'Tỉnh Gia Lai', '64', '600000'),
(42, 'Tỉnh Đắk Lắk', '66', '630000'),
(43, 'Tỉnh Đắk Nông', '67', '640000'),
(44, 'Tỉnh Lâm Đồng', '68', '670000'),
(45, 'Tỉnh Bình Phước', '70', '830000'),
(46, 'Tỉnh Tây Ninh', '72', '840000'),
(47, 'Tỉnh Bình Dương', '74', '590000'),
(48, 'Tỉnh Đồng Nai', '75', '810000'),
(49, 'Tỉnh Bà Rịa - Vũng Tàu', '77', '790000'),
(50, 'Thành phố Hồ Chí Minh', '79', '700000'),
(51, 'Tỉnh Long An', '80', '850000'),
(52, 'Tỉnh Tiền Giang', '82', '860000'),
(53, 'Tỉnh Bến Tre', '83', '930000'),
(54, 'Tỉnh Trà Vinh', '84', '940000'),
(55, 'Tỉnh Vĩnh Long', '86', '890000'),
(56, 'Tỉnh Đồng Tháp', '87', '870000'),
(57, 'Tỉnh An Giang', '89', '880000'),
(58, 'Tỉnh Kiên Giang', '91', '920000'),
(59, 'Thành phố Cần Thơ', '92', '900000'),
(60, 'Tỉnh Hậu Giang', '93', '910000'),
(61, 'Tỉnh Sóc Trăng', '94', '950000'),
(62, 'Tỉnh Bạc Liêu', '95', '260000'),
(63, 'Tỉnh Cà Mau', '96', '970000'),
(65, 'provinces test demo', '2134343421212', '1212123435353');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `provicence_id` (`provicence_id`),
  ADD KEY `districts_id` (`districts_id`),
  ADD KEY `customer_group_id` (`customer_group_id`);

--
-- Chỉ mục cho bảng `customers_group`
--
ALTER TABLE `customers_group`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `province_id` (`province_id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `provicence_id` (`provicence_id`),
  ADD KEY `district_id` (`district_id`);

--
-- Chỉ mục cho bảng `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Chỉ mục cho bảng `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `post_category_id` (`post_category_id`);

--
-- Chỉ mục cho bảng `post_categories`
--
ALTER TABLE `post_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `brand_id` (`brand_id`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Chỉ mục cho bảng `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Chỉ mục cho bảng `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Chỉ mục cho bảng `product_relates`
--
ALTER TABLE `product_relates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Chỉ mục cho bảng `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `customers_group`
--
ALTER TABLE `customers_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=715;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `post_categories`
--
ALTER TABLE `post_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;

--
-- AUTO_INCREMENT cho bảng `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT cho bảng `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `product_relates`
--
ALTER TABLE `product_relates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `customers` (`id`);

--
-- Các ràng buộc cho bảng `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`customer_group_id`) REFERENCES `customers_group` (`id`),
  ADD CONSTRAINT `customers_ibfk_2` FOREIGN KEY (`provicence_id`) REFERENCES `provinces` (`id`),
  ADD CONSTRAINT `customers_ibfk_3` FOREIGN KEY (`districts_id`) REFERENCES `districts` (`id`);

--
-- Các ràng buộc cho bảng `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`);

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`provicence_id`) REFERENCES `provinces` (`id`),
  ADD CONSTRAINT `orders_ibfk_3` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`);

--
-- Các ràng buộc cho bảng `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Các ràng buộc cho bảng `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`post_category_id`) REFERENCES `post_categories` (`id`);

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_categories` (`id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`);

--
-- Các ràng buộc cho bảng `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Các ràng buộc cho bảng `product_relates`
--
ALTER TABLE `product_relates`
  ADD CONSTRAINT `product_relates_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Các ràng buộc cho bảng `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `product_reviews_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `customers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
