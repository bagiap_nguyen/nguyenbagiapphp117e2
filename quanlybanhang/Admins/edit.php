<?php  
    require '../Layouts/index.php';

    $id     = $_GET['id'];

    $admins = getOneRecord('*','admins',"id = '{$id}'"); 

    if ($id == null || count($admins) == 0) {
        header('Location:index.php');
        exit();
    }


    $errors = [];
    if (isset($_POST['submit'])) {

        if (!isset($_POST['id'])) {
            $errors[]   = 'ID không được sửa.';
        }

        if (!isset($_POST['username']) || empty($_POST['username'])) {
            $errors[]   = 'Vui lòng nhập username.';
        }

        if (!isset($_POST['password']) || empty($_POST['password'])) {
            $errors[]   = 'Vui lòng nhập password.';
        }

        if (!isset($_POST['fullname']) || empty($_POST['fullname'])) {
            $errors[]   = 'Vui lòng nhập fullname.';
        }

        if (!isset($_POST['email']) || empty($_POST['email'])) {
            $errors[]   = 'Vui lòng nhập Email.';
        }

        if ($_POST['password'] != $_POST['repass']) {
            $errors[] = 'Mật khẩu nhập lại không khớp.';
        }

        if (!isset($_POST['status'])) {
            $errors[]   = 'Vui lòng nhập trạng thái admin.';
        }

        if (!isset($_POST['role'])) {
            $errors[]   = 'Vui lòng phân quyền admin.';
        }

        $check_id       = getOneRecord('id','admins',"id = '{$_POST['id']}' AND id <> '{$id}' ");

        if (!is_null($check_id)) {
            $errors[]   = 'Admin có id đã tồn tại  .';
        }

        if (count($errors) == 0) {

            $username   = $_POST['username'];

            $password   = $_POST['password'];

            $email      = $_POST['email'];

            $fullname   = $_POST['fullname'];

            $role       = $_POST['role'];

            $status     = $_POST['status'];
            
            $check_email= getOneRecord('email','admins',
                            "email ='{$email}'");

            $check_username = getOneRecord('username','admins',
                    "username = '{$username}' ");

            if ($check_username || $check_email) {

                $errors[]      = 'Tài khoản đã tồn tại.';
            } else {

                $password       = md5($password);

                $colums     = ['username', 'password', 'email', 'fullname', 'role', 'status'];

                $data       = ["'{$username}'","'{$password}'",
                "'{$email}'","'{$fullname}'","'{$role}'","'{$status}'"];

                $updated        = [];

                for ($i=0; $i < count($colums) ; $i++) { 

                    $updated[$colums[$i]] = $data[$i];
                }

                $updatedTo      = updateData('admins',$updated," id ='{$id}'");

                if ($updatedTo) {
                    $success = 'Success!';
                } else {
                    $errors[] = 'Failed';
                }
            }

        }
    }
?>

<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User
                        <small>Edit</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    <form action="" method="POST">

                        <div class="form-group">
                                <?php  
                                    if (isset($success)) :
                                ?>
                                <label style="color: green;">
                                    <?php echo $success; ?>
                                    
                                </label>
                                <?php  
                                    endif;
                                ?>
                                <?php  
                                    if (count($errors) > 0) :
                                        foreach ($errors as $key => $value) :
                                ?>
                                <p style="color: red;">
                                   <?php echo $value; ?> 
                                </p>
                                <?php  
                                        endforeach;
                                    endif;
                                ?>
                        </div>

                        <div class="form-group">

                            <label>ID</label>
                            <input class="form-control" name="id" value="<?php 
                                if(isset($_POST['id'])) echo $_POST['id'];
                                    else echo $admins['id'];
                            ?>" readonly/>
                        </div>


                        <div class="form-group">
                            <label>Username</label>
                            <input class="form-control" name="username" value="<?php 
                                if(isset($_POST['username'])) echo $_POST['username'];
                                    else echo $admins['username'];
                            ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" value="<?php 
                                if(isset($_POST['password'])) echo $_POST['password'];
                                    else echo $admins['password'];
                            ?>"/>
                        </div>
                        <div class="form-group">
                            <label>RePassword</label>
                            <input type="password" class="form-control" name="repass" placeholder="Please Enter RePassword" />
                        </div>
                        <div class="form-group">
                            <label>Fullname</label>
                            <input class="form-control" name="fullname" value="<?php 
                                if(isset($_POST['fullname'])) echo $_POST['fullname'];
                                    else echo $admins['fullname'];
                            ?>" />
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" value="<?php 
                                if(isset($_POST['email'])) echo $_POST['email'];
                                    else echo $admins['email'];
                            ?>"/>
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <label class="radio-inline">
                                <input name="role" value="0" checked="" type="radio" <?php if (isset($_POST['status']) && $_POST['role'] == 0 || $admins['role'] == 0 )  {
                                    echo "checked = 'checked'";
                                } ?>>Admin
                            </label>
                            <label class="radio-inline">
                                <input name="role" value="1" type="radio" <?php if (isset($_POST['role']) && $_POST['role'] == 1 || $admins['role'] == 1 )  {
                                    echo "checked = 'checked'";
                                } ?>>Super Admin
                            </label>
                        </div>

                        <div class="form-group">
                            <label>Status</label>
                            <label class="radio-inline">
                                <input name="status" value="1" checked="" type="radio"<?php if (isset($_POST['status']) && $_POST['status'] == 1 || $admins['status'] == 1 )  {
                                    echo "checked = 'checked'";
                                } ?>>Kích hoạt
                            </label>
                            <label class="radio-inline">
                                <input name="status" value="0" type="radio" <?php if (isset($_POST['status']) && $_POST['status'] == 0 || $admins['status'] == 0 )  {
                                    echo "checked = 'checked'";
                                } ?>>Vô hiệu hóa.
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default" name="submit">User Edit</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
<!-- /#page-wrapper -->