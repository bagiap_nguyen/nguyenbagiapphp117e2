<?php  
    require '../Layouts/index.php';

    $listAdmins     = getAllData('id,username,role,status',
                            'admins','','');
?>

<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User
                        <small>List</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <?php  
                    if (!is_null($listAdmins) && count($listAdmins)> 0):
                    
                ?>
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr align="center">
                            <th>ID</th>
                            <th>Username</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th>Delete</th>
                            <th>View</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php  
                            foreach ($listAdmins as $item) :
                        ?>
                        <tr class="odd gradeX" align="center">
                            <td>
                                <?php  
                                    echo $item['id'];
                                ?>
                            </td>
                            <td>
                                <?php  
                                    echo $item['username'];
                                ?>
                            </td>
                            <td>
                                <?php  
                                    if ($item['role'] == 0) {
                                        echo "Admin";
                                    } else {
                                        echo "Super Admin";
                                    }
                                ?>
                            </td>
                            <td>
                                <?php  
                                    if ($item['status'] == 0) {
                                        echo "Đang khóa";
                                    } else {
                                        echo "Kích hoạt";
                                    }
                                ?>
                            </td>
                            <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
                                <a href="delelte.php?id=<?php echo $item['id']; ?>"> 
                                    Delete
                                </a>
                            </td>
                            <td class="center"><i class="fa fa-pencil fa-fw"></i> 
                                <a href="view.php?id=<?php echo $item['id']; ?>"> 
                                    View
                                </a>
                            </td>
                            <td class="center"><i class="fa fa-pencil fa-fw"></i>
                                <a href="edit.php?id=<?php echo $item['id']; ?>"> 
                                    Edit
                                </a>
                            </td>
                        </tr>

                        <?php  
                            endforeach;
                        ?>
                        
                    </tbody>
                </table>
                <?php  
                    else:
                ?>
                <p>
                    Chưa có dữ liệu
                </p>
                <?php  
                    endif;
                ?>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
<!-- /#page-wrapper -->