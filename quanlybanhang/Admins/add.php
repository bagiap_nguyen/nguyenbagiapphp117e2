<?php  
    require '../Layouts/index.php';

    $errors = [];
    if (isset($_POST['submit'])) {
    	
    	if (empty($_POST['username']) || !isset($_POST['username'])) {
    		$errors[] = 'Vui lòng điền tên đăng nhập';
    	}

    	if (empty($_POST['password'])) {
    		$errors[] = 'Vui lòng nhập mật khẩu.';
    	}

    	if (empty($_POST['email'])) {
    		$errors[] = 'Vui lòng nhập email';
    	}

    	if (empty($_POST['fullname'])) {
    		$errors[] = 'Vui lòng nhập đầy đủ họ tên.';
    	}

    	if (!isset($_POST['role'])) {
    		$errors[] = 'Vui lòng chọn mức phân quyền.';
    	}

    	if (!isset($_POST['status'])) {
    		$errors[] = 'Vui lòng chọn status';
    	}

    	if ($_POST['password'] != $_POST['repass']) {
    		$errors[] = 'Mật khẩu nhập lại không khớp.';
    	}

    	if (count($errors) == 0) {

    		$username 	= $_POST['username'];

    		$password 	= $_POST['password'];

    		$email 		= $_POST['email'];

    		$fullname 	= $_POST['fullname'];

    		$role 		= $_POST['role'];

    		$status 	= $_POST['status'];

    		$check_email= getOneRecord('email','admins',
    						"email ='{$email}'");

    		$check_username = getOneRecord('username','admins',
    				"username = '{$username}' ");

    		if ($check_username || $check_email) {

    			$errors[] 	   = 'Tài khoản đã tồn tại.';

    		} else {

    			$password  		= md5($password);

    			$colums 	= ['username', 'password', 'email', 'fullname', 'role', 'status'];

	    		$data 		= ["'{$username}'","'{$password}'",
	    		"'{$email}'","'{$fullname}'","'{$role}'","'{$status}'"];

	    		$insert 	= insertData('admins',$colums,$data);

	    		if ($insert == true) {
					$success = 'Thêm thành công.';
				} else {
					$errors[] = 'Xảy ra lỗi, thêm thất bại.';
				}
    		}

    		
    	}
    }
?>

<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User
                        <small>Add</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    <form action="" method="POST">

                    	<div class="form-group">
                                <?php  
                                    if (isset($success)) :
                                ?>
                                <label style="color: green;">
                                    <?php echo $success; ?>
                                    
                                </label>
                                <?php  
                                    endif;
                                ?>
                                <?php  
                                    if (count($errors) > 0) :
                                        foreach ($errors as $key => $value) :
                                ?>
                                <p style="color: red;">
                                   <?php echo $value; ?> 
                                </p>
                                <?php  
                                        endforeach;
                                    endif;
                                ?>
                            </div>

                        <div class="form-group">
                            <label>Username</label>
                            <input class="form-control" name="username" placeholder="Please Enter Username" />
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Please Enter Password" />
                        </div>
                        <div class="form-group">
                            <label>RePassword</label>
                            <input type="password" class="form-control" name="repass" placeholder="Please Enter RePassword" />
                        </div>
                        <div class="form-group">
                            <label>Fullname</label>
                            <input class="form-control" name="fullname" placeholder="Please Enter Username" />
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Please Enter Email" />
                        </div>
                        <div class="form-group">
                            <label>Role</label>
                            <label class="radio-inline">
                                <input name="role" value="0" checked="" type="radio">Admin
                            </label>
                            <label class="radio-inline">
                                <input name="role" value="1" type="radio" >Super Admin
                            </label>
                        </div>

                        <div class="form-group">
                            <label>Status</label>
                            <label class="radio-inline">
                                <input name="status" value="1" checked="" type="radio">Kích hoạt
                            </label>
                            <label class="radio-inline">
                                <input name="status" value="0" type="radio" >Vô hiệu hóa.
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default" name="submit">User Add</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
<!-- /#page-wrapper -->