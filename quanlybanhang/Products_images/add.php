<?php  
	require '../Layouts/index.php';

	$products 	= getAllData('id,name','products','','');


    $errors     = [];
    if (isset($_POST['submit'])) {
        // Validate
        // 
        if (!isset($_POST['product_id'])) {
            $errors[]   = 'Vui lòng chọn sản phẩm.';
        }

        if (!isset($_POST['is_featured'])) {
            $errors[]   = 'Vui lòng chọn cài đặt.';
        }

        if (!isset($_POST['img'])) {
            $errors[]   = 'Vui lòng tải hình ảnh.';
        }

        if (count($errors) == 0) {
            $product_id     = $_POST['product_id'];

            $img            = $_POST['img'];

            $is_featured    = $_POST['is_featured'];

            $colums         = ['product_id', 'img', 'is_featured'];

            $data           = ["'{$product_id}'","'{$img}'","'{$is_featured}'"];

            $insert         = insertData('product_images',$colums,$data);

            if ($insert) {
                $success    = 'Thêm thành công';
            } else {
                $errors[]   = 'Xảy ra lỗi.';
            }
        }
    }

?>

<!-- Page Content -->
    <div id="page-wrapper">
    	
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Product images
                        <small>Add</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <div class="col-lg-7" style="padding-bottom:120px">
                    <form action="" method="POST">

                        <div class="form-group">
                                <?php  
                                    if (count($errors) > 0) :
                                        foreach ($errors as $key => $value) :
                                ?>
                                <label style="color: red; font-size: 21px;">
                                    <?php echo $value; ?>
                                </label>
                                <?php 
                                        endforeach; 
                                    endif;
                                ?>

                                <?php  
                                    if (isset($success)) :
                                ?>
                                <p style="color: blue;font-size: 22px;">
                                    <?php echo $success; ?>
                                </p>
                                <?php  
                                    endif;
                                ?>
                            </div>
                        <div class="form-group">
                            <label>Products</label>
                            <select class="form-control" name="product_id">
                                <?php  
                                    foreach ($products as $item) :
                                ?>
                                <option value="<?php echo $item['id']; ?>">
                                    <?php  
                                        echo $item['name'];
                                    ?>
                                </option>
                                <?php  
                                    endforeach;
                                ?>

                            </select>
                        </div>

                        <div class="form-group">
                                <label>Images</label>
                                <input type="file" name="img">
                            </div>
                        
                        <div class="form-group">
                            <label>Tùy chọn</label>
                            <label class="radio-inline">
                                <input name="is_featured" value="1" checked="" type="radio">Ưu tiên
                            </label>
                            <label class="radio-inline">
                                <input name="is_featured" value="0" type="radio">Bình thường
                            </label>
                        </div>
                        <button type="submit" class="btn btn-default" value="submit" name="submit"> Add</button>
                        <button type="reset" class="btn btn-default">Reset</button>
                    <form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
<!-- /#page-wrapper -->