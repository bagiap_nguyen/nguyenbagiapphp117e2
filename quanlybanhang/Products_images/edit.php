<?php  
	require '../Layouts/index.php';

	$id = $_GET['id'];

	if ($id == null) {
		header('Location:index.php');
		exit();
	}

	$pro_img 	= getOneRecord('*','product_images',"id='{$id}'");

	if (is_null($pro_img)) {
		header('Location:index.php');
		exit();
	}

	$pr_id 		= $pro_img['product_id'];

	$product 	= getOneRecord('name','products',"id ='{$pr_id}'");


	$errors 	= [];
	if (isset($_POST['submit'])) {

		if (!isset($_POST['status'])) {
			$errors[] 	= 'Vui lòng chọn trạng thái ảnh.';
		}

		if (count($errors) == 0) {

			$id_img 			= $_POST['id'];

			$is_featured 		= $_POST['status'];
			
			$check_id       	= getOneRecord('id','product_images',"id = '{$id_img}' AND id <> '{$id}' ");

			if (!is_null($check_id)) {
				$errors[] 	= 'Ảnh có id đã tồn tại.';
			} else {
				$sql = "UPDATE product_images SET is_featured = '{$is_featured}' WHERE id ='{$id}' ";

				$query = $db->query($sql);

				if ($query) {
					$success = 'Cập nhật thành công.';
				} else {
					$errors[] 	= 'Xảy ra lỗi';
				}
			}
		}

	}

?>
<!-- Page Content -->
	<div id="page-wrapper">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-lg-12">
	                <h1 class="page-header">Product Images
	                    <small>Edit</small>
	                </h1>
	            </div>
	            <!-- /.col-lg-12 -->
	            <div class="col-lg-7" style="padding-bottom:120px">
	            	<div class="heading">
	            		<p style="color: green; font-size: 21px;">
	            			<?php  
	            				echo $product['name'];
	            			?>
	            		</p>
	            	</div>
	                <form action="" method="POST">

	                	<div class="form-group">
                            <?php  
                                if (count($errors) > 0) :
                                    foreach ($errors as $key => $value) :
                            ?>
                            <label style="color: red; font-size: 21px;">
                                <?php echo $value; ?>
                                
                            </label>

                            <?php  
                                    endforeach;
                                endif;
                            ?>
                            <?php  
                                if (isset($success)) :
                            ?>
                            <p style="color: blue;font-size: 21px;">
                                <?php echo $success; ?>
                            </p>
                            <?php endif; ?>
                        </div>

	                	<div class="form-group">

                            <label>ID</label>
                            <input class="form-control" name="id" value="<?php 
                                    if(isset($_POST['id'])) echo $_POST['id'];
                                        else echo $pro_img['id'];
                                ?>" readonly/>
                        </div>
	                
	                    <div class="form-group">
	                        <label>Tùy chọn hiển thị ưu tiên</label>
	                        <label class="radio-inline">
	                            <input name="status" value="0" checked="" type="radio">Invisible
	                        </label>
	                        <label class="radio-inline">
	                            <input name="status" value="1" type="radio">Visible
	                        </label>
	                    </div>
	                    <button type="submit" class="btn btn-default" name="submit">Save</button>
	                    <button type="reset" class="btn btn-default">Reset</button>
	                <form>
	            </div>
	        </div>
	        <!-- /.row -->
	    </div>
	    <!-- /.container-fluid -->
	</div>
<!-- /#page-wrapper -->