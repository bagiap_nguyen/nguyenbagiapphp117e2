<?php  
	require '../Layouts/index.php';

	$table 		= 'products';

	$field 	= " products.name ,product_images.id, product_images.img,product_images.is_featured ";

	$join 		= " INNER JOIN product_images ON products.id = product_images.product_id ";

	$where 		= " AND  products.id IN 
					(SELECT  product_id  FROM product_images
					)";

	$inner_join 		= $table . ''.$join;

	$pro_img 	= getAllData($field,$inner_join,$where,'');

?>

<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Product
                        <small>Images</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                <?php  
	            	if (isset($_SESSION['flash_message'])) :
	            ?>
	            <div class="_message">
	            	<p style="font-size: 30px;color: blue;" >
	            		<?php  
	            			echo $_SESSION['flash_message'];
	            		?>
	            	</p>
	            </div>
	            <?php  
	            		unset($_SESSION['flash_message']);
	            	endif;
	            ?>

	            
                <?php  
                	if (!is_null($pro_img) && count($pro_img) > 0) :
                ?>
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr align="center">
                            <th>ID</th>
                            <th>Product name</th>
                            <th>Featured</th>
                            <th>IMG</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php  
                    		foreach ($pro_img as $item) :
                    	?>
                        <tr class="odd gradeX" align="center">
                            <td>
                            	<?php  
                            		echo $item['id'];
                            	?>
                            </td>
                            <td>
                            	<?php  
                            		echo $item['name'];
                            	?>
                            </td>
                            <td>
                            	<?php  
                            		if ($item['is_featured'] == 0) {
                            			echo "Ảnh thường";
                            		} else {
                            			echo "Ảnh đại diện";
                            		}
                            	?>
                            </td>
                            <td>
                                <?php  
                                    echo $item['img'];
                                ?>
                            </td>
                            
                            <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="delete.php?id=<?php echo $item['id']; ?>"> Delete</a></td>
                            <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="edit.php?id=<?php echo $item['id']; ?>">Edit</a></td>
                        </tr>
                        <?php  
                        		endforeach;
                        ?>

                        
                    </tbody>
                </table>
                <?php  
                	else :
                ?>
                <h2>
                	Đang cập nhật...
                </h2>
                <?php  
                	endif;
                ?>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
<!-- /#page-wrapper -->