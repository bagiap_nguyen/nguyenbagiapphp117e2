<?php  
	require '../Layouts/index.php';

	$id = $_GET['id'];

	function deleteProductImages($id){

		if ($id == null) {
			return false;
		}

		$check_id 	= getOneRecord('id','product_images',"id='{$id}'");

		if (is_null($check_id)) {
			return false;
		}

		$delete = deleteData('product_images',"id='{$id}'");

		if ($delete) {
			return true;
		} else {
			return false;
		}
	}

	$deleteImg 		= deleteProductImages($id);

	if ($deleteImg == true) {
		$_SESSION['flash_message'] = 'Xóa thành công!';
		header('Location:index.php');
		exit();
	}

	if ($deleteImg == flase) {
		$_SESSION['flash_message'] = 'Không thể xóa, kiểm tra lại!';
		header('Location:index.php');
		exit();
	}
?>