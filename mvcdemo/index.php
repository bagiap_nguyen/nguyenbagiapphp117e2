<?php  
	require 'Helper/helper.php';
	require 'Config/Database.php';

	//index?c=Controller&&m=Method&&param..
	$controller = (isset($_GET['c'])) 	? $_GET['c'] : 'home';
	$method 	= (isset($_GET['m'])) 	? $_GET['m'] : 'index';

	$controllerName = ucfirst($controller) . 'Controller';
	
	if (file_exists(CONTROLLER_PATH . $controllerName . '.php')) {
		require(CONTROLLER_PATH . $controllerName . '.php');
		$c = new $controllerName();
		$c->$method();
	}
?>