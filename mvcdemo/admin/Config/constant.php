<?php  
	define('HOST', 'localhost');
	define('USERNAME', 'root');
	define('PASSWORD', '');
	define('DATABASE', 'shopaoquan');

	define('ROOT_PATH', dirname(__DIR__));
	
	define('CONTROLLER_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'Controller' . DIRECTORY_SEPARATOR);
	//echo CONTROLLER_PATH;
	
	define('MODEL_PATH', ROOT_PATH 	. DIRECTORY_SEPARATOR . 'Model' . DIRECTORY_SEPARATOR);

	//echo MODEL_PATH;
	
	define('VIEW_PATH', ROOT_PATH 	. DIRECTORY_SEPARATOR . 'View' . DIRECTORY_SEPARATOR);

	define('CONFIG_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'Config' . DIRECTORY_SEPARATOR);


	define('HELPER_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'Helper' . DIRECTORY_SEPARATOR);

	define('PUBLIC_PATH', ROOT_PATH. DIRECTORY_SEPARATOR .'Public'.DIRECTORY_SEPARATOR);
	
?>