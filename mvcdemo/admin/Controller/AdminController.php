<?php 
	require MODEL_PATH . 'Admin.php';


	class AdminController
	{

		protected $adminModel;

		public function __construct()
		{

			$this->adminModel = new Admin();
		}
		public function index()
		{
			$lisAdmins = $this->adminModel->getAdmins();
			
			$data 	  = [
							'admins' => $lisAdmins,
							'total'  => 10
						];
			return view('Admin.index',$data);
		}

		public function add()
		{

			if (isset($_POST['submit'])) {

				$username = trim($_POST['username']);
				$password = trim($_POST['password']);
				$fullname = trim($_POST['fullname']);
				$email 	  = trim($_POST['email']);
				$password = trim($_POST['password']);
				$phone 	  = trim($_POST['phone']);
				$address  = trim($_POST['address']);
				$level    = $_POST['level'];
				$status   = $_POST['status'];

				$admin = new Admin();

				$errors = $admin->validateAdmin($username,$password,$fullname,$email,$address,$level,$status,$phone);

				$check_user = $admin->getAdmin('username',"username = '{$username}'");

				if ($check_user) {
					$errors[] = 'Tên đăng nhập đã tồn tại.';
				}

				$check_email = $admin->getAdmin('email',"email = '{$email}'");
				if ($check_email) {
					$errors[] = 'Email đã tồn tại.';
				}
				if (count($errors) == 0) {
					
					$addEntity = $admin->createNewAdmin($username,$phone,$fullname,$email,$password,$address,$level,$status);
					if ($addEntity) {
						$success = 'ok';
					} else {
						$errors[] = 'not ok';
					}
				} 

			}

			$data = [
							'errors'  => isset($errors) ? $errors : '',
							'success' => isset($success) ? $success : ''
					];

			return view('Admin.add',$data);
		}

		public function edit()
		{
			$id 		= $_GET['id'];
			$admin 	= $this->adminModel->getAdmin('*',"id = '{$id}'");

			if ($id == null || $admin == null) {
				header('Location:?c=admin&m=index');
				exit();
			}

			if (isset($_POST['submit'])) {

				$username = trim($_POST['username']);

				$password = trim($_POST['password']);

				$fullname = trim($_POST['fullname']);

				$email 	  = trim($_POST['email']);

				$password = trim($_POST['password']);

				$phone 	  = trim($_POST['phone']);

				$address  = trim($_POST['address']);

				$level    = $_POST['level'];
				$status   = $_POST['status'];



				$errors = $this->adminModel->validateAdmin($username,$phone,$fullname,$email,$password,$address,$level,$status);

				$check_user = $this->adminModel->getAdmin('username',"username = '{$username}' AND username <> '{$admin['username']}' ");

				if ($check_user) {
					$errors[] = 'Tên đăng nhập đã tồn tại.';
				}

				$check_email = $this->adminModel->getAdmin('email',"email = '{$email}' AND email <> '{$admin['email']}'");

				if ($check_email) {
					$errors[] = 'Email đã tồn tại.';
				}

				$check_id = $this->adminModel->getAdmin('id',"id = '{$email}' AND id <> '{$_POST['id']}'");

				if ($check_id) {
					$errors[] = 'Id không được sửa.';
				}


				if (count($errors) == 0) {
					$edit = $this->adminModel->updateAdmin($username,$phone,$fullname,$email,$password,$address,$level,$status
						,$where = "id ='{$id}' " );
					if ($edit) {
						$success = 'ok';
					} else {
						$errors[] = 'not ok';
					}
				}
			}

			$data = [
							'admin'   => isset($admin) ? $admin : '',
							'errors'  => isset($errors) ? $errors : '',
							'success' => isset($success) ? $success : ''
					];


			return view('Admin.edit',$data);
		}

		public function delete()
		{
			$id = $_GET['id'];

			$test = $this->adminModel->getAdmin('id',"id = '{$id}'");

			if ($id == null || $test == null) {
				header('Location:?c=admin');
				exit;
			}

			$delete = $this->adminModel->deleteAdmin("id = '{$id}'");

			if ($delete) {
				header('Location:?c=admin');
				exit();
			} else {
				echo "1";
			}
		}

		
	}
?>