<style type="text/css">
	#input span {
		float: left;
		width: 100px;

	}
	#input {
		margin-top: 20px;
	}
</style>


<div id="message">
	<?php  
		if (isset($errors) && is_array($errors) && count($errors) > 0) :
			foreach ($errors as $key => $value) :
	?>
	<p>
		<?php  
			echo $value;
		?>
	</p>
	<?php  
			endforeach;
		endif;
	?>
</div>


<div id="content">
	<form action="" method="post">
		<div id="heading">
			<h2>Add- Admin</h2>
		</div>
		<div id="main">
			<div id="input">
				<span>Username:</span> 
				<input type="text" name="username">
			</div>

			<div id="input">
				<span>Fullname:</span> 
				<input type="text" name="fullname">
			</div>

			<div id="input">
				<span>Email:</span> 
				<input type="text" name="email">
			</div>

			<div id="input">
				<span>Password:</span>
				<input type="password" name="password">
			</div>
			<div id="input">
				<span>Confirm password:</span> 
				<input type="password" name="repass">
			</div>
			<div id="input">
				<span>Address:</span> 
				<input type="text" name="address">
			</div>
			<div id="input">
				<span>Phone:</span> 
				<input type="text" name="phone">
			</div>
			<div id="input">
				<span>Level:</span> 
				<input type="radio" name="level" value="0" checked="">Admin
				<input type="radio" name="level" value="1"> Super Admin
			</div>

			<div id="input">
				<span>Status:</span>
				<input type="radio" name="status" value="0" checked=""> Vô hiệu
				<input type="radio" name="status" value="1"> Hoạt động
			</div>
			<div id="submit">
				<input type="submit" name="submit" value="Add">
			</div>
		</div>
	</form>
</div>
