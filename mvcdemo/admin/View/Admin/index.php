<?php  

	if (count($admins) > 0 && !is_null($admins)) :
?>
<div id="list-admin">
	<table border="1" cellpadding="10">
		<thead>
			<tr>
				<th colspan="9">
					<a href="?c=admin&m=add">Add Admin</a>
				</th>
			</tr>
			<tr>
				<th>ID</th>
                <th>Username</th>
                <th>Fullname</th>
                <th>Email</th>
                <th>Level</th>
                <th>Created_at</th>
                <th>Status</th>
                <th colspan="2">Options</th>
			</tr>
		</thead>
		<tbody>
			<?php  
				foreach ($admins as $admin) :
			?>
			<tr>
				<td>
					<?php  
						echo $admin['id'];
					?>
				</td>
				<td>
					<?php  
						echo $admin['username'];
					?>
				</td>

				<td>
					<?php  
						echo $admin['fullname'];
					?>
				</td>
				<td>
					<?php  
						echo $admin['email'];
					?>
				</td>
				<td>
					<?php  
						echo ($admin['level']==1) ? 'Super Admin' : 'Admin';
					?>
				</td>

				<td>
					<?php  
						echo $admin['created_at'];
					?>
				</td>
				<td>
					<?php  
						echo ($admin['status'] == 1) ? 'Đang hoạt động' : 'Đang bị vô hiệu ' ;
					?>
				</td>
				<td>
					<a href="?c=admin&m=edit&id=<?php echo $admin['id']; ?>">Edit</a>
				</td>
				<td>
					<a href="?c=admin&m=delete&id=<?php echo $admin['id']; ?>">Delete</a>
				</td>
			</tr>
			<?php  
				endforeach;
			?>
		</tbody>
	</table>
</div>
<?php  
	else:
?>
<div id="not-found">
	<p>
		Chưa có dữ liệu...
	</p>
</div>
<?php  
	endif;
?>