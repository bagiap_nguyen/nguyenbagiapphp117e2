<?php 

	function view($view = '', $data = [])
	{
		foreach ($data as $key => $items) {
			$$key = $items;
		}

		$view = @str_replace('.', DIRECTORY_SEPARATOR, $view);

		$content = VIEW_PATH . $view . '.php';


		if (!file_exists($content)) {
			$content = VIEW_PATH 	. 'Layouts' . DIRECTORY_SEPARATOR
									.'error.php';
		}

		require VIEW_PATH . 'Layouts'	.  DIRECTORY_SEPARATOR 
										.'default.php';
	}

	function redirect($url = '/')
	{
		header('Location: ' . $url);
		exit;
	}